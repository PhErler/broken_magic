#pragma once

#include "GameObject.hpp"

namespace MagicEngine
{
	struct VolumetricLightInfo
	{
		//TODO: only orthographic working atm
		// dir is only used if locked object is not set
		VolumetricLightInfo(bool projective = true,	glm::vec3 color = glm::vec3(1,1,1), glm::vec3 dir = glm::vec3(1,1,0))
		: _projective(projective)
		, _color(color)
		, _dir(dir)
		{}

		bool _projective;
		glm::vec3 _color;
		glm::vec3 _dir;
	};

	class VolumetricObject : public GameObject
	{
	public:
		VolumetricObject(); // transparent -> forward rendering; not transparent -> deferred rendering
		virtual ~VolumetricObject();

		virtual void update(double dt) override;

		void setLightInfo(VolumetricLightInfo volLightInfo) { _volLightInfo = volLightInfo; }
		VolumetricLightInfo getLightInfo() { return _volLightInfo; }

		void setStepSize(float stepSize = 0.1f) { _stepSize = stepSize; }
		float getStepSize() { return _stepSize; }
		void setLightRayStepSize(float lightRayStepSize = 0.5f) { _lightRayStepSize = lightRayStepSize; }
		float getLightRayStepSize() { return _lightRayStepSize; }
		void setOpacityFactor(float opacityFactor = 1.0f) { _opacityFactor = opacityFactor; }
		float getOpacityFactor() { return _opacityFactor; }

		void setLockedObject(GameObject* lockOnThis) { _lockedObject = lockOnThis; }
		GameObject* getLockedObject() { return _lockedObject; }
		
		glm::vec2 getNoisePos() { return _noisePos; }

		float getNoiseOffsetFactor() { return _noiseOffsetFactor; }
		void setNoiseOffsetFactor(float factor) { _noiseOffsetFactor = factor; }

		void setNoiseSpeed(glm::vec2 speed) { _noiseSpeed = speed; }

	protected:
		std::shared_ptr<VolumeTexture> _volumeTexture;
		float _stepSize = 0.1f;
		float _lightRayStepSize = 0.5f;
		float _opacityFactor = 1.0f;

		VolumetricLightInfo _volLightInfo;

		glm::vec2 _noisePos;
		float _noiseOffsetFactor = 0.1f;
		glm::vec2 _noiseSpeed = glm::vec2(0.05f, 0.05f);

		GameObject* _lockedObject;
	};
}

