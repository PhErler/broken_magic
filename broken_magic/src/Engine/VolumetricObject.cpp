#include "VolumetricObject.hpp"

#include "glm\gtx\transform.hpp"

using namespace std;
using namespace glm;
using namespace MagicEngine;

namespace
{
	const float FOV = 90;
	const float Aspect = 1;
}

VolumetricObject::VolumetricObject()
{
	_isVolumetric = true;
}

VolumetricObject::~VolumetricObject()
{
	_volumeTexture.reset();
}

void MagicEngine::VolumetricObject::update(double dt)
{
	GameObject::update(dt);

	mat4 modelMatrix = getWorldspaceMatrix();
	vec3 modelPos = vec3(modelMatrix[3].x, modelMatrix[3].y, modelMatrix[3].z);

	vec3 lightPos = modelPos;
	vec3 lightDir = _volLightInfo._dir;
	if (_lockedObject)
	{
		lightDir = normalize(lightPos - _lockedObject->getPosition());
		_volLightInfo._dir = lightDir;
	}

	_noisePos += float(dt) * _noiseSpeed;
	if (_noisePos.x > 1)
	{
		_noisePos.x -= 1;
	}
	if (_noisePos.y > 1)
	{
		_noisePos.y -= 1;
	}
}
