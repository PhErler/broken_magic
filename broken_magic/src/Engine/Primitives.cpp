#include "Primitives.hpp"

using namespace std;
using namespace glm;
using namespace MagicEngine;

shared_ptr<VertexBuffer> MagicEngine::Primitives::getUnitCubeVertexBuffer()
{
	const vector<vec3> corners = {
		vec3(-0.5, -0.5, -0.5),
		vec3( 0.5, -0.5, -0.5),
		vec3(-0.5, -0.5,  0.5),
		vec3( 0.5, -0.5,  0.5),
		vec3(-0.5,  0.5, -0.5),
		vec3( 0.5,  0.5, -0.5),
		vec3(-0.5,  0.5,  0.5),
		vec3( 0.5,  0.5,  0.5),
	};

	// front faces outside
	const vector<vector<int>> faces = {
		{ 5, 1, 0, },
		{ 4, 5, 0, },
		{ 7, 5, 4, },
		{ 6, 7, 4, },
		{ 6, 4, 0, },
		{ 2, 6, 0, },
		{ 7, 3, 5, },
		{ 3, 1, 5, },
		{ 3, 7, 6, },
		{ 2, 3, 6, },
		{ 1, 3, 2, },
		{ 1, 2, 0, },
	};

	// front faces inside
	//vector<vector<int>> faces = {
	//	{ 0, 1, 5 },
	//	{ 0, 5, 4 },
	//	{ 4, 5, 7 },
	//	{ 4, 7, 6 },
	//	{ 0, 4, 6 },
	//	{ 0, 6, 2 },
	//	{ 5, 3, 7 },
	//	{ 5, 1, 3 },
	//	{ 6, 7, 3 },
	//	{ 6, 3, 2 },
	//	{ 2, 3, 1 },
	//	{ 0, 2, 1 },
	//};

	std::vector<glm::vec3> vertices;
	vertices.reserve(12 * 3);

	for (auto &face : faces)
	{
		for (auto &vertexIndex : face)
		{
			vertices.push_back(corners[vertexIndex]);
		}
	}

	return make_shared<VertexBuffer>(vertices);
}

shared_ptr<VertexBuffer> MagicEngine::Primitives::getQuadVertexBuffer()
{
	// plane made of 2 triangles
	const vector<vec3> vertices = {
		vec3(-1.0f, -1.0f, 0.0f),
		vec3( 1.0f, -1.0f, 0.0f),
		vec3(-1.0f,  1.0f, 0.0f),
		vec3(-1.0f,  1.0f, 0.0f),
		vec3( 1.0f, -1.0f, 0.0f),
		vec3( 1.0f,  1.0f, 0.0f),
		vec3(-1.0f, -1.0f, 0.0f),
		vec3(1.0f, -1.0f, 0.0f),
		vec3(-1.0f,  1.0f, 0.0f),
		vec3(-1.0f,  1.0f, 0.0f),
		vec3(1.0f, -1.0f, 0.0f),
		vec3(1.0f,  1.0f, 0.0f),
	};

	std::vector<glm::vec3> quadVertices;
	quadVertices.reserve(2 * 3);

	for (auto &vertex : vertices)
	{
		quadVertices.push_back(vertex);
	}

	return make_shared<VertexBuffer>(quadVertices);
}
