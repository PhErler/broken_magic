#include "FrameBuffer.hpp"

#include "RenderTexture.hpp"
#include "MagicEngineMain.hpp"
#include "OGL.hpp"

using namespace std;
using namespace MagicEngine;

#ifdef _DEBUG
GLuint FrameBuffer::s_boundDrawBuffer = 0;
#endif

FrameBuffer::FrameBuffer(std::vector<RenderTexture*>* colorTextures, const std::vector<GLenum> &drawBufferAttachments, RenderTexture* depthTexture, bool withStencil)
{
	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	OGL::genFramebuffers(1, &_resourceID);
	updateRenderTexture(colorTextures, drawBufferAttachments, depthTexture, withStencil);
	clear(colorTextures != nullptr, depthTexture != nullptr, withStencil);
}

FrameBuffer::~FrameBuffer()
{
	OGL::deleteFramebuffers(1, &_resourceID);
}

void FrameBuffer::updateRenderTexture(std::vector<RenderTexture*>* colorTextures, const std::vector<GLenum> &drawBufferAttachments, RenderTexture* depthTexture, bool withStencil)
{
	OGL::bindFramebuffer(GL_FRAMEBUFFER, _resourceID);

	if (depthTexture)
	{
		GLuint attachment;
		if (withStencil)
		{
			attachment = GL_DEPTH_STENCIL_ATTACHMENT;
		}
		else
		{
			attachment = GL_DEPTH_ATTACHMENT;
		}

		if (depthTexture->getType() == RenderTexture::DepthCubemap)
		{
			OGL::framebufferTexture(GL_FRAMEBUFFER, attachment, depthTexture->getResourceID(), 0);
		}
		else
		{
			OGL::framebufferTexture2D(GL_DRAW_FRAMEBUFFER, attachment, GL_TEXTURE_2D, depthTexture->getResourceID(), 0);
		}

		_width = depthTexture->getWidth();
		_height = depthTexture->getHeight();
	}

	if (colorTextures) {
		int i = 0;
		_drawBuffers.clear();
		_drawBuffers = drawBufferAttachments;
		for (RenderTexture* colorTexture : (*colorTextures))
		{
			OGL::framebufferTexture2D(GL_DRAW_FRAMEBUFFER, _drawBuffers[i], GL_TEXTURE_2D, colorTexture->getResourceID(), 0);

			// Set the list of draw buffers.
			OGL::drawBuffers(_drawBuffers.size(), &_drawBuffers[0]);

			_width = colorTexture->getWidth();
			_height = colorTexture->getHeight();

			++i;
		}
	}
	else {
		OGL::drawBuffer(GL_NONE);	// No color output in the bound framebuffer, only depth.
		OGL::readBuffer(GL_NONE);	// No color output in the bound framebuffer, only depth.
	}


#ifdef _DEBUG
	// the detected error here can have happened everywhere
	GLenum error = OGL::getError();
	assert(error == GL_NO_ERROR);

	if (colorTextures)
	{
		_colorTextures = *colorTextures;
	}
	_depthTexture = depthTexture;
	s_boundDrawBuffer = _resourceID;
	markTexturesForWrite(true);
#endif

	GLenum status = OGL::checkFramebufferStatus(GL_FRAMEBUFFER);
	assert(status == GL_FRAMEBUFFER_COMPLETE);
}

void FrameBuffer::activate()
{
	OGL::bindFramebuffer(GL_DRAW_FRAMEBUFFER, _resourceID);
	MagicEngineMain::setViewPort(Rect(0, 0, _width, _height));

#ifdef _DEBUG
	s_boundDrawBuffer = _resourceID;
	markTexturesForWrite(true);
#endif
}

void FrameBuffer::useAssignedAttachments()
{
	if (!_drawBuffers.empty())
	{
		OGL::drawBuffers(_drawBuffers.size(), &_drawBuffers[0]);
	}
}

void MagicEngine::FrameBuffer::activateForWrite(const std::vector<GLenum>& drawBufferAttachments)
{
	_drawBuffers.clear();
	_drawBuffers = drawBufferAttachments;

	activate();

	if (!_drawBuffers.empty())
	{
		OGL::drawBuffers(_drawBuffers.size(), &_drawBuffers[0]);
	}
}

void FrameBuffer::activateForRead(int colorAttachment)
{
	OGL::bindFramebuffer(GL_READ_FRAMEBUFFER, _resourceID);
	OGL::readBuffer(colorAttachment);
}

void FrameBuffer::deactivate()
{
	OGL::bindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	OGL::bindFramebuffer(GL_READ_FRAMEBUFFER, 0);

#ifdef _DEBUG
	s_boundDrawBuffer = 0;
	markTexturesForWrite(false);
#endif
}

void FrameBuffer::clear(bool clearColor, bool clearDepth, bool clearStencil, 
	float r, float g, float b, float a, float depth, GLint stencil)
{
#ifdef _DEBUG
	assert(s_boundDrawBuffer == _resourceID);
#endif

	int bufferBits = 0x0;

	if (clearColor)
	{
		OGL::clearColor(r, g, b, a);
		bufferBits |= GL_COLOR_BUFFER_BIT;
	}
	if (clearDepth)
	{
		OGL::clearDepth(depth);
		bufferBits |= GL_DEPTH_BUFFER_BIT;
	}
	if (clearStencil)
	{
		OGL::clearStencil(stencil);
		bufferBits |= GL_STENCIL_BUFFER_BIT;
	}

	OGL::clear(bufferBits);
}

#ifdef _DEBUG
void FrameBuffer::markTexturesForWrite(bool write)
{
	for (RenderTexture* tex : _colorTextures)
	{
		tex->setBoundForWrite(write);
	}
	if (_depthTexture)
	{
		_depthTexture->setBoundForWrite(write);
	}
}
#endif
