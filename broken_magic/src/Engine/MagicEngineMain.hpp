#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <memory>

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "GameObject.hpp"
#include "RenderTexture.hpp"
#include "FrameBuffer.hpp"
#include "IGameManager.hpp"
#include "Resource.hpp"
#include "Camera.hpp"
#include "Light.hpp"

#include "ShadowMappingPipeline.hpp"
#include "DeferredRenderingPipeline.hpp"
#include "PostProcessingRenderingPipeline.hpp"
#include "VolumeRenderingPipeline.hpp"
#include "SkyboxRenderPipeline.hpp"

#include "SoundEngine.hpp"

namespace MagicEngine
{
	struct CommandLineParams
	{
		CommandLineParams(GLuint xRes,
		GLuint yRes,
		GLuint refreshRate,
		bool fullScreen,
		float quality)
		: _xRes(xRes)
		, _yRes(yRes)
		, _refreshRate(refreshRate)
		, _fullScreen(fullScreen)
		, _quality(quality)
		{};

		GLuint _xRes;
		GLuint _yRes;
		GLuint _refreshRate;
		bool _fullScreen;
		float _quality;
	};

	// to avoid opengl state changes
	struct Rect
	{
		Rect(GLuint left,
		GLuint top,
		GLuint width,
		GLuint height)
		: _left(left)
		, _top(top)
		, _width(width)
		, _height(height)
		{};

		GLuint _left;
		GLuint _top;
		GLuint _width;
		GLuint _height;
	};

	struct DirectionalLight
	{
		DirectionalLight(BaseLight baseLight, glm::vec3 dir, glm::mat4 projectionMatrix)
		: _baseLight(baseLight)
		, _dir(glm::normalize(dir))
		, _projectionMatrix(projectionMatrix)
		{
			_viewMatrix = glm::lookAt(glm::vec3(0, 0, 0), _dir, glm::vec3(0, 1, 0));
		}

		BaseLight _baseLight;
		glm::vec3 _dir;
		glm::mat4 _viewMatrix;
		glm::mat4 _projectionMatrix;
	};

	class MagicEngineMain
	{
	public:
		static int init(int argc, char* argv[]);
		static int run();
		static int deinit();

		static GLFWwindow* getWindow();
		static const CommandLineParams& getCommandLineParams();

		static void setSceneGraphRootNode(GameObject* gameObject){ if (!s_sceneGraphRoot) s_sceneGraphRoot = gameObject; }
		static GameObject* getSceneGraphRootNode() { return s_sceneGraphRoot; }

		static void setGameManager(IGameManager* gameManager){ s_gameManager = gameManager; }
		static IGameManager* getGameManager(){ return s_gameManager; }

		static void setCamera(Camera* camera);
		static Camera* getCamera() { return s_camera; }

		static std::vector<Light*> getLights();
		static void addLight(Light* light);
		static void removeLight(Light* light);

		static DirectionalLight getDirectionalLight() { return s_directionalLight; }
		static void setDirectionalLight(DirectionalLight dirLight) { s_directionalLight = dirLight; }

		static DeferredRenderingPipeline* getDeferredRenderingPipeline() { return s_deferredRenderingPipeline; }
		static ShadowMappingPipeline* getShadowMappingPipeline() { return s_shadowMappingPipeline; }

		// viewfrustum culling
		static bool isCullingEnabled(){ return s_cullingEnabled; }
		static void addRenderedVertices(int count);
		static int getRenderedVertices();
		static void toggleCulling();

		// wireframe mode
		static bool wireFrameMode() { return s_wireFrameMode; }
		static void changeWireFrameMode();

		static int getDeferredDebugRenderingState() { return s_deferredDebugRenderingState; }
		static void incrementDeferredDebugRenderingState() { s_deferredDebugRenderingState = (s_deferredDebugRenderingState + 1) % 5; }

		static int getDepthOfFieldRenderingState() { return s_depthOfFieldRenderingState; }
		static void incrementDepthOfFieldRenderingState() { s_depthOfFieldRenderingState = (s_depthOfFieldRenderingState + 1) % 4; }
		static void decrementDepthOfFieldRenderingState() { s_depthOfFieldRenderingState = (s_depthOfFieldRenderingState - 1) % 4; }
		static bool getOutputDepthOfFieldRenderingState() { return s_depthOfFieldRenderingStateOutput; }
		static void DepthOfFieldRenderingStateOutput() { s_depthOfFieldRenderingStateOutput = true; }
		static void DepthOfFieldRenderingStateOutputFalse() { s_depthOfFieldRenderingStateOutput = false; }
		static bool getNormalMapRenderingState() { return s_normalMapRenderingState; }
		static void switchNormalMapRenderingState() { s_normalMapRenderingState = !s_normalMapRenderingState; }

		static GameObject::SceneParts getCurrentSceneState() { return s_currentSceneState; }
		static void setCurrentSceneState(GameObject::SceneParts scene) { s_currentSceneState = scene; }

		// frames per second
		static double getFPS();

		static void setViewPort(Rect viewPort);

		static GameObject* getGameObjectWithName(std::string name);

	private:
		static CommandLineParams parseCommandLineParams(int argc, char* argv[]);

		static void update();
		static void render();

		static DeferredRenderingPipeline* s_deferredRenderingPipeline;
		static ShadowMappingPipeline* s_shadowMappingPipeline;
		static PostProcessingRenderingPipeline* s_depthOfFieldRenderingPipeline;
		static VolumeRenderingPipeline* s_volumeRenderingPipeline;
		static SkyboxRenderPipeline* s_skyboxRenderPipeline;

		static VertexArray* s_vertexArray;

		static bool s_wireFrameMode;
		static bool s_cullingEnabled;
		static int s_verticesRenderedThisFrame;
		static int s_deferredDebugRenderingState;
		static int s_depthOfFieldRenderingState;
		static bool s_depthOfFieldRenderingStateOutput;
		static bool s_normalMapRenderingState;

		static GameObject::SceneParts s_currentSceneState;

		static bool s_initialized;
		static int s_errorCode;

		static CommandLineParams s_commandLineParams;
		static GLFWwindow* s_window;
		static IGameManager* s_gameManager;

		static std::vector<Light*> s_lights;

		static DirectionalLight s_directionalLight;

		static GameObject* s_sceneGraphRoot;	// tree structure

		static Camera* s_camera;

		static double s_fps;
		static Rect s_currViewport;

		static SoundEngine* s_music;
	};
}

