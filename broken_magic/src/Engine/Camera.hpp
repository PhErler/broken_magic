#pragma once

#include "GLSLProgram.hpp"

#include <memory>

#include "glm\glm.hpp"

namespace MagicEngine
{
	class RenderTexture;
	class FrameBuffer;
	class VertexBuffer;

	class Camera
	{
	public:
		Camera();
		virtual ~Camera();

		virtual void update(double dt);
		virtual void renderToScreen();

		const glm::vec3& getPosition() const { return _position; }
		void setPosition(const glm::vec3& pos) { _position = pos; }
		const glm::mat4& getViewMatrix() const { return _viewMatrix; }
		void setViewMatrix(const glm::mat4& view) { _viewMatrix = view; }
		const glm::mat4& getProjectionMatrix() const { return _projectionMatrix; }
		glm::vec2 getNearFar();

		RenderTexture* getRenderColorTexture() { return _renderColorTexture; }
		RenderTexture* getRenderDepthTexture() { return _renderDepthTexture; }
		FrameBuffer* getRenderBuffer() { return _renderBuffer; }

	protected:
		glm::vec3 _position;
		glm::vec3 _viewDir;
		glm::vec3 _viewRight;
		glm::mat4 _viewMatrix;
		glm::mat4 _projectionMatrix;
		glm::vec2 _nearFar;

		RenderTexture* _renderColorTexture;
		RenderTexture* _renderDepthTexture;
		FrameBuffer* _renderBuffer;
		VertexBuffer* _quadVertexBuffer;

		// Initial horizontal angle : toward -Z
		float _horizontalAngle = 3.14f;
		// Initial vertical angle : none
		float _verticalAngle = 0.0f;

		float _foV = 45.0f;

		float _speed = 15.0f; // 3 units / second
		float _mouseSpeed = 0.005f;

		float _zNearPlane = 0.1f;
		float _zFarPlane = 1000.0f;
	};
}

