#include "ShadowMappingPipeline.hpp"

#include "MagicEngineMain.hpp"
#include "Texture.hpp"

#include <glm/glm.hpp>

using namespace std;
using namespace glm;
using namespace MagicEngine;

namespace
{
#ifdef _DEBUG
	const int ShadowMapSize = 512;
#else
	const int ShadowMapSize = 4096; // is good enough
#endif

	const float ShadowMappingSampleDistance = 5000;
}

ShadowMappingPipeline::ShadowMappingPipeline(Camera* cam)
	: IRenderPipeline(cam)
{
	_shadowTexture = new RenderTexture(ShadowMapSize, ShadowMapSize,
		RenderTexture::Depth);
	_shadowFrameBuffer = new FrameBuffer(nullptr, vector<GLenum>(), _shadowTexture, false);

	_depthProgram = ResourceManager::getShader("resources\\shaders\\ShadowMappingZBuffer.vs",
		"resources\\shaders\\ShadowMappingZBuffer.fs");
	_shader = ResourceManager::getShader("resources\\shaders\\ShadowMappingShading.vs",
		"resources\\shaders\\ShadowMappingShading.fs");

	_zPassLightMVPLocation = _depthProgram->getUniformLocation("lightMVP");

	_uniformLocations.MvpMatrix = _shader->getUniformLocation("MVP");
	_uniformLocations.LightBiasMVP = _shader->getUniformLocation("LightBiasMVP");
	_uniformLocations.ViewMatrix = _shader->getUniformLocation("V");
	_uniformLocations.ModelMatrix = _shader->getUniformLocation("M");
	_uniformLocations.InvLightDir = _shader->getUniformLocation("invLightDirection_worldspace");

	_uniformLocations.DiffuseTexture = _shader->getUniformLocation("uDiffuseTextureSampler");
	_uniformLocations.ShadowMap = _shader->getUniformLocation("uShadowMap");
	_uniformLocations.DiffuseColor = _shader->getUniformLocation("uDirectionalLight.Base.DiffuseColor");
	_uniformLocations.AmbientColor = _shader->getUniformLocation("uDirectionalLight.Base.AmbientColor");
	_uniformLocations.SpecularColor = _shader->getUniformLocation("uDirectionalLight.Base.SpecularColor");
	_uniformLocations.EyeWorld = _shader->getUniformLocation("uEyeWorldPos");
	_uniformLocations.ShadowMappingSampleDistance = _shader->getUniformLocation("uShadowMappingSampleDistance");
	_uniformLocations.TextureTiling = _shader->getUniformLocation("uTextureTiling");
	_uniformLocations.SpecularExponent = _shader->getUniformLocation("uSpecularExponent");
}

ShadowMappingPipeline::~ShadowMappingPipeline()
{
	_shader.reset();
	_depthProgram.reset();

	delete _shadowFrameBuffer;
	delete _shadowTexture;
}

void MagicEngine::ShadowMappingPipeline::render()
{
	if (!_cam)
	{
		return;
	}

	_state = 0;
	zBufferPass();

	_state = 1;
	shadingPass();
}

void ShadowMappingPipeline::zBufferPass()
{
	OGL::depthMask(GL_TRUE);
	OGL::enable(GL_DEPTH_TEST);
	OGL::disable(GL_BLEND);

	OGL::enable(GL_CULL_FACE);
	OGL::cullFace(GL_BACK);

	// clear and render to depth buffer
	_shadowFrameBuffer->activate();
	_shadowFrameBuffer->useAssignedAttachments();
	_shadowFrameBuffer->clear(false, true, false);
	_shadowFrameBuffer->deactivate();

	// prevent shadow acne
	OGL::enable(GL_POLYGON_OFFSET_FILL);
	OGL::polygonOffset(4.0f, 0.0f); // try some values

	glm::mat4 lightViewMatrix = MagicEngineMain::getDirectionalLight()._viewMatrix;
	glm::mat4 lightProjectionMatrix = MagicEngineMain::getDirectionalLight()._projectionMatrix;

	_depthProgram->activate();

	_shadowFrameBuffer->activate();
	_shadowFrameBuffer->useAssignedAttachments();
	MagicEngineMain::getSceneGraphRootNode()->render(this);
	_shadowFrameBuffer->deactivate();

	_depthProgram->deactivate();

	OGL::disable(GL_CULL_FACE);
	OGL::depthMask(GL_FALSE);
	OGL::disable(GL_POLYGON_OFFSET_FILL);
}

void ShadowMappingPipeline::shadingPass()
{
	OGL::enable(GL_DEPTH_TEST);

	OGL::enable(GL_CULL_FACE);
	OGL::cullFace(GL_BACK);

	// prevent shadow acne; reset to 0
	OGL::enable(GL_POLYGON_OFFSET_FILL);
	OGL::polygonOffset(0.0f, 0.0f);

	OGL::disable(GL_BLEND);

	FrameBuffer* camFB = MagicEngineMain::getCamera()->getRenderBuffer();

	_shader->activate();

	_shadowTexture->activate(1);
	_shader->bindTexture(_uniformLocations.ShadowMap, 1);

	vec3 cameraPos = MagicEngineMain::getCamera()->getPosition();

	DirectionalLight dirLight = MagicEngineMain::getDirectionalLight();
	GLSLProgram::setUniform(_uniformLocations.InvLightDir, -dirLight._dir.x, -dirLight._dir.y, -dirLight._dir.z);
	GLSLProgram::setUniform(_uniformLocations.DiffuseColor, dirLight._baseLight.DiffuseColor.x, dirLight._baseLight.DiffuseColor.y, dirLight._baseLight.DiffuseColor.z);
	GLSLProgram::setUniform(_uniformLocations.AmbientColor, dirLight._baseLight.AmbientColor.x, dirLight._baseLight.AmbientColor.y, dirLight._baseLight.AmbientColor.z);
	GLSLProgram::setUniform(_uniformLocations.SpecularColor, dirLight._baseLight.SpecularColor.x, dirLight._baseLight.SpecularColor.y, dirLight._baseLight.SpecularColor.z);
	GLSLProgram::setUniform(_uniformLocations.ShadowMappingSampleDistance, ShadowMappingSampleDistance);
	GLSLProgram::setUniform(_uniformLocations.EyeWorld, cameraPos.x, cameraPos.y, cameraPos.z);

	glm::mat4 viewMatrix = MagicEngineMain::getCamera()->getViewMatrix();
	_shader->setUniform(_uniformLocations.ViewMatrix, viewMatrix);

	camFB->activate();
	camFB->useAssignedAttachments();
	MagicEngineMain::getSceneGraphRootNode()->render(this);
	camFB->deactivate();

	_shadowTexture->deactivate(1);

	_shader->deactivate();

	OGL::disable(GL_CULL_FACE);
	OGL::disable(GL_POLYGON_OFFSET_FILL);
}

void MagicEngine::ShadowMappingPipeline::draw(GameObject *gameObject)
{
	if (!gameObject->isVisible() || !gameObject->isTransparent() || gameObject->isVolumetric())
	{
		return;
	}

	if (_state == 0)
	{
		if (!gameObject->isTransparent())
		{
			zBufferDraw(gameObject);
		}
	}
	else if (_state == 1)
	{
		shadingDraw(gameObject);
	}
	else
	{
		cout << "ShadowMappingPipeline::draw wrong state" << endl;
	}
}

void ShadowMappingPipeline::zBufferDraw(GameObject *gameObject)
{
	std::shared_ptr<ModelData> modelData = gameObject->getModelData();
	if (!modelData)
	{
		return;
	}

	mat4 modelMatrix = gameObject->getWorldspaceMatrix();
	glm::mat4 lightViewMatrix = MagicEngineMain::getDirectionalLight()._viewMatrix;
	glm::mat4 lightProjectionMatrix = MagicEngineMain::getDirectionalLight()._projectionMatrix;
	mat4 mPlVl = lightProjectionMatrix * lightViewMatrix * modelMatrix;
	_depthProgram->activate();
	GLSLProgram::setUniform(_zPassLightMVPLocation, mPlVl);

	modelData->_vertexBuffer.activate();
	modelData->_vertexBuffer.draw();
	modelData->_vertexBuffer.deactivate();
	_depthProgram->deactivate();
}

void ShadowMappingPipeline::shadingDraw(GameObject *gameObject)
{
	// Compute the MVP matrix
	mat4 modelMatrix = gameObject->getWorldspaceMatrix();
	mat4 viewMatrix = _cam->getViewMatrix();
	glm::mat4 MVP = _cam->getProjectionMatrix() * viewMatrix * modelMatrix;

	if (MagicEngineMain::isCullingEnabled() && !gameObject->isInViewFrustum(MVP))
	{
		return;
	}

	std::shared_ptr<ModelData> modelData = gameObject->getModelData();
	if (!modelData)
	{
		return;
	}

	std::shared_ptr<Texture> texture = gameObject->getTexture();

	glm::mat4 biasMatrix(
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
	);

	glm::mat4 lightViewMatrix = MagicEngineMain::getDirectionalLight()._viewMatrix;
	glm::mat4 lightProjectionMatrix = MagicEngineMain::getDirectionalLight()._projectionMatrix;
	glm::mat4 lightBiasMVP = biasMatrix * lightProjectionMatrix * lightViewMatrix * modelMatrix;

	std::shared_ptr<GLSLProgram> specialProgram = gameObject->getProgram();
	if (specialProgram)
	{
		specialProgram->activate();
	}
	else
	{
		_shader->activate();

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		_shader->setUniform(_uniformLocations.MvpMatrix, MVP);
		_shader->setUniform(_uniformLocations.ModelMatrix, modelMatrix);
		_shader->setUniform(_uniformLocations.LightBiasMVP, lightBiasMVP);

		_shader->setUniform(_uniformLocations.TextureTiling, _textureTiling.x, _textureTiling.y);
		_shader->setUniform(_uniformLocations.SpecularExponent, gameObject->getSpecularExponent());

		// Set our DiffuseTextureSampler to user Texture Unit 0
		texture->activate(0);
		_shader->bindTexture(_uniformLocations.DiffuseTexture, 0);
	}

	modelData->_vertexBuffer.activate();
	modelData->_uvBuffer.activate();
	modelData->_normalBuffer.activate();

	modelData->_vertexBuffer.draw();

	modelData->_vertexBuffer.deactivate();
	modelData->_uvBuffer.deactivate();
	modelData->_normalBuffer.deactivate();

	if (specialProgram)
	{
		specialProgram->deactivate();
	}
	else
	{
		texture->deactivate(0);
		_shader->deactivate();
	}
}
