// Based on the smoke particles cuda example by NVidia

#include "GLSLProgram.hpp"

#include "OGL.hpp"

#include <fstream>
#include <stdlib.h>
#include <iostream>

#include <GL/glew.h>

using namespace std;
using namespace MagicEngine;

GLSLProgram::GLSLProgram(const string &vertexSourcePath, const string &fragmentSourcePath, const string &geometrySourcePath,
                         GLenum gsInput, GLenum gsOutput)
{
    _resourceID = compileProgram(vertexSourcePath, fragmentSourcePath, geometrySourcePath, gsInput, gsOutput);
	printf("gen program %d \n", _resourceID);
}

GLSLProgram::~GLSLProgram()
{
    if (_resourceID)
    {
		OGL::deleteProgram(_resourceID);
		printf("del program %d \n", _resourceID);
		_resourceID = -1;
    }
}

void GLSLProgram::activate()
{
	OGL::useProgram(_resourceID);
}

void GLSLProgram::deactivate()
{
	OGL::useProgram(0);
}

GLint GLSLProgram::getUniformLocation(const std::string &uniformName)
{
	activate();
	auto shader = s_uniformLocationMap.find(uniformName);
	if (shader == s_uniformLocationMap.end())
	{	// not found
		int location = OGL::getUniformLocation(_resourceID, uniformName.c_str());
		s_uniformLocationMap[uniformName] = location;
		if (location == -1)
		{
			cout << "GetUniformLocation failed for shader " << _resourceID << " uniform \"" << uniformName << "\"" << endl;
		}
		return s_uniformLocationMap[uniformName];
	}
	else 
	{	// found
		return shader->second;
	}
}

void GLSLProgram::bindTexture(GLint location, GLint unit)
{
    if (location >= 0)
    {
		OGL::useProgram(_resourceID);
		OGL::uniform1i(location, unit);
    }
}


GLuint GLSLProgram::checkCompileStatus(GLuint shader, GLint *status)
{
	OGL::getShaderiv(shader, GL_COMPILE_STATUS, status);

    if (!(*status))
    {
        char log[2048];
        int len;
		OGL::getShaderInfoLog(shader, 2048, (GLsizei *)&len, log);
        printf("Error: shader(%04d), Info log: %s\n", (int)shader, log);
		OGL::deleteShader(shader);
        return 0;
    }

    return 1;
}


GLuint GLSLProgram::compileProgram(const string &vertexSourcePath, const string &fragmentSourcePath, const string &geometrySourcePath,
                            GLenum gsInput, GLenum gsOutput)
{
    GLuint vertexShader   = OGL::createShader(GL_VERTEX_SHADER);
    GLuint fragmentShader = OGL::createShader(GL_FRAGMENT_SHADER);

    GLint compiled = 0;

	GLuint program = OGL::createProgram();

	if (!vertexSourcePath.empty())
	{
		string vertexSource = readFile(vertexSourcePath);
		const char* vertexSourcePointer = vertexSource.c_str();
		OGL::shaderSource(vertexShader, 1, &vertexSourcePointer, 0);
		OGL::compileShader(vertexShader);
		OGL::attachShader(program, vertexShader);

		if (checkCompileStatus(vertexShader, &compiled) == 0)
		{
			printf("<compileProgram compilation error with vertexShader>:\n");
			printf("%s\n", vertexSourcePath.c_str());
			return 0;
		}
	}

	if (!fragmentSourcePath.empty())
	{
		string fragmentSource = readFile(fragmentSourcePath);
		const char* fragmentSourcePointer = fragmentSource.c_str();
		OGL::shaderSource(fragmentShader, 1, &fragmentSourcePointer, 0);
		OGL::compileShader(fragmentShader);
		OGL::attachShader(program, fragmentShader);

		if (checkCompileStatus(fragmentShader, &compiled) == 0)
		{
			printf("<compileProgram compilation error with fragmentShader>:\n");
			printf("%s\n", fragmentSourcePath.c_str());
			return 0;
		}
	}

    if (!geometrySourcePath.empty())
    {
		string geometrySource = readFile(geometrySourcePath);
		const char* geometrySourcePointer = geometrySource.c_str();

        GLuint geomShader = OGL::createShader(GL_GEOMETRY_SHADER);
		OGL::shaderSource(geomShader, 1, &geometrySourcePointer, 0);
		OGL::compileShader(geomShader);
		OGL::getShaderiv(geomShader, GL_COMPILE_STATUS, (GLint *)&compiled);

        if (checkCompileStatus(geomShader, &compiled) == 0)
        {
            printf("<compileProgram compilation error with geomShader>:\n");
            printf("%s\n", geometrySourcePath.c_str());
            return 0;
        }

		OGL::attachShader(program, geomShader);
    }

	OGL::linkProgram(program);

    // check if program linked
    GLint success = 0;
	OGL::getProgramiv(program, GL_LINK_STATUS, &success);

    if (success == GL_FALSE)
    {
		GLint maxLength = 0;
		OGL::getProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		//The maxLength includes the NULL character
		std::vector<GLchar> temp(maxLength);
		OGL::getProgramInfoLog(program, maxLength, &maxLength, &temp[0]);
        fprintf(stderr, "Failed to link program:\n%s\n", &temp[0]);
		OGL::deleteProgram(program);
        program = 0;
		system("pause");
        exit(EXIT_FAILURE);
    }

    return program;
}

string GLSLProgram::readFile(const string& filePath)
{
	std::string shaderCode;
	std::ifstream shaderStream(filePath, std::ios::in);
	if (shaderStream.is_open()) {
		std::string Line = "";
		while (getline(shaderStream, Line))
			shaderCode += "\n" + Line;
		shaderStream.close();
		return shaderCode;
	}
	else {
		printf("Impossible to open %s. Are you in the right directory?\n", filePath.c_str());
		return "";
	}
}

