#pragma once

#include "Resource.hpp"
#include "Texture.hpp"
#include "VolumeTexture.hpp"
#include "GLSLProgram.hpp"

#include "assimp\scene.h"

#include <map>
#include <string>
#include <stack>
#include <memory>

#include "glm\glm.hpp"

namespace MagicEngine
{
	class GameObject;

	struct AssimpMaterial
	{
		int _materialIndex;
		std::string _path;
	};

	struct ModelData
	{
		ModelData(std::vector<glm::vec3>& vertexBuffer, std::vector<glm::vec2>& uvBuffer, std::vector<glm::vec3>& normalBuffer,
			std::vector<glm::vec3>& tangents, std::string& name, float& width, float& height, float& depth, glm::vec3& _minBB, 
			glm::vec3& _maxBB, int& materialIndex);
		~ModelData() {}

		VertexBuffer _vertexBuffer;
		UVBuffer _uvBuffer;
		NormalBuffer _normalBuffer;
		TangentBuffer _tangentBuffer;
		std::string _name;
		float _width;
		float _height;
		float _depth;
		AssimpMaterial _material;
		glm::vec3 _minBB;
		glm::vec3 _maxBB;
		glm::mat4 _modelMatrix;
		std::vector<glm::vec3> _aabbVertices;

	};

	class SimpleTree
	{
	public:

		SimpleTree(std::string name);
		virtual ~SimpleTree();

		void setParent(SimpleTree* parent);
		virtual const SimpleTree* getParent();
		virtual void addChild(SimpleTree* child);

		SimpleTree* _parent = nullptr;
		std::vector<SimpleTree*> _children;
		std::string _name;
		glm::mat4 _modelMartix;
		std::string _materialPath;
	protected:
	};

	class ResourceManager
	{
	public:
		static void loadCompleteScene(std::vector<MagicEngine::GameObject*>* gameObjects, std::string modelPath);
		// returns the same pointer for the same inputs.
		static std::shared_ptr<GLSLProgram> getShader(std::string vertexShaderPath, std::string fragmentShaderPath, std::string geometryShaderPath = "");
		static std::shared_ptr<ModelData> getModelData(std::string name);
		static std::shared_ptr<Texture> getTexture(std::string texturePath);
		static std::shared_ptr<VolumeTexture> getVolumeTexture(std::string type);

		static std::shared_ptr<ModelData> getSphereModelData() { return s_sphere; }
		static std::shared_ptr<VertexBuffer> getQuad();
		static std::shared_ptr<VertexBuffer> getUnitCube();
		
		static void clearCache();

	private:
		static void handleDuplicatedMeshes(SimpleTree *vectorOfMeshes);
		static void loadScene(const aiScene *scene, aiNode *node, SimpleTree *parent);
		static void addAllMeshesToGameObjectArray(GameObject* rootObj, SimpleTree *vectorOfMeshes);

		static std::map<std::string, std::shared_ptr<GLSLProgram>> s_shaderProgramMap;
		static std::map<std::string, std::shared_ptr<ModelData>> s_modelDataMap;
		static std::map<std::string, std::shared_ptr<Texture>> s_textureMap;
		static std::map<std::string, std::shared_ptr<VolumeTexture>> s_volumeTextureMap;

		static std::shared_ptr<ModelData> s_sphere;
		static std::shared_ptr<VertexBuffer> s_quad;
		static std::shared_ptr<VertexBuffer> s_unitCube;

		static void setSceneFlags(GameObject* staticGeometry);
	};

}

