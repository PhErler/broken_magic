#include "VolumeTexture.hpp"

#include "Noise.h"
#include "VoxelGrid.h"
#include "OGL.hpp"

#include "glm\glm.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;
using namespace glm;
using namespace MagicEngine;

namespace
{
	const int BYTES_PER_TEXEL = 4;
}

VolumeTexture::VolumeTexture(std::string file)
{
	tex3D tex;

	//createWindowTestTexture(tex);
	//createBricksTestTexture(tex);
	//createSlicesTestTexture(tex);
	//createShadowTestTexture(tex);
	//createWoolTexture(tex, type);
	//createCloudTexture(tex);

	//saveTextureToFile("resources\\cloud2.txt", tex);
	loadTextureFromFile(file, tex);

	// ask for enough memory for the texels and make sure we got it before proceeding
	int width = tex.size();
	int height = tex[0].size();
	int depth = tex[0][0].size();
	vector<unsigned char> texels(width * height * depth * BYTES_PER_TEXEL);

#define LAYER(r)	(width * height * r * BYTES_PER_TEXEL)
	// 2->1 dimension mapping function
#define TEXEL2(s, t)	(BYTES_PER_TEXEL * (s * width + t))
	// 3->1 dimension mapping function
#define TEXEL3(s, t, r)	(TEXEL2(s, t) + LAYER(r))

	for (int x = 0; x < width; ++x)
	{
		for (int y = 0; y < height; ++y)
		{
			for (int z = 0; z < depth; ++z)
			{
				texels[TEXEL3(x, y, z) + 0] = tex[x][y][z];
				texels[TEXEL3(x, y, z) + 1] = 0;
				texels[TEXEL3(x, y, z) + 2] = 0;
				texels[TEXEL3(x, y, z) + 3] = 0;
			}
		}
	}
	

	// request 1 texture name from OpenGL
	OGL::genTextures(1, &_resourceID);
	// tell OpenGL we're going to be setting up the texture name it gave us	
	ITexture::bindTextureUnit(GL_TEXTURE_3D, _resourceID);
	// when this texture needs to be shrunk to fit on small polygons, use linear interpolation of the texels to determine the color
	OGL::texParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	// when this texture needs to be magnified to fit on a big polygon, use linear interpolation of the texels to determine the color
	OGL::texParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// we want the texture to repeat over the S axis, so if we specify coordinates out of range we still get textured.
	OGL::texParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	// same as above for T axis
	OGL::texParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	// same as above for R axis
	OGL::texParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
	// this is a 3d texture, level 0 (max detail), GL should store it in RGB8 format, its WIDTHxHEIGHTxDEPTH in size, 
	// it doesnt have a border, we're giving it to GL in RGB format as a series of unsigned bytes, and texels is where the texel data is.
	OGL::texImage3D(GL_TEXTURE_3D, 0, GL_R8, width, height, depth, 0, GL_RGBA, GL_UNSIGNED_BYTE, &texels[0]);

	ITexture::bindTextureUnit(GL_TEXTURE_3D, 0);
}

VolumeTexture::~VolumeTexture()
{
}

void VolumeTexture::activate(unsigned int textureSampler)
{
	activateTextureUnit(textureSampler);
	ITexture::bindTextureUnit(GL_TEXTURE_3D, _resourceID);
}

void VolumeTexture::deactivate(unsigned int textureSampler)
{
	activateTextureUnit(textureSampler);
	ITexture::bindTextureUnit(GL_TEXTURE_3D, 0);
}

/*
void VolumeTexture::createWindowTestTexture(tex3D &tex)
{
	const int WIDTH = 10;
	const int HEIGHT = 10;
	const int DEPTH = 10;

	tex = tex3D(WIDTH);
	for (int x = 0; x < WIDTH; ++x)
	{
		tex[x] = vector<vector<vec4>>(HEIGHT);
		for (int y = 0; y < HEIGHT; ++y)
		{
			tex[x][y] = vector<vec4>(DEPTH);
			for (int z = 0; z < DEPTH; ++z)
			{
				if (x == 0 || x == WIDTH - 1 || y == 0 || y == HEIGHT - 1)
				{
					tex[x][y][z] = vec4(0, 0, 0, 0.9f);
				}
				else
				{
					tex[x][y][z] = vec4(1, 1, 1, 0.01f);
				}
			}
		}
	}
}

void VolumeTexture::createBricksTestTexture(tex3D &tex)
{
	const int WIDTH = 5;
	const int HEIGHT = 5;
	const int DEPTH = 5;

	tex = tex3D(WIDTH);
	for (int x = 0; x < WIDTH; ++x)
	{
		tex[x] = vector<vector<vec4>>(HEIGHT);
		for (int y = 0; y < HEIGHT; ++y)
		{
			tex[x][y] = vector<vec4>(DEPTH);
			for (int z = 0; z < DEPTH; ++z)
			{
				tex[x][y][z] = vec4(float(x) / float(WIDTH),
					float(y) / float(HEIGHT),
					float(z) / float(DEPTH),
					0.02f);
			}
		}
	}

	// lines through cube
	for (int x = 0; x < WIDTH; ++x)
	{
		tex[x][0][0] = vec4(1.0f, 0.0f, 0.0f, 0.8f);
	}
	for (int y = 0; y < HEIGHT; ++y)
	{
		tex[1][y][1] = vec4(0.0f, 1.0f, 0.0f, 0.8f);
	}
	for (int z = 0; z < DEPTH; ++z)
	{
		tex[2][2][z] = vec4(0.0f, 0.0f, 1.0f, 0.8f);
	}
}

void VolumeTexture::createSlicesTestTexture(tex3D &tex)
{
	const int WIDTH = 10;
	const int HEIGHT = 10;
	const int DEPTH = 10;

	tex = tex3D(WIDTH);
	for (int x = 0; x < WIDTH; ++x)
	{
		tex[x] = vector<vector<vec4>>(HEIGHT);
		for (int y = 0; y < HEIGHT; ++y)
		{
			tex[x][y] = vector<vec4>(DEPTH);
			for (int z = 0; z < DEPTH; ++z)
			{
				if (z % 2 == 0)
				{
					tex[x][y][z] = vec4(0, 0, 0, 0.9f);
				}
				else
				{
					tex[x][y][z] = vec4(1, 1, 1, 0.01f);
				}
			}
		}
	}
}

void VolumeTexture::createShadowTestTexture(tex3D &tex)
{
	const int WIDTH = 10;
	const int HEIGHT = 10;
	const int DEPTH = 10;

	tex = tex3D(WIDTH);
	for (int x = 0; x < WIDTH; ++x)
	{
		tex[x] = vector<vector<vec4>>(HEIGHT);
		for (int y = 0; y < HEIGHT; ++y)
		{
			tex[x][y] = vector<vec4>(DEPTH);
			for (int z = 0; z < DEPTH; ++z)
			{
				if (x == WIDTH - 2 && (y > HEIGHT / 4 && y < HEIGHT * 3 / 4)
								   && (z > DEPTH / 4 && z < DEPTH * 3 / 4))
				{
					tex[x][y][z] = vec4(0.5f, 0.5f, 1.0f, 1.0f);
				}
				else if (x == 1)
				{
					tex[x][y][z] = vec4(1.0f, 0.5f, 0.5f, 1.0f);
				}
				else
				{
					tex[x][y][z] = vec4(0.25f, 0.5f, 0.5f, 0.01f);
				}
			}
		}
	}
}

void VolumeTexture::createWoolTexture(tex3D &tex, int playerNumber)
{
	const int WIDTH = 100;
	const int HEIGHT = 100;
	const int DEPTH = 100;

	const float InnerBorderDist = 0.3f;
	const float MiddleBorderDist = 0.4f;

	vec3 color(0.5f, 0.5f, 0.5f);
	if (playerNumber == 0)
	{
		color = vec3(0.8f, 0.8f, 0.8f); // white
	}
	else if (playerNumber == 1)
	{
		color = vec3(0.2f, 0.2f, 0.2f); // black
	}
	else if (playerNumber == 2)
	{
		color = vec3(0.5f, 0.2f, 0.2f); // red
	}
	else if (playerNumber == 3)
	{
		color = vec3(0.4f, 0.3f, 0.0f); // brown
	}

	// shells of increasing opacity to the center
	tex = tex3D(WIDTH);
	for (int x = 0; x < WIDTH; ++x)
	{
		tex[x] = vector<vector<vec4>>(HEIGHT);
		for (int y = 0; y < HEIGHT; ++y)
		{
			tex[x][y] = vector<vec4>(DEPTH);
			for (int z = 0; z < DEPTH; ++z)
			{
				float distX = abs(float(x) - float(WIDTH) / 2) / float(WIDTH) * 2;
				float distY = abs(float(y) - float(HEIGHT) / 2) / float(HEIGHT) * 2;
				float distZ = abs(float(z) - float(DEPTH) / 2) / float(DEPTH) * 2;
				float distToCenter = sqrtf(distX*distX + distY*distY + distZ*distZ);

				if (distToCenter < InnerBorderDist)
				{
					float randomFactor = float(rand() % 1000) * 0.001f * 0.2f;
					tex[x][y][z] = vec4(color.r, color.g, color.b, 0.6f + randomFactor);
				}
				else if (distToCenter < MiddleBorderDist)
				{
					float linearFactor = (distToCenter - InnerBorderDist) / (MiddleBorderDist - InnerBorderDist);
					float randomFactor = float(rand() % 1000) * 0.001f * 0.2f;
					tex[x][y][z] = vec4(color.r, color.g, color.b, 0.6f - linearFactor * 0.6f + randomFactor);
				}
				else
				{
					tex[x][y][z] = vec4(color.r, color.g, color.b, 0.0f);
				}
			}
		}
	}
}
*/

void MagicEngine::VolumeTexture::createCloudTexture(tex3D & tex)
{
	//const int Seed = 42; // sqhere cloud
	const int Seed = 85;
	const int NumVoxels = 75;
	//const int NumVoxels = 300;
	const float VoxelSize = 1 / (float)NumVoxels;
	const float DefaultDensity = 0;

	const float Sharpness = 0.7f; // Cloud fuzziness and sharpness
	const float Density = 15.9f; // Cloud density
	//const float Density = 0.9f; // Cloud density
	const float Persistance = 0.8f;
	const int Octaves = 10;
	const float FlattingFactor = 1;

	VoxelGrid *grid = new VoxelGrid(VoxelSize, NumVoxels, NumVoxels, NumVoxels, DefaultDensity);
	Noise *noise = new Noise(Persistance, Octaves, Seed); // Seed = 188

	// Get max length from grid center to grid surface
	vec3 center(grid->getMaxX()*0.5f, grid->getMaxY()*0.5f, grid->getMaxZ()*0.5f);
	float max_distance = length(vec3(0.5f, 0.5f, 0.5f) - center);
	float max_ratio = 1 / max_distance;

	// Generate random density
	for (int kk = 0; kk < grid->getMaxZ(); kk++) {
		for (int jj = 0; jj < grid->getMaxY(); jj++) {
			for (int ii = 0; ii < grid->getMaxX(); ii++) {
				float cloud = noise->PerlinNoise3(ii*grid->getVoxelSize(), jj*grid->getVoxelSize(), kk*grid->getVoxelSize());

				// we want flat clouds
				vec3 voxel(vec3(ii * FlattingFactor, jj, kk));
				float distance = length(voxel - center); // Distance from current voxel to grid center
				float cover = distance * max_ratio + 0.3f /*+ jj * max_ratio*/; // Amount of cloud (0.93) + we want flat clouds

				cloud = cloud - cover;
				if (cloud < 0) cloud = 0; 
				cloud *= Density;
				cloud = 1.0f - powf(Sharpness, cloud);

				grid->setVoxelDensity(ii, jj, kk, cloud);

				vec3 color(1, 1, 1);
				grid->setVoxelColor(ii, jj, kk, color);
			}
		}
	}

	// Set voxel grid
	tex = tex3D(NumVoxels);
	for (int x = 0; x < NumVoxels; ++x)
	{
		tex[x] = vector<vector<unsigned char>>(NumVoxels);
		for (int y = 0; y < NumVoxels; ++y)
		{
			tex[x][y] = vector<unsigned char>(NumVoxels);
			for (int z = 0; z < NumVoxels; ++z)
			{
				float density = grid->getVoxelDensity(x, y, z);
				unsigned char densityToSave = (unsigned char)(density * 255);
				tex[x][y][z] = densityToSave;
			}
		}
	}

	delete grid;
	grid = nullptr;

	delete noise;
	noise = nullptr;
}

void VolumeTexture::saveTextureToFile(std::string fileName, tex3D &tex)
{
	ofstream keyPointFile;
	keyPointFile.open(fileName, ios::out | ios::trunc /*| ios::binary*/);
	if (!keyPointFile.good())
	{
		std::cout << "Exception opening volume texture file " << fileName << endl;
		return;
	}

	int width = tex.size();
	int height = tex[0].size();
	int depth = tex[0][0].size();

	keyPointFile << width << " ";
	keyPointFile << height << " ";
	keyPointFile << depth << " ";
	keyPointFile << 42 << endl;

	for (int x = 0; x < width; ++x)
	{
		for (int y = 0; y < height; ++y)
		{
			for (int z = 0; z < depth; ++z)
			{
				unsigned char voxel = tex[x][y][z];
				if (voxel != 0)
				{
					keyPointFile << x << " ";
					keyPointFile << y << " ";
					keyPointFile << z << " ";
					keyPointFile << (unsigned int)voxel << endl; // cast to int to prevent output as ascii char
				}
			}
		}
	}

	keyPointFile.close();

	std::cout << "Saved volume texture to the file " << fileName << endl;
}

void VolumeTexture::loadTextureFromFile(std::string fileName, tex3D &tex)
{
	ifstream textureFile;
	textureFile.open(fileName, ios::in /*| ios::binary*/);
	if (!textureFile.good())
	{
		std::cout << "Exception opening volume texture file " << fileName << endl;
		return;
	}

	tex.clear();

	string temp;
	int readx = 0;
	int ready = 0;
	int readz = 0;
	int reada = 0;
	bool dimension = true;

	int width;
	int height;
	int depth;

	int x = 0;
	int y = 0;
	int z = 0;

	while (getline(textureFile, temp)) {
		istringstream iss(temp);

		int valueCounter = 0;

		while (getline(iss, temp, ' ')) {
			istringstream ist(temp);

			switch (valueCounter)
			{
			case 0:
				ist >> readx;
				break;
			case 1:
				ist >> ready;
				break;
			case 2:
				ist >> readz;
				break;
			case 3:
				ist >> reada;
				break;
			default:
				break;
			}

			++valueCounter;
		}

		if (dimension)
		{
			width = readx;
			height = ready;
			depth = readz;

			// init texture
			tex = tex3D(width);
			for (int x = 0; x < width; ++x)
			{
				tex[x] = vector<vector<unsigned char>>(height);
				for (int y = 0; y < height; ++y)
				{
					tex[x][y] = vector<unsigned char>(depth);
				}
			}

			dimension = false;
		}
		else
		{
			// set values
			reada = clamp(reada, 0, 255);
			tex[readx][ready][readz] = (unsigned char)reada;
		}
	}

	textureFile.close();

	std::cout << "Read volume texture from the file " << fileName << endl;
}
