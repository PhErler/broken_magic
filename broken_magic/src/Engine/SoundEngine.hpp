#pragma once
#include <vector>

/*SOUND*/
#include <fmod.h>
#include <fmod_errors.h>
#include <fmod.hpp>


namespace MagicEngine
{
	class SoundEngine
	{
	public:
		SoundEngine();
		~SoundEngine();

		void play();
		void pause();

	private:

		FMOD::Channel* channel;
		FMOD::Sound* sound = nullptr;
		FMOD::System* system = nullptr;
		FMOD_RESULT backgrondSound;
	};
}
