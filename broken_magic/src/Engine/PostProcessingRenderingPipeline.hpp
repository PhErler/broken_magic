#pragma once

#include "IRenderPipeline.hpp"

#include "Primitives.hpp"

#include <vector>

namespace MagicEngine
{
	class Light;

	class PostProcessingRenderingPipeline : public IRenderPipeline
	{
	public:
		PostProcessingRenderingPipeline(Camera* cam);
		~PostProcessingRenderingPipeline();


		virtual void render() override;
		virtual void draw(GameObject* gameObject) override;

		static float s_focalDistance;
		static float s_focalRange;
		static float s_exposure;

	protected:

		enum DeferredRenderingPipeline_TEXTURE_TYPE {
			DepthOfFieldRenderingPipeline_TEXTURE_TYPE_DEPTH = 0,
			DepthOfFieldRenderingPipeline_TEXTURE_TYPE_GAUSSIAN = 1,
			DepthOfFieldRenderingPipeline_TEXTURE_TYPE_BLENDING = 2,
			DepthOfFieldRenderingPipeline_NUM_TEXTURES = 3,
		};

		enum BloomRenderingPipeline_TEXTURE_TYPE {
			BloomRenderingPipeline_TEXTURE_TYPE_BRIGHT_COLOR = 0,
			BloomRenderingPipeline_TEXTURE_TYPE_GAUSSIAN = 1,
			BloomRenderingPipeline_TEXTURE_TYPE_BLENDING = 2,
			BloomRenderingPipeline_NUM_TEXTURES = 3,
		};

		int _width  = 0;
		int _height = 0;

		std::shared_ptr<VertexBuffer> _quadVertexBuffer;

		RenderTexture* _dofDepthTex;
		RenderTexture* _dofGaussianTex;
		RenderTexture* _bloomBrightnessTex;
		RenderTexture* _bloomGaussianTex;
		RenderTexture* _compositeTex;

		FrameBuffer* _dofDepthFbo;
		FrameBuffer* _dofGaussianFbo;
		FrameBuffer* _bloomBrightnessFbo;
		FrameBuffer* _bloomGaussianFbo;
		FrameBuffer* _compositeFbo;

		std::shared_ptr<GLSLProgram> _blurValueProgram;
		std::shared_ptr<GLSLProgram> _gaussianProgram;
		//std::shared_ptr<GLSLProgram> _dofFinalProgram;

		std::shared_ptr<GLSLProgram> _brightColorProgram;
		//std::shared_ptr<GLSLProgram> _blendingProgram;

		std::shared_ptr<GLSLProgram> _finalProgram;

		struct
		{
			GLint Tex0 = 0;
			GLint focalDistance = 0;
			GLint focalRange = 0;
			GLint nearFar = 0;

		} _blurValueUniformLocations;

		struct
		{
			GLint Tex0 = 0;
			GLint horizontal = 0;
			GLint width = 0;
			GLint height = 0;

		} _gaussianUniformLocations;

		/*struct
		{
			GLint colorTex = 0;
			GLint blurValueTex = 0;
			GLint gaussianTex = 0;

		} _dofFinalProgramUniformLocations;
		*/
		struct
		{
			GLint Tex0 = 0;

		} _brightColorUniformLocations;
		/*
		struct
		{
			GLint Tex0 = 0;
			GLint Tex1 = 0;

		} _blendingUniformLocations;
		*/
		struct
		{
			GLint color = 0;
			GLint dof_blurValueTex = 0;
			GLint dof_gaussianTex = 0;
			GLint bloom_gaussianTex = 0;
			GLint exposure = 0;

		} _finalProgramUniformLocations;
	private:

	};

}