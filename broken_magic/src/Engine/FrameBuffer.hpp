#pragma once

#include "Resource.hpp"

#include <vector>

namespace MagicEngine
{
	class RenderTexture;

	class FrameBuffer : public IResource
	{
	public:
		FrameBuffer(std::vector<RenderTexture*>* colorTextures, const std::vector<GLenum> &drawBufferAttachments, RenderTexture* depthTexture, bool withStencil);
		virtual ~FrameBuffer();

		virtual void activate() override;
		void useAssignedAttachments();
		virtual void activateForWrite(const std::vector<GLenum> &drawBufferAttachments);
		virtual void activateForRead(int colorAttachment);
		virtual void deactivate() override;

		virtual void clear(bool clearColor, bool clearDepth, bool clearStencil,
			float r = 0, float g = 0, float b = 0, float a = 0, float depth = 1, GLint stencil = 0);

	protected:
#ifdef _DEBUG
		static GLuint s_boundDrawBuffer;
#endif

		// if you want this from the outside, better make more FBOs, instead
		void updateRenderTexture(std::vector<RenderTexture*>* colorTextures, const std::vector<GLenum> &drawBufferAttachments, RenderTexture* depthTexture, bool withStencil);

#ifdef _DEBUG
		void markTexturesForWrite(bool write);
#endif

		std::vector<GLenum> _drawBuffers;
		GLuint _width = 0;
		GLuint _height = 0;

#ifdef _DEBUG
		std::vector<RenderTexture*> _colorTextures;
		RenderTexture* _depthTexture = nullptr;
#endif
	};
}
