#include "Texture.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <iostream>

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include "stb_image.h"
#include "OGL.hpp"

using namespace std;
using namespace MagicEngine;

Texture::Texture(std::string path)
{
	if (!path.empty())
	{
		loadImage(path);
	}
}

void Texture::loadImage(std::string path, int newMinSamplingMethod, int newMaxSamplingMethod)
{
	printf("Reading image %s\n", path.c_str());

	string last4Chars = path.substr(path.length() - 4, string::npos);
	// to lower case
	std::transform(last4Chars.begin(), last4Chars.end(), last4Chars.begin(), ::tolower);

	if (last4Chars == ".dds")
	{
		_resourceID = Texture::loadDDS(path.c_str(), newMinSamplingMethod, newMaxSamplingMethod);
	}
	else if (last4Chars == ".bmp")
	{
		_resourceID = Texture::loadBMP_custom(path.c_str(), newMinSamplingMethod, newMaxSamplingMethod);
	}
	else if (last4Chars == ".png" || last4Chars == ".jpg")
	{
		_resourceID = Texture::loadPNG_JPG(path.c_str(), newMinSamplingMethod, newMaxSamplingMethod);
	}
	else
	{
		_resourceID = 0;
		cout << "WARNING: Unsupported texture format: \"" << last4Chars << "\" of file: \"" << path << "\"";
	}
}

void Texture::unloadImage()
{
	if (_resourceID)
	{
		OGL::deleteTextures(1, &_resourceID);
	}
}

Texture::~Texture()
{
	unloadImage();
}

void Texture::activate(unsigned int textureSampler)
{
	// Bind our texture in a Texture Unit
	activateTextureUnit(textureSampler);
	ITexture::bindTextureUnit(GL_TEXTURE_2D, _resourceID);
}

void Texture::deactivate(unsigned int textureSampler)
{
	activateTextureUnit(textureSampler);
	ITexture::bindTextureUnit(GL_TEXTURE_2D, 0);
}

GLuint Texture::loadPNG_JPG(const char * imagepath, int newMinSamplingMethod, int newMaxSamplingMethod) {

	int width, height, numComponents;

	/*Load texture data*/
	unsigned char* imageData = stbi_load(imagepath, &width, &height, &numComponents, 4);

	if (imageData == NULL)
		std::cerr << "Error: Texture loading failed for texture: " << imagepath << std::endl;

	GLuint textureID;
	OGL::genTextures(1, &textureID);
	ITexture::bindTextureUnit(GL_TEXTURE_2D, textureID);
	// All upcoming GL_TEXTURE_2D operations now have effect on this texture object
	// Set the texture wrapping parameters
	OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping to GL_REPEAT (usually basic wrapping method)
	OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// Set texture filtering parameters
	OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, newMaxSamplingMethod);
	OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, newMinSamplingMethod);
	// Create texture and generate mipmaps
	OGL::texImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
	OGL::generateMipmap(GL_TEXTURE_2D); // Generate MipMaps

									 /*delete Texture when we dont need it anymore*/
	stbi_image_free(imageData);
	ITexture::bindTextureUnit(GL_TEXTURE_2D, 0);

	return textureID;
}

// basing on the opengl-tutorial from
// http://www.opengl-tutorial.org/
GLuint Texture::loadBMP_custom(string imagepath, int newMinSamplingMethod, int newMaxSamplingMethod)
{
	vector<char> data;
	int width = 0;
	int height = 0;

	_path = imagepath;

	loadBMP(imagepath, data, width, height);

	if (data.empty())
	{
		return 0;
	}

	// Create one OpenGL texture
	GLuint textureID;
	OGL::genTextures(1, &textureID);
	
	// "Bind" the newly created texture : all future texture functions will modify this texture
	ITexture::bindTextureUnit(GL_TEXTURE_2D, textureID);

	// Give the image to OpenGL
	OGL::texImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, &data[0]);

	// OpenGL has now copied the data. Free our own version
	data.clear();

	// ... nice trilinear filtering.
	OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, newMaxSamplingMethod);
	OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, newMinSamplingMethod);
	OGL::generateMipmap(GL_TEXTURE_2D);

	ITexture::bindTextureUnit(GL_TEXTURE_2D, 0);

	// Return the ID of the texture we just created
	return textureID;
}

#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII

// basing on the opengl-tutorial from
// http://www.opengl-tutorial.org/
GLuint Texture::loadDDS(const char * imagepath, int newMinSamplingMethod, int newMaxSamplingMethod){

	unsigned char header[124];

	FILE *fp;

	_path = imagepath;
 
	/* try to open the file */ 
	fp = fopen(imagepath, "rb"); 
	if (fp == NULL){
		printf("%s could not be opened. Are you in the right directory?\n", imagepath);
		return 0;
	}
   
	/* verify the type of file */ 
	char filecode[4]; 
	fread(filecode, 1, 4, fp); 
	if (strncmp(filecode, "DDS ", 4) != 0) { 
		fclose(fp); 
		return 0; 
	}
	
	/* get the surface desc */ 
	fread(&header, 124, 1, fp); 

	unsigned int height      = *(unsigned int*)&(header[8 ]);
	unsigned int width	     = *(unsigned int*)&(header[12]);
	unsigned int linearSize	 = *(unsigned int*)&(header[16]);
	unsigned int mipMapCount = *(unsigned int*)&(header[24]);
	unsigned int fourCC      = *(unsigned int*)&(header[80]);

 
	unsigned char * buffer;
	unsigned int bufsize;
	/* how big is it going to be including all mipmaps? */ 
	bufsize = mipMapCount > 1 ? linearSize * 2 : linearSize; 
	buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char)); 
	fread(buffer, 1, bufsize, fp); 
	/* close the file pointer */ 
	fclose(fp);

	unsigned int components  = (fourCC == FOURCC_DXT1) ? 3 : 4; 
	unsigned int format;
	switch(fourCC) 
	{ 
	case FOURCC_DXT1: 
		format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT; 
		break; 
	case FOURCC_DXT3: 
		format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT; 
		break; 
	case FOURCC_DXT5: 
		format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT; 
		break; 
	default: 
		free(buffer); 
		return 0; 
	}

	// Create one OpenGL texture
	GLuint textureID;
	OGL::genTextures(1, &textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	ITexture::bindTextureUnit(GL_TEXTURE_2D, textureID);
	OGL::pixelStorei(GL_UNPACK_ALIGNMENT,1);	
	
	unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16; 
	unsigned int offset = 0;

	/* load the mipmaps */ 
	for (unsigned int level = 0; level < mipMapCount && (width || height); ++level) 
	{ 
		unsigned int size = ((width+3)/4)*((height+3)/4)*blockSize; 
		OGL::compressedTexImage2D(GL_TEXTURE_2D, level, format, width, height,  
			0, size, buffer + offset); 
	 
		offset += size; 
		width  /= 2; 
		height /= 2; 

		// Deal with Non-Power-Of-Two textures. This code is not included in the webpage to reduce clutter.
		if(width < 1) width = 1;
		if(height < 1) height = 1;

	} 

	free(buffer); 

	ITexture::bindTextureUnit(GL_TEXTURE_2D, 0);

	return textureID;
}
