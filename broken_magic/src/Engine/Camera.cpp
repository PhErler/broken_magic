#include "Camera.hpp"

#include "MagicEngineMain.hpp"
#include "RenderTexture.hpp"
#include "FrameBuffer.hpp"
#include "Resource.hpp"
#include "ResourceManager.hpp"
#include "OGL.hpp"

#include "glm\gtc\matrix_transform.hpp"

using namespace std;
using namespace glm;
using namespace MagicEngine;

Camera::Camera()
	: _position(0, 2, 25)	// Initial position : on +Z
	, _viewDir(1, 0, 0)
	, _viewRight(0, 0, 1)
{
	// quad
	float leftNDC = -1;
	float rightNDC = 1;
	float topNDC = 1;
	float bottomNDC = -1;

	vector<vec3> vertices; // plane made of 2 triangles
	vertices.push_back(vec3(leftNDC, bottomNDC, 0.0f));
	vertices.push_back(vec3(rightNDC, bottomNDC, 0.0f));
	vertices.push_back(vec3(leftNDC, topNDC, 0.0f));
	vertices.push_back(vec3(leftNDC, topNDC, 0.0f));
	vertices.push_back(vec3(rightNDC, bottomNDC, 0.0f));
	vertices.push_back(vec3(rightNDC, topNDC, 0.0f));
	_quadVertexBuffer = new VertexBuffer(vertices);

	GLFWwindow* window = MagicEngineMain::getWindow();

	const CommandLineParams& commandLineParams = MagicEngineMain::getCommandLineParams();

	_renderColorTexture = new RenderTexture(commandLineParams._xRes, commandLineParams._yRes, RenderTexture::RGBA);
	_renderDepthTexture = new RenderTexture(commandLineParams._xRes, commandLineParams._yRes, RenderTexture::DepthWithStencil);
	vector<RenderTexture*> colorTextures;
	colorTextures.push_back(_renderColorTexture);

	vector<GLenum> attachments = { GL_COLOR_ATTACHMENT0 };
	_renderBuffer = new FrameBuffer(&colorTextures, attachments, _renderDepthTexture, true);

	float windowAspect = static_cast<float>(commandLineParams._xRes) / static_cast<float>(commandLineParams._yRes);

	_projectionMatrix = glm::perspective(_foV, windowAspect, _zNearPlane, _zFarPlane);
}

Camera::~Camera()
{
	if (MagicEngineMain::getCamera() == this)
	{
		MagicEngineMain::setCamera(nullptr);
	}

	if (_renderColorTexture)
	{
		delete _renderColorTexture;
	}

	if (_renderDepthTexture)
	{
		delete _renderDepthTexture;
	}

	if (_renderBuffer)
	{
		delete _renderBuffer;
	}
}

// basing on the opengl-tutorial from
// http://www.opengl-tutorial.org/
void Camera::update(double dt)
{
	GLFWwindow* window = MagicEngineMain::getWindow();
	GLuint xRes = MagicEngineMain::getCommandLineParams()._xRes;
	GLuint yRes = MagicEngineMain::getCommandLineParams()._yRes;

	float deltaTime = float(dt);

	// Get mouse position
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	// Reset mouse position for next frame
	glfwSetCursorPos(window, xRes / 2, yRes / 2);

	// Compute new orientation
	_horizontalAngle += _mouseSpeed * float(xRes / 2 - xpos);
	_verticalAngle += _mouseSpeed * float(yRes / 2 - ypos);

	// Direction : Spherical coordinates to Cartesian coordinates conversion
	_viewDir = vec3(
		cos(_verticalAngle) * sin(_horizontalAngle),
		sin(_verticalAngle),
		cos(_verticalAngle) * cos(_horizontalAngle)
		);

	// Right vector
	_viewRight = vec3(
		sin(_horizontalAngle - 3.14f / 2.0f),
		0,
		cos(_horizontalAngle - 3.14f / 2.0f)
		);

	// Up vector
	glm::vec3 up = glm::cross(_viewRight, _viewDir);

	float hurryFactor = 1;
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
		hurryFactor = 10;
	}

	// Move forward
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS){
		_position += _viewDir * deltaTime * _speed * hurryFactor;
	}
	// Move backward
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS){
		_position -= _viewDir * deltaTime * _speed * hurryFactor;
	}
	// Strafe right
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS){
		_position += _viewRight * deltaTime * _speed * hurryFactor;
	}
	// Strafe left
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS){
		_position -= _viewRight * deltaTime * _speed * hurryFactor;
	}
	// Strafe up
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		_position += cross(_viewRight, _viewDir) * deltaTime * _speed * hurryFactor;
	}
	// Strafe down
	if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
		_position -= cross(_viewRight, _viewDir) * deltaTime * _speed * hurryFactor;
	}

	// Camera matrix
	_viewMatrix = glm::lookAt(
		_position,				// Camera is here
		_position + _viewDir,	// and looks here : at the same position, plus "direction"
		up						// Head is up (set to 0,-1,0 to look upside-down)
	);

}

void Camera::renderToScreen()
{
	OGL::bindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	OGL::clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	_renderBuffer->activateForRead(GL_COLOR_ATTACHMENT0);

	const CommandLineParams &winParams = MagicEngineMain::getCommandLineParams();
	OGL::blitFramebuffer(0, 0, winParams._xRes, winParams._yRes,
		0, 0, winParams._xRes, winParams._yRes, GL_COLOR_BUFFER_BIT, GL_LINEAR);
}

glm::vec2 Camera::getNearFar() { return glm::vec2(_zNearPlane, _zFarPlane); }
