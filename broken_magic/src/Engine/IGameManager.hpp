#pragma once

class btDynamicsWorld;

namespace MagicEngine
{
	class IGameManager
	{
	public:
		virtual void init() = 0;
		virtual void deinit() = 0;

		virtual void update(double dt) = 0;
		virtual void render() = 0;
	};
}

