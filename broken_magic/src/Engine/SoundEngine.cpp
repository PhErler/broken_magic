#include "SoundEngine.hpp"

using namespace MagicEngine;

SoundEngine::SoundEngine() {
	//create system for FMOD
	backgrondSound = FMOD::System_Create(&system);

	//load sounds
	system->setOutput(FMOD_OUTPUTTYPE_DSOUND);
	system->init(32, FMOD_INIT_NORMAL, 0);
	backgrondSound = system->createSound("resources\\sounds\\Broken Magic final.mp3", FMOD_DEFAULT, NULL, &sound);
}

SoundEngine::~SoundEngine() {
	backgrondSound = sound->release();
	backgrondSound = system->close();
	backgrondSound = system->release();
}


void SoundEngine::play() {
	channel = 0;
	backgrondSound = system->playSound(FMOD_CHANNEL_FREE, sound, false, &channel);
	system->update();
}
void SoundEngine::pause() {
	channel->setPaused(true);
}

