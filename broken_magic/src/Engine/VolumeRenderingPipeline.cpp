#include "VolumeRenderingPipeline.hpp"

#include "MagicEngineMain.hpp"
#include "Texture.hpp"
#include "Camera.hpp"
#include "VolumetricObject.hpp"
#include "ResourceManager.hpp"
#include "OGL.hpp"

#include "glm\gtx\transform.hpp"
#include "glm\gtc\constants.hpp"

using namespace std;
using namespace glm;
using namespace MagicEngine;

VolumeRenderingPipeline::VolumeRenderingPipeline(Camera* cam, RenderTexture* opaqueGeometryWorldSpacePositions)
	: IRenderPipeline(nullptr), _unitCube(ResourceManager::getUnitCube()), _quadVertexBuffer(ResourceManager::getQuad())
{
	_cam = cam;
	_opaqueGeometryWorldSpacePositions = opaqueGeometryWorldSpacePositions;

	_worldSpacePositionShader = ResourceManager::getShader("resources\\shaders\\WorldSpacePosition.vs",
		"resources\\shaders\\WorldSpacePosition.fs");
	_rayCastShader = ResourceManager::getShader("resources\\shaders\\VolumeRayCast.vs",
		"resources\\shaders\\VolumeRayCast.fs");

	const CommandLineParams &winParams = MagicEngineMain::getCommandLineParams();
	_bufferSizeX = winParams._xRes;
	_bufferSizeY = winParams._yRes;
	_posBBFrontWS = new RenderTexture(_bufferSizeX, _bufferSizeY, RenderTexture::RGB32F);
	_posBBBackWS = new RenderTexture(_bufferSizeX, _bufferSizeY, RenderTexture::RGB32F);

	vector<GLenum> attachments = { GL_COLOR_ATTACHMENT0 };
	vector<RenderTexture*> colorTextures = { _posBBFrontWS };
	_posBBFrontWSFbo = new FrameBuffer(&colorTextures, attachments, nullptr, false);
	colorTextures = { _posBBBackWS };
	_posBBBackWSFbo = new FrameBuffer(&colorTextures, attachments, nullptr, false);

	_noiseTexture = ResourceManager::getTexture("resources\\texures\\noise4.jpg");

	_wsPosLocations.mvp = _worldSpacePositionShader->getUniformLocation("gMVP");
	_wsPosLocations.modelToWorld = _worldSpacePositionShader->getUniformLocation("gModelToWorld");

	_raycastLocations.volTex = _rayCastShader->getUniformLocation("volTex");
	_raycastLocations.noiseTex = _rayCastShader->getUniformLocation("noiseTexture");
	_raycastLocations.posBackFaceWSTex = _rayCastShader->getUniformLocation("posBackFaceWSTex");
	_raycastLocations.posFrontFaceWSTex = _rayCastShader->getUniformLocation("posFrontFaceWSTex");
	_raycastLocations.posOpaqueGeometryWSTex = _rayCastShader->getUniformLocation("posOpaqueGeometryWSTex");
	
	_raycastLocations.stepSize = _rayCastShader->getUniformLocation("stepSize");
	_raycastLocations.lightRayStepSize = _rayCastShader->getUniformLocation("lightRayStepSize");
	_raycastLocations.opacityFactor = _rayCastShader->getUniformLocation("opacityFactor");
	_raycastLocations.worldToModel = _rayCastShader->getUniformLocation("worldToModel");

	_raycastLocations.noisePosOffset = _rayCastShader->getUniformLocation("noisePosOffset");
	_raycastLocations.noiseOffsetFactor = _rayCastShader->getUniformLocation("noiseOffsetFactor");

	_raycastLocations.posCamMS = _rayCastShader->getUniformLocation("posCamMS");
	_raycastLocations.posLightMS = _rayCastShader->getUniformLocation("posLightMS");
	_raycastLocations.colLight = _rayCastShader->getUniformLocation("colLight");
}

VolumeRenderingPipeline::~VolumeRenderingPipeline()
{
	_worldSpacePositionShader.reset();
	_rayCastShader.reset();

	_noiseTexture.reset();

	if (_posBBFrontWS != nullptr) {
		delete _posBBFrontWS;
		_posBBFrontWS = nullptr;
	}
	if (_posBBBackWS != nullptr) {
		delete _posBBBackWS;
		_posBBBackWS = nullptr;
	}
	if (_posBBFrontWSFbo != nullptr) {
		delete _posBBFrontWSFbo;
		_posBBFrontWSFbo = nullptr;
	}
	if (_posBBBackWSFbo != nullptr) {
		delete _posBBBackWSFbo;
		_posBBBackWSFbo = nullptr;
	}
}

void VolumeRenderingPipeline::render()
{
	if (!_cam)
	{
		return;
	}

	MagicEngineMain::getSceneGraphRootNode()->render(this);
}

void VolumeRenderingPipeline::draw(GameObject* gameObject)
{
	if (!gameObject->isVisible() || !gameObject->isVolumetric())
	{
		return;
	}

	VolumetricObject* volObject = static_cast<VolumetricObject*>(gameObject);
	std::shared_ptr<VolumeTexture> volumeTexture = volObject->getVolumeTexture();
	if (!volumeTexture)
	{
		return;
	}

	vec3 camPos = _cam->getPosition();
	mat4 viewMatrix = _cam->getViewMatrix();
	mat4 projectionMatrix = _cam->getProjectionMatrix();

	mat4 modelToWorldMatrix = volObject->getWorldspaceMatrix();
	mat4 mvp = projectionMatrix * viewMatrix * modelToWorldMatrix;
	renderBoundingBox(true, modelToWorldMatrix, mvp, camPos);
	renderBoundingBox(false, modelToWorldMatrix, mvp, camPos);
	
	float stepSize = volObject->getStepSize();
	float lightRayStepSize = volObject->getLightRayStepSize();
	float opacityFactor = volObject->getOpacityFactor();

	VolumetricLightInfo volLight = volObject->getLightInfo();
	vec3 lightDir = volLight._dir;
	vec3 lightColor = volLight._color;

	vec3 posLight = vec3(0.0, 0.0, 0.0);
	GameObject* lightObject = volObject->getLockedObject();
	if (lightObject)
		posLight = lightObject->getPosition();

	vec3 eye = normalize(vec3(viewMatrix[0][2], viewMatrix[1][2], viewMatrix[2][2]));

	mat4 worldToModelMatrix = inverse(modelToWorldMatrix);
	vec3 posCamMS = vec3(worldToModelMatrix * vec4(camPos, 1));
	vec3 posLightMS = vec3(worldToModelMatrix * vec4(posLight, 1));

	vec2 noisePosOffset = volObject->getNoisePos();
	float noiseFactor = volObject->getNoiseOffsetFactor();

	OGL::disable(GL_DEPTH_TEST);

	// do not write into depth buffer
	OGL::depthMask(GL_FALSE);

	OGL::disable(GL_CULL_FACE);

	OGL::enable(GL_BLEND);
	OGL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

	// init ray cast uniforms for this frame
	_rayCastShader->activate();

	volumeTexture->activate(0);
	_rayCastShader->setUniform(_raycastLocations.volTex, 0);
	_noiseTexture->activate(1);
	_rayCastShader->setUniform(_raycastLocations.noiseTex, 1);
	_posBBBackWS->activate(2);
	_rayCastShader->setUniform(_raycastLocations.posBackFaceWSTex, 2);
	_posBBFrontWS->activate(3);
	_rayCastShader->setUniform(_raycastLocations.posFrontFaceWSTex, 3);
	_opaqueGeometryWorldSpacePositions->activate(4);
	_rayCastShader->setUniform(_raycastLocations.posOpaqueGeometryWSTex, 4);

	_rayCastShader->setUniform(_raycastLocations.stepSize, stepSize);
	_rayCastShader->setUniform(_raycastLocations.lightRayStepSize, lightRayStepSize);
	_rayCastShader->setUniform(_raycastLocations.opacityFactor, opacityFactor);
	_rayCastShader->setUniform(_raycastLocations.worldToModel, worldToModelMatrix);
	
	_rayCastShader->setUniform(_raycastLocations.noisePosOffset, noisePosOffset);
	_rayCastShader->setUniform(_raycastLocations.noiseOffsetFactor, noiseFactor);

	_rayCastShader->setUniform(_raycastLocations.posCamMS, posCamMS);
	_rayCastShader->setUniform(_raycastLocations.posLightMS, posLightMS);
	_rayCastShader->setUniform(_raycastLocations.colLight, lightColor);

	// test
	//_rayCastShader->setUniform2f(_raycastLocations.noisePosOffset, lightColor);
	//_rayCastShader->setUniform1f(_raycastLocations.noiseOffsetFactor, lightColor.z);

	//printf("col %f, %f, %f \n", lightColor);
	//printf("loc %d \n", _raycastLocations.colLight);
	//printf("err %d \n", OGL::getError());
	
	// perform ray cast
	_cam->getRenderBuffer()->activate();

	_quadVertexBuffer->activate();
	_quadVertexBuffer->draw();
	_quadVertexBuffer->deactivate();

	_cam->getRenderBuffer()->deactivate();
	_opaqueGeometryWorldSpacePositions->deactivate(4);
	_posBBFrontWS->deactivate(3);
	_posBBBackWS->deactivate(2);
	_noiseTexture->deactivate(1);
	volumeTexture->deactivate(0);

	_rayCastShader->deactivate();

	OGL::disable(GL_BLEND);

	// write again into depth buffer
	OGL::depthMask(GL_TRUE);
}

mat4 VolumeRenderingPipeline::removeRotation(const mat4 &m)
{
	float scale = sqrt(m[0][0] * m[0][0] + m[1][0] * m[1][0] + m[2][0] * m[2][0]);
	mat4 withoutRotation = mat4(
		scale, 0, 0, 0, // first column (not row!)
		0, scale, 0, 0,
		0, 0, scale, 0,
		m[3][0], m[3][1], m[3][2], 1
	);

	return withoutRotation;
}

void MagicEngine::VolumeRenderingPipeline::renderBoundingBox(bool frontFace, glm::mat4 &modelToWorldMatrix, glm::mat4 &mvp, glm::vec3 camPos)
{
	FrameBuffer* faceFbo;
	vec3 clearColor = vec3(0.0, 0.0, 0.0);

	OGL::enable(GL_CULL_FACE);
	if (frontFace)
	{
		faceFbo = _posBBFrontWSFbo;
		OGL::cullFace(GL_BACK);
		clearColor = camPos;
	}
	else
	{
		faceFbo = _posBBBackWSFbo;
		OGL::cullFace(GL_FRONT);
	}

	faceFbo->activate();
	faceFbo->clear(true, false, false, clearColor.x, clearColor.y, clearColor.z);

	_worldSpacePositionShader->activate();
	_worldSpacePositionShader->setUniform(_wsPosLocations.mvp, mvp);
	_worldSpacePositionShader->setUniform(_wsPosLocations.modelToWorld, modelToWorldMatrix);

	_unitCube->activate();
	_unitCube->draw();
	_unitCube->deactivate();

	_worldSpacePositionShader->deactivate();

	faceFbo->deactivate();

	OGL::cullFace(GL_BACK);
	OGL::disable(GL_CULL_FACE);
}
