/*
Taken from the Volumetric Clouds Tutorial on
http://www.futuredatalab.com/volumetricloud/

Repository:
https://github.com/slsdo/VolumetricClouds
*/

#include "VoxelGrid.h"

#include "glm\glm.hpp"

#include <string>
#include <iostream>

using namespace std;
using namespace glm;

VoxelGrid::VoxelGrid()
{
	voxelSize = 0.0;
	xCount = 0;
	yCount = 0;
	zCount = 0;
	defaultDensity = 0.0;
}

VoxelGrid::VoxelGrid(float size, int x, int y, int z, float d)
{
	voxelSize = size;
	xCount = x;
	yCount = y;
	zCount = z;
	defaultDensity = d;
	
	// Create voxel grid
	for (float kk = 0; kk < zCount; kk++) {
		for (float jj = 0; jj < yCount; jj++) {
			for (float ii = 0; ii < xCount; ii++) {
				Voxel *voxel = new Voxel();
				voxel->density = defaultDensity;
				voxelGrid.push_back(voxel);
			}
		}
	}

	mat4 mat;	
	mat[0][0] = xCount*voxelSize; mat[0][1] = 0; mat[0][2] = 0; mat[0][3] = xCount*voxelSize*0.5f;
	mat[1][0] = 0; mat[1][1] = yCount*voxelSize; mat[1][2] = 0; mat[1][3] = yCount*voxelSize*0.5f;
	mat[2][0] = 0; mat[2][1] = 0; mat[2][2] = zCount*voxelSize; mat[2][3] = zCount*voxelSize*0.5f;
	mat[3][0] = 0; mat[3][1] = 0; mat[3][2] = 0; mat[3][3] = 1;
	t_inv = inverse(mat);
	mat4 tstar = mat;
	tstar[0][3] = 0.0; tstar[1][3] = 0.0; tstar[2][3] = 0.0;
	tstar_inv = inverse(tstar);
}

VoxelGrid::VoxelGrid(string filename)
{
	string line; // Temporary storage
	ifstream infile(filename.c_str()); // Open as stream
	if (infile.fail()) {
		cout << "Failed to open file!" << endl;
		return;
	}

	getline(infile, line); // Get voxel size
	voxelSize = (float)atof(line.c_str());
	getline(infile, line); // Get voxel grid x, y, z length
	stringstream ssrc(line); ssrc >> xCount >> yCount >> zCount;
	getline(infile, line); // Get default density
	defaultDensity = (float)atof(line.c_str());
	
	while (!infile.eof()) {
		getline(infile, line);
		if (line.empty()) break;
		Voxel *voxel = new Voxel();
		voxel->density = (float)atof(line.c_str());
		voxelGrid.push_back(voxel);
	}

	infile.close();

	mat4 mat;	
	mat[0][0] = xCount*voxelSize; mat[0][1] = 0; mat[0][2] = 0; mat[0][3] = xCount*voxelSize*0.5f;
	mat[1][0] = 0; mat[1][1] = yCount*voxelSize; mat[1][2] = 0; mat[1][3] = yCount*voxelSize*0.5f;
	mat[2][0] = 0; mat[2][1] = 0; mat[2][2] = zCount*voxelSize; mat[2][3] = zCount*voxelSize*0.5f;
	mat[3][0] = 0; mat[3][1] = 0; mat[3][2] = 0; mat[3][3] = 1;
	t_inv = inverse(mat);
	mat4 tstar = mat;
	tstar[0][3] = 0.0; tstar[1][3] = 0.0; tstar[2][3] = 0.0;
	tstar_inv = inverse(tstar);
}

VoxelGrid::~VoxelGrid()
{
	voxelGrid.clear();
}

void VoxelGrid::setVoxelColor(int x, int y, int z, vec3 rgb)
{
	// If outside of grid
	if ((x >= xCount) || (y >= yCount) || (z >= zCount) || (x < 0) || (y < 0) || (z < 0)) {
		return;
	}
	
	// Get array index
	int index = getVoxelIndex(x, y, z);
	voxelGrid[index]->color = rgb;
}

void VoxelGrid::setVoxelColor(vec3 xyz, vec3 rgb)
{
	vec3 ijk = world2voxel(xyz);
	setVoxelColor((int)ijk[0], (int)ijk[1], (int)ijk[2], rgb);
}

void VoxelGrid::setVoxelDensity(int x, int y, int z, float d)
{
	// If outside of grid
	if ((x >= xCount) || (y >= yCount) || (z >= zCount) || (x < 0) || (y < 0) || (z < 0)) {
		return;
	}
	
	// Get array index
	int index = getVoxelIndex(x, y, z);
	voxelGrid[index]->density = d;
}

void VoxelGrid::setVoxelDensity(vec3 xyz, float d)
{
	vec3 ijk = world2voxel(xyz);
	return setVoxelDensity((int)ijk[0], (int)ijk[1], (int)ijk[2], d);
}

void VoxelGrid::setVoxelTransmissivity(int x, int y, int z, float q)
{
	// If outside of grid
	if ((x >= xCount) || (y >= yCount) || (z >= zCount) || (x < 0) || (y < 0) || (z < 0)) {
		return;
	}
	
	// Get array index
	int index = getVoxelIndex(x, y, z);
	voxelGrid[index]->transmissivity = q;
}

void VoxelGrid::setVoxelTransmissivity(vec3 xyz, float q)
{
	vec3 ijk = world2voxel(xyz);
	return setVoxelTransmissivity((int)ijk[0], (int)ijk[1], (int)ijk[2], q);
}

vec3 VoxelGrid::getVoxelColor(int x, int y, int z)
{
	// If outside of grid
	if ((x >= xCount) || (y >= yCount) || (z >= zCount) || (x < 0) || (y < 0) || (z < 0)) {
		return vec3(0, 0, 0);
	}

	// Get array index
	int index = getVoxelIndex(x, y, z);
	return voxelGrid[index]->color;
}

vec3 VoxelGrid::getVoxelColor(vec3 xyz)
{
	vec3 ijk = world2voxel(xyz);
	return getVoxelColor((int)ijk[0], (int)ijk[1], (int)ijk[2]);
}

float VoxelGrid::getVoxelDensity(int x, int y, int z)
{
	// If outside of grid
	if ((x >= xCount) || (y >= yCount) || (z >= zCount) || (x < 0) || (y < 0) || (z < 0)) {
		return defaultDensity;
	}

	// Get array index
	int index = getVoxelIndex(x, y, z);
	return voxelGrid[index]->density;
}

float VoxelGrid::getVoxelDensity(vec3 xyz)
{
	vec3 ijk = world2voxel(xyz);
	return getVoxelDensity((int)ijk[0], (int)ijk[1], (int)ijk[2]);
}

float VoxelGrid::getVoxelTransmissivity(int x, int y, int z)
{
	// If outside of grid
	if ((x >= xCount) || (y >= yCount) || (z >= zCount) || (x < 0) || (y < 0) || (z < 0)) {
		return 0;
	}

	// Get array index
	int index = getVoxelIndex(x, y, z);
	return voxelGrid[index]->transmissivity;
}

float VoxelGrid::getVoxelTransmissivity(vec3 xyz)
{
	vec3 ijk = world2voxel(xyz);
	return getVoxelTransmissivity((int)ijk[0], (int)ijk[1], (int)ijk[2]);
}

vec3 VoxelGrid::interpVoxelColor(float x, float y, float z)
{
	vec3 interpColor(0, 0, 0);
	float sizeRatio = 1/voxelSize;

	// Convert from coordinate to index
	float x_index = floor(x*sizeRatio);
	float y_index = floor(y*sizeRatio);
	float z_index = floor(z*sizeRatio);

	// If outside of grid
	if ((x_index >= xCount) || (y_index >= yCount) || (z_index >= zCount) || (x_index < 0) || (y_index < 0) || (z_index < 0)) {
		return vec3(0, 0, 0);
	}

	// Tri-linear interpolation
	for (float kk = z_index; kk <= z_index + 1; kk++) {
		for (float jj = y_index; jj <= y_index + 1; jj++) {
			for (float ii = x_index; ii <= x_index + 1; ii++) {
				float wx = (voxelSize - fabs(x - ii*voxelSize))*sizeRatio;
				float wy = (voxelSize - fabs(y - jj*voxelSize))*sizeRatio;
				float wz = (voxelSize - fabs(z - kk*voxelSize))*sizeRatio;

				float weight = wx*wy*wz;
				interpColor += vec3(weight, weight, weight) * getVoxelColor((int)ii, (int)jj, (int)kk);
			}
		}
	}
	
	return interpColor;
}

vec3 VoxelGrid::interpVoxelColor(vec3 xyz)
{
	return interpVoxelColor(xyz[0], xyz[1], xyz[2]);
}

float VoxelGrid::interpVoxelDensity(float x, float y, float z)
{
	float interpDensity = 0;
	float sizeRatio = 1/voxelSize;

	// Convert from coordinate to index
	float x_index = floor(x*sizeRatio);
	float y_index = floor(y*sizeRatio);
	float z_index = floor(z*sizeRatio);

	// If outside of grid
	if ((x_index >= xCount) || (y_index >= yCount) || (z_index >= zCount) || (x_index < 0) || (y_index < 0) || (z_index < 0)) {
		return defaultDensity;
	}

	// Tri-linear interpolation
	for (float kk = z_index; kk <= z_index + 1; kk++) {
		for (float jj = y_index; jj <= y_index + 1; jj++) {
			for (float ii = x_index; ii <= x_index + 1; ii++) {
				float wx = (voxelSize - fabs(x - ii*voxelSize))*sizeRatio;
				float wy = (voxelSize - fabs(y - jj*voxelSize))*sizeRatio;
				float wz = (voxelSize - fabs(z - kk*voxelSize))*sizeRatio;

				float weight = wx*wy*wz;
				interpDensity += weight*getVoxelDensity((int)ii, (int)jj, (int)kk);
			}
		}
	}
	
	return interpDensity;
}

float VoxelGrid::interpVoxelDensity(vec3 xyz)
{
	return interpVoxelDensity(xyz[0], xyz[1], xyz[2]);
}

float VoxelGrid::interpVoxelTransmissivity(float x, float y, float z)
{
	float interpTransmissivity = 0;
	float sizeRatio = 1/voxelSize;

	// Convert from coordinate to index
	float x_index = floor(x*sizeRatio);
	float y_index = floor(y*sizeRatio);
	float z_index = floor(z*sizeRatio);

	// If outside of grid
	if ((x_index >= xCount) || (y_index >= yCount) || (z_index >= zCount) || (x_index < 0) || (y_index < 0) || (z_index < 0)) {
		return 0;
	}

	// Tri-linear interpolation
	for (float kk = z_index; kk <= z_index + 1; kk++) {
		for (float jj = y_index; jj <= y_index + 1; jj++) {
			for (float ii = x_index; ii <= x_index + 1; ii++) {
				float wx = (voxelSize - fabs(x - ii*voxelSize))*sizeRatio;
				float wy = (voxelSize - fabs(y - jj*voxelSize))*sizeRatio;
				float wz = (voxelSize - fabs(z - kk*voxelSize))*sizeRatio;

				float weight = wx*wy*wz;
				interpTransmissivity += weight*getVoxelTransmissivity((int)ii, (int)jj, (int)kk);
			}
		}
	}
	
	return interpTransmissivity;
}

float VoxelGrid::interpVoxelTransmissivity(vec3 xyz)
{
	return interpVoxelTransmissivity(xyz[0], xyz[1], xyz[2]);
}

int VoxelGrid::getVoxelIndex(int x, int y, int z)
{
	// Return index of voxel in array based on its x, y, z index
	return x + y*xCount + z*xCount*yCount;
}

vector<Voxel*> VoxelGrid::getVoxelGrid() { return voxelGrid; }
float VoxelGrid::getVoxelSize() { return voxelSize; }
float VoxelGrid::getDefaultDensity() { return defaultDensity; }
int VoxelGrid::getMaxX() { return xCount; }
int VoxelGrid::getMaxY() { return yCount; }
int VoxelGrid::getMaxZ() { return zCount; }
mat4 VoxelGrid::getTInv() { return t_inv; }
mat4 VoxelGrid::getTStarInv() { return tstar_inv; }

bool VoxelGrid::isInsideGrid(vec3 xyz)
{
	float x_max = xCount*voxelSize;
	float y_max = yCount*voxelSize;
	float z_max = zCount*voxelSize;

	if (xyz[0] < x_max && xyz[1] < y_max && xyz[2] < z_max && xyz[0] >= 0 && xyz[1] >= 0 && xyz[2] >= 0) {
		return true;
	}

	return false;
}

vec3 VoxelGrid::world2voxel(vec3 world)
{
	vec3 grid_root(0.0, 0.0, 0.0);
	vec3 ijk = (world - grid_root) / vec3(voxelSize, voxelSize, voxelSize); // component-wise division
	ijk = floor(ijk);
	return ijk;
}

vec3 VoxelGrid::voxel2world(vec3 ijk)
{
	vec3 grid_root(0.0, 0.0, 0.0);
	vec3 world = ijk * vec3(voxelSize, voxelSize, voxelSize) + grid_root;
	return world;
}