#pragma once

#include "Resource.hpp"

namespace MagicEngine
{
	class RenderTexture : public ITexture
	{
	public:
		enum RenderTextureType
		{
			Invalid,
			RGB,
			RGBA,
			RGB32F,
			Depth,
			DepthWithStencil,
			DepthCubemap,
		};

		RenderTexture(GLuint width, GLuint height, RenderTextureType type);
		virtual ~RenderTexture();

		virtual void activate(unsigned int textureSampler);
		virtual void deactivate(unsigned int textureSampler);

		GLuint getWidth() { return _width; }
		GLuint getHeight() { return _height; }

		RenderTextureType getType() { return _type; }

#ifdef _DEBUG
		void setBoundForWrite(bool boundForWrite);
#endif

	protected:
		GLuint _width = 0;
		GLuint _height = 0;

		RenderTextureType _type = Invalid;

#ifdef _DEBUG
		bool _boundForRead = false;
		bool _boundForWrite = false;
#endif
	};
}
