#include "ResourceManager.hpp"

#include "ModelLoader.hpp"
#include "GameObject.hpp"
#include "Light.hpp"
#include "MagicEngineMain.hpp"
#include "Primitives.hpp"

#include <sstream>
#include <string>

#include "assimp\postprocess.h"
#include "assimp\mesh.h"
#include "assimp\Importer.hpp"

using namespace std;
using namespace glm;
using namespace MagicEngine;

map<string, shared_ptr<GLSLProgram>> ResourceManager::s_shaderProgramMap;
map<string, shared_ptr<ModelData>> ResourceManager::s_modelDataMap;
map<string, shared_ptr<Texture>> ResourceManager::s_textureMap;
map<string, shared_ptr<VolumeTexture>> ResourceManager::s_volumeTextureMap;

// ModelData for Sphere
shared_ptr<ModelData> ResourceManager::s_sphere;
shared_ptr<VertexBuffer> ResourceManager::s_quad;
shared_ptr<VertexBuffer> ResourceManager::s_unitCube;

ModelData::ModelData(std::vector<glm::vec3>& vertexBuffer, std::vector<glm::vec2>& uvBuffer, std::vector<glm::vec3>& normalBuffer,
	std::vector<glm::vec3>& tangents, std::string& name, float& width, float& height, float& depth, glm::vec3& minBB,
	glm::vec3& maxBB, int& materialIndex)
	: _vertexBuffer(vertexBuffer), _uvBuffer(uvBuffer), _normalBuffer(normalBuffer), _tangentBuffer(tangents), _name(name), _width(width),
	_height(height), _depth(depth), _minBB(minBB), _maxBB(maxBB)
{
	_material._materialIndex = materialIndex;

	_aabbVertices.reserve(8);
	_aabbVertices.push_back(vec3(_minBB.x, _minBB.y, _minBB.z));
	_aabbVertices.push_back(vec3(_minBB.x, _minBB.y, _maxBB.z));
	_aabbVertices.push_back(vec3(_minBB.x, _maxBB.y, _minBB.z));
	_aabbVertices.push_back(vec3(_minBB.x, _maxBB.y, _maxBB.z));
	_aabbVertices.push_back(vec3(_maxBB.x, _minBB.y, _minBB.z));
	_aabbVertices.push_back(vec3(_maxBB.x, _minBB.y, _maxBB.z));
	_aabbVertices.push_back(vec3(_maxBB.x, _maxBB.y, _minBB.z));
	_aabbVertices.push_back(vec3(_maxBB.x, _maxBB.y, _maxBB.z));
}

void ResourceManager::clearCache()
{
	s_shaderProgramMap.clear();
	s_modelDataMap.clear();
	s_textureMap.clear();
	s_volumeTextureMap.clear();

	s_sphere.reset();
	s_quad.reset();
	s_unitCube.reset();
}

shared_ptr<GLSLProgram> ResourceManager::getShader(string vertexShaderPath, string fragmentShaderPath, string geometryShaderPath)
{
	string keyString = vertexShaderPath + fragmentShaderPath + geometryShaderPath;

	auto shader = s_shaderProgramMap.find(keyString);
	if (shader == s_shaderProgramMap.end())
	{	// not found
		s_shaderProgramMap[keyString] = std::make_shared<GLSLProgram>(vertexShaderPath, fragmentShaderPath, geometryShaderPath);
		return s_shaderProgramMap[keyString];
	}
	else {	// found
		return shader->second;
	}
}

void ResourceManager::loadCompleteScene(std::vector<MagicEngine::GameObject*>* gameObjects, std::string modelPath) {
	printf("Loading Mesh file %s...\n", modelPath.c_str());

	// Saving temporary the Names of the Meshes which are added at the end to our gameobject
	SimpleTree vectorOfMeshes("root");

	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(modelPath.c_str(), aiProcess_GenNormals | aiProcess_Triangulate | aiProcess_CalcTangentSpace);

	/* LOAD SCENE - Load all meshes of the scene */
	for (unsigned int i = 0; i < scene->mRootNode->mNumChildren; ++i) {
		aiNode *node = scene->mRootNode->mChildren[i];
		loadScene(scene, node, &vectorOfMeshes);
	}

	/* LOAD ALL MATERIALS */
	unsigned int numMaterials = scene->mNumMaterials;
	vector<AssimpMaterial> materialRemap;

	for (unsigned int i = 0; i < numMaterials; i++)
	{
		const aiMaterial* material = scene->mMaterials[i];
		int texIndex = 0;
		aiString path;

		if (material->GetTexture(aiTextureType_DIFFUSE, texIndex, &path) == AI_SUCCESS) {
			AssimpMaterial mat;
			mat._materialIndex = i;
			mat._path = path.data;
			materialRemap.push_back(mat);
		}
	}

	// Join Meshes with materials
	for (auto modelData : s_modelDataMap)
	{	
		shared_ptr<ModelData> model = modelData.second;

		// Only if material path is empty, because maybe a duplicated mesh model has a assigned resource material
		if (!model->_material._path.empty()) continue;

		for (AssimpMaterial mat : materialRemap) {
			if (model->_material._materialIndex == mat._materialIndex) {
				std::size_t pos = mat._path.find("resources");
				model->_material._path = mat._path.substr(pos);
				break;
			}
		}

		if (model->_material._path.length() == 0) {
			printf("Error: mesh %s has no material attached to it, it gets the default texture!\n", model->_name.c_str());
			model->_material._path = "resources\\texures\\default.bmp";
		}
	}

	GameObject* rootObj = new GameObject();
	MagicEngineMain::setSceneGraphRootNode(rootObj);

	// Add all meshes to gameobject array - Go through the scene objects
	// Root Node for all the GameObjects
	// Set up an imaginary root node
	GameObject* geometryRootObj = new GameObject();
	geometryRootObj->rotate(-1, 0, 0, 90); // because of blender meshes need to rotate 90 degrees
										   // default translate from origin 
	gameObjects->push_back(geometryRootObj);

	handleDuplicatedMeshes(&vectorOfMeshes);
	addAllMeshesToGameObjectArray(geometryRootObj, &vectorOfMeshes);
	rootObj->addChild(geometryRootObj);

	/* clean up */
	for (SimpleTree* &child : vectorOfMeshes._children) {
		delete child;
	}
	vectorOfMeshes._children.clear();

	/*
	GameObject* lightRoot = new GameObject();
	lightRoot->animateRotate(0, 1, 0, 20);
	rootObj->addChild(lightRoot);

	// LOAD ALL LIGHTS IN
	aiNode* root = scene->mRootNode;
	// Insatnce of struct Light from Engine Main
	for (unsigned int i = 0; i < scene->mNumLights; i++)
	{
		aiLight* light = scene->mLights[i];
		aiString name = light->mName;
		aiNode* lightnode = root->FindNode(name);

		// convert attributes of light to vectors
		glm::vec3 position = glm::vec3(lightnode->mTransformation.a4, lightnode->mTransformation.b4, lightnode->mTransformation.c4) / glm::vec3(lightnode->mTransformation.a1);
		glm::vec3 ambient = glm::vec3(light->mColorAmbient.r, light->mColorAmbient.g, light->mColorAmbient.b) * 0.01f;
		glm::vec3 diffuse = glm::vec3(light->mColorDiffuse.r, light->mColorDiffuse.g, light->mColorDiffuse.b) * 0.01f;
		glm::vec3 specular = glm::vec3(light->mColorSpecular.r, light->mColorSpecular.g, light->mColorSpecular.b) * 0.01f;
		glm::vec3 direction = glm::vec3(light->mDirection.x, light->mDirection.y, light->mDirection.z);

		// blender lets us set only 1 color. this color is exported in diffuse and specular but not ambient. so we just copy it
		ambient = diffuse * 0.25f;

		// assimps attenuation is wrong, we abuse this to mark different light classes
		Attenuation atten(light->mAttenuationConstant, light->mAttenuationLinear, light->mAttenuationQuadratic);

		// ugly hack
		unsigned char gameobject_position;
		float nearPlane = 0;
		float farPlane = 0;
		if (name == aiString("point_outdoor"))		// outside
		{
			atten = Attenuation(0, 0.005f, 0.005f);
			gameobject_position = GameObject::Outdoor;
			nearPlane = 1;
			farPlane = 60;
		}
		else if (name == aiString("point_indoor"))	// inside
		{
			atten = Attenuation(0, 0.05f, 0.1f);
			gameobject_position = GameObject::Indoor;
			nearPlane = 0.1f;
			farPlane = 8;
		}
		else if (name == aiString("point_outside"))	// outside
		{
			atten = Attenuation(0, 0.01f, 0.1f);
			gameobject_position = GameObject::OutsideSnowball;
			nearPlane = 1.0f;
			farPlane = 500;
		}

		printf("po %f, %f, %f \n", position.x, position.y, position.z);
		printf("am %f, %f, %f \n", ambient.x, ambient.y, ambient.z);
		printf("di %f, %f, %f \n", diffuse.x, diffuse.y, diffuse.z);
		printf("sp %f, %f, %f \n", specular.x, specular.y, specular.z);
		printf("dir %f, %f, %f \n", direction.x, direction.y, direction.z);
		printf("att %f, %f, %f \n", atten.Constant, atten.Linear, atten.Quadratic);
		printf("innercone %f \n", light->mAngleInnerCone);
		printf("outercone %f \n", light->mAngleOuterCone);
		printf("type %d \n", light->mType);

		BaseLight baseLight(diffuse, ambient, specular);

		// Set the light attributes
		Light* lightObj = new Light(position, atten, baseLight,
			light->mAngleInnerCone, light->mAngleOuterCone, direction, light->mType, nearPlane, farPlane);
		lightObj->setModelDataForLight(s_sphere);
		lightObj->setInScenePart(gameobject_position);
		lightObj->setGeometryName(name.C_Str());
		

		// light debug rendering
		// GameObject* lightRep = new GameObject(false);
		// float scale = 1 / lightObj->getScale();
		// lightRep->scale(scale, scale, scale);
		// lightRep->setModelDataForLight(s_sphere);
		// lightObj->addChild(lightRep);

		// Add light to gameobject array
		//lightRoot->addChild(lightObj);
	}*/
}

void ResourceManager::addAllMeshesToGameObjectArray(GameObject* parentObj, SimpleTree *vectorOfMeshes) {
	for (SimpleTree* &child : vectorOfMeshes->_children) {

		GameObject* staticGeometry = new GameObject();
		staticGeometry->setGeometryName(child->_name);
		staticGeometry->setModelData(child->_name, child->_materialPath);

		// translate Object as it is positioned in the blender file
		staticGeometry->setModelMatrix(child->_modelMartix);

		// set animation if there is one for the snowballs
		if (child->_name == "both_snowball") {
			staticGeometry->animateRotate(0.1f,0,1.0f,40);
		}
		if (child->_name == "outdoor_eagle_flying") {
			staticGeometry->animateRotate(0, 0, 1.0f, 20);
		}
		if (child->_name == "indoor_crystal") {
			staticGeometry->animateRotate(0, 0, 1.0f, -20);
		}

		// Set position in Scene for the Object to reduce rendering time
		setSceneFlags(/*OUT*/ staticGeometry);

		parentObj->addChild(staticGeometry);

		// Go deeper into tree
		addAllMeshesToGameObjectArray(staticGeometry, child);
	}
}

void ResourceManager::setSceneFlags(GameObject* staticGeometry) {
	std::string name = staticGeometry->getGeometryName();
	if (name.find("outdoor") != std::string::npos)
		staticGeometry->setInScenePart(GameObject::Outdoor);
	else if (name.find("indoor") != std::string::npos)
		staticGeometry->setInScenePart(GameObject::Indoor);
	else if (name.find("outside") != std::string::npos)
		staticGeometry->setInScenePart(GameObject::OutsideSnowball);
	else 
		staticGeometry->setInScenePart(GameObject::OutsideSnowball | GameObject::Outdoor | GameObject::Indoor);
}

void ResourceManager::loadScene(const aiScene *scene, aiNode *node, SimpleTree *parent) {

	if (node->mNumMeshes > 0) {
		aiMesh *mesh = scene->mMeshes[node->mMeshes[0]];

		vector<glm::vec3> vertices;
		vector<glm::vec2> uvs;
		vector<glm::vec3> normals;
		vector<glm::vec3> tangents;
		std::string name = node->mName.data;
		float width, height, depth;
		glm::vec3 minBB;
		glm::vec3 maxBB;
		glm::mat4 modelMatrix(1.0);
		int materialIndex;
		// Problem with Blender transformation Matrix
		if (parent->_name == "root")
			modelMatrix = glm::translate(modelMatrix, glm::vec3(node->mTransformation.a4, -node->mTransformation.c4, node->mTransformation.b4) / glm::vec3(node->mTransformation.a1));
		else
			modelMatrix = glm::translate(modelMatrix, glm::vec3(node->mTransformation.a4, node->mTransformation.b4, node->mTransformation.c4) / glm::vec3(node->mTransformation.a1));


		bool res = ModelLoader::loadMesh(mesh, vertices, uvs, normals, tangents, width, height, depth, minBB, maxBB, materialIndex);

		SimpleTree *simpleNode = new SimpleTree(name);

		if (name == "Sphere") {
			// Mesh just needed for the lights
			s_sphere = std::make_shared<ModelData>(vertices, uvs, normals, tangents, name, width, height, depth, minBB, maxBB, materialIndex);
			s_sphere->_material._path = "resources\\texures\\default.bmp";
		}
		else {
			s_modelDataMap[name] = std::make_shared<ModelData>(vertices, uvs, normals, tangents, name, width, height, depth, minBB, maxBB, materialIndex);

			parent->addChild(simpleNode);
			simpleNode->_modelMartix = modelMatrix;
		}
		// Go deeper if it has Children
		for (unsigned int i = 0; i < node->mNumChildren; i++) {
			loadScene(scene, node->mChildren[i], simpleNode);
		}
	}
	else {
		// ignore lights
	}

}

shared_ptr<ModelData> ResourceManager::getModelData(string name)
{
	auto modelData = s_modelDataMap.find(name);
	if (modelData == s_modelDataMap.end())
	{	// not found
		vector<glm::vec3> vertices;
		vector<glm::vec2> uvs;
		vector<glm::vec3> normals;
		bool res = ModelLoader::loadOBJ(name.c_str(), vertices, uvs, normals);
		assert(res == true);

		vec3 minBB(-0.5f, -0.5f, -0.5f);
		vec3 maxBB( 0.5f,  0.5f,  0.5f);
		float width = maxBB.x - minBB.x;
		float height = maxBB.y - minBB.y;
		float depth = maxBB.z - minBB.z;
		int matIndex = -1;
		s_modelDataMap[name] = std::make_shared<ModelData>(vertices, uvs, normals, vector<vec3>(), name, width, height, depth, minBB, maxBB, matIndex);
		return s_modelDataMap[name];
	}
	else {	// found
		return modelData->second;
	}
}

shared_ptr<Texture> ResourceManager::getTexture(string texturePath)
{
	auto texture = s_textureMap.find(texturePath);
	if (texture == s_textureMap.end())
	{	// not found
		s_textureMap[texturePath] = std::make_shared<Texture>(texturePath);
		return s_textureMap[texturePath];
	}
	else {	// found
		return texture->second;
	}
}

shared_ptr<VolumeTexture> ResourceManager::getVolumeTexture(string type)
{
	auto texture = s_volumeTextureMap.find(type);
	if (texture == s_volumeTextureMap.end())
	{	// not found
		s_volumeTextureMap[type] = std::make_shared<VolumeTexture>(type);
		return s_volumeTextureMap[type];
	}
	else {	// found
		return texture->second;
	}
}

std::shared_ptr<VertexBuffer> MagicEngine::ResourceManager::getQuad()
{
	if (!s_quad)
	{
		s_quad = Primitives::getQuadVertexBuffer();
	}
	return s_quad;
}

std::shared_ptr<VertexBuffer> MagicEngine::ResourceManager::getUnitCube()
{
	if (!s_unitCube)
	{
		s_unitCube = Primitives::getUnitCubeVertexBuffer();
	}
	return s_unitCube;
}

void ResourceManager::handleDuplicatedMeshes(SimpleTree *vectorOfMeshes)
{
	unsigned int size = vectorOfMeshes->_children.size();
	for (unsigned int x = 0; x < size; x++)
	{
		SimpleTree* child = vectorOfMeshes->_children[x];

		vector<glm::mat4> matrices;
		vector<std::string> materialpaths;
		glm::mat4 init_matrix(1.0);
		glm::mat4 m(1.0);

		/*if (child->_name == "indoor_door") {
			matrices.push_back(glm::translate(init_matrix, glm::vec3(3.12173, -0.09117, 46.58646)));
		}
		else*/ if (child->_name == "indoor_bookcase") {
			m = glm::translate(init_matrix, glm::vec3(1.17334, 2.85159, 46.47585));
			matrices.push_back(glm::rotate<float>(m, 90, glm::vec3(0, 0, 1)));

			m = glm::translate(init_matrix, glm::vec3(-1.69878, 2.58739, 46.47585));
			matrices.push_back(glm::rotate<float>(m, 135, glm::vec3(0, 0, 1)));
			m = glm::translate(init_matrix, glm::vec3(-2.60488, 1.68129, 46.47585));
			matrices.push_back(glm::rotate<float>(m, 135, glm::vec3(0, 0, 1)));

			m = glm::translate(init_matrix, glm::vec3(-2.57978, -1.69633, 46.47585));
			matrices.push_back(glm::rotate<float>(m, 225, glm::vec3(0, 0, 1)));
			m = glm::translate(init_matrix, glm::vec3(-1.68174, -2.59436, 46.47585));
			matrices.push_back(glm::rotate<float>(m, 225, glm::vec3(0, 0, 1)));

			// Big one
			//m = glm::translate(init_matrix, glm::vec3(1.17334, 2.85159, -133.48112));
			//m = glm::scale(m, glm::vec3(766.577, 766.577, 766.577));
			//matrices.push_back(glm::rotate<float>(m, 90, glm::vec3(0, 0, 1)));
		}
		else if (child->_name == "outdoor_tower_cylinder") {
			matrices.push_back(glm::translate(init_matrix, glm::vec3(1.60185, 1.55945, 56.13654)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(-0.02998, 2.12307, 56.13654)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(-1.52244, 1.48003, 56.13654)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(-2.12307, -0.02998, 56.13654)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(-1.48003, -1.52244, 56.13654)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(-0.02998, -2.23537, 56.13654)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(1.55945, -1.60185, 56.13654)));
		}
		/*else if (child->_name == "indoor_wooden_barrel") {
			matrices.push_back(glm::translate(init_matrix, glm::vec3(-0.54302, 2.72402, 45.98428)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(-0.35735, 2.67458, 46.32956)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(-0.19297, 2.7684, 45.98428)));
			m = glm::translate(init_matrix, glm::vec3(0.14602, 2.7825, 45.96325));
			matrices.push_back(glm::rotate<float>(m, 90, glm::vec3(1, 0, 0)));

			matrices.push_back(glm::translate(init_matrix, glm::vec3(-0.16744, -2.7475, 45.98428)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.14793, -2.70312, 45.98428)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(-0.02497, -2.76285, 46.32956)));
			m = glm::translate(init_matrix, glm::vec3(0.50397, -2.62936, 45.96325));
			m = glm::rotate<float>(m, 13, glm::vec3(0, 0, 1));
			matrices.push_back(glm::rotate<float>(m, 90, glm::vec3(1, 0, 0)));
			m = glm::translate(init_matrix, glm::vec3(-0.51902, -2.62936, 45.96325));
			m = glm::rotate<float>(m, -23, glm::vec3(0, 0, 1));
			matrices.push_back(glm::rotate<float>(m, 90, glm::vec3(1, 0, 0)));
		}*/
		else if (child->_name == "indoor_book_open") {
			m = glm::translate(init_matrix, glm::vec3(2.08708, 1.7765, 46.34558));
			matrices.push_back(glm::rotate<float>(m, -48, glm::vec3(0, 0, 1)));
		}
		else if (child->_name == "indoor_knight_1") {
			m = glm::translate(init_matrix, glm::vec3(2.62838, -1.0104, 46.74702));
			matrices.push_back(glm::rotate<float>(m, 180, glm::vec3(0, 0, 1)));
		}
		else if (child->_name == "indoor_Wizard_portrait") {
			matrices.push_back(glm::translate(init_matrix, glm::vec3(-0.56669, -3.02267, 46.94011)));
			materialpaths.push_back("resources\\texures\\indoor\\wizardportraits\\wizard_portrait.bmp");
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.56669, -3.02267, 46.94011)));
			materialpaths.push_back("resources\\texures\\indoor\\wizardportraits\\wizard_portrait2.bmp");
		}
		/*else if (child->_name == "both_snowball_bottom") {
			m = glm::translate(init_matrix, glm::vec3(2.84165, 0.83426, 46.65002));
			matrices.push_back(glm::scale(m, glm::vec3(0.001, 0.001, 0.001)));
		}
		else if (child->_name == "indoor_book_close") {
			// second row
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, 0.15, -0.26212)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, 0.10, -0.26212)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, 0.05, -0.26212)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, 0.00, -0.26212)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, -0.20, -0.26212)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, -0.15, -0.26212)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, -0.10, -0.26212)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, -0.05, -0.26212)));

			// first row
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, 0.20, -0.50988)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, 0.15, -0.50988)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, 0.10, -0.50988)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, 0.05, -0.50988)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, 0.00, -0.50988)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, -0.20, -0.50988)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, -0.15, -0.50988)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, -0.10, -0.50988)));
			matrices.push_back(glm::translate(init_matrix, glm::vec3(0.08462, -0.05, -0.50988)));
			
			// books on row of snowball
			m = glm::translate(init_matrix, glm::vec3(0.08791, 0.18914, 0.22703));
			matrices.push_back(glm::rotate<float>(m, -12, glm::vec3(1, 0, 0)));
			m = glm::translate(init_matrix, glm::vec3(0.08791, -0.18914, 0.22703));
			matrices.push_back(glm::rotate<float>(m, 12, glm::vec3(1, 0, 0)));
			
		}
		else if (child->_name == "both_window") {
			m = glm::translate(init_matrix, glm::vec3(2.32455, 2.3412, 47.47604));
			matrices.push_back(glm::rotate<float>(m, 90, glm::vec3(0, 0, 1)));

			m = glm::translate(init_matrix, glm::vec3(-2.34604, 2.33254, 47.47604));
			matrices.push_back(glm::rotate<float>(m, 180, glm::vec3(0, 0, 1)));
		}*/

		for (unsigned int i = 0; i < matrices.size(); i++)
		{
			// set the new model Matrix of the Object
			glm::mat4 mat = matrices[i];
			SimpleTree* node = new SimpleTree(child->_name.c_str());
			node->_modelMartix = mat;
			// set material of node if desired
			if (i < materialpaths.size()) node->_materialPath = materialpaths[i];

			vectorOfMeshes->addChild(node);

			// Add children to "node" if "child" has children 
			for (SimpleTree* &subchild : child->_children)
			{
				node->addChild(subchild);
			}
		}


		// duplicate for every child as well 
		handleDuplicatedMeshes(child);
	}
}


SimpleTree::SimpleTree(std::string name)
{
	_name = name;
}

SimpleTree::~SimpleTree()
{
	for (SimpleTree* &child : _children) {
		//delete child;
	}
	_children.clear();
}

void SimpleTree::setParent(SimpleTree* parent) {
	_parent = parent;
}

const SimpleTree* SimpleTree::getParent() {
	return _parent;
}

void SimpleTree::addChild(SimpleTree* child) {
	//if (child->getParent() != 0)
	//	std::cout << "child can only have one parent!";

	child->setParent(this);
	_children.push_back(child);
}