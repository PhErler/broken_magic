#include "CubemapTexture.hpp"

#include "OGL.hpp"

#include <iostream>

//from http://learnopengl.com/code_viewer.php?code=advanced/cubemaps_skybox_optimized

using namespace std;
using namespace MagicEngine;

namespace
{
	static const GLenum types[6] = {
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
	};
}

CubemapTexture::CubemapTexture(
	const string& Directory,
	const string& PosXFilename,
	const string& NegXFilename,
	const string& PosYFilename,
	const string& NegYFilename,
	const string& PosZFilename,
	const string& NegZFilename)
{
	string::const_iterator it = Directory.end();
	it--;
	string BaseDir = (*it == '\\') ? Directory : Directory + "\\";

	m_fileNames[0] = BaseDir + PosXFilename;
	m_fileNames[1] = BaseDir + NegXFilename;
	m_fileNames[2] = BaseDir + PosYFilename;
	m_fileNames[3] = BaseDir + NegYFilename;
	m_fileNames[4] = BaseDir + PosZFilename;
	m_fileNames[5] = BaseDir + NegZFilename;

	_resourceID = 0;

	Load();
}

CubemapTexture::~CubemapTexture()
{
	if (_resourceID != 0) {
		OGL::deleteTextures(1, &_resourceID);
	}
}

bool CubemapTexture::Load()
{
	vector<char> data;
	int width = 0;
	int height = 0;


	OGL::genTextures(1, &_resourceID);
	ITexture::bindTextureUnit(GL_TEXTURE_CUBE_MAP, _resourceID);

	for (unsigned int i = 0; i < 6; ++i) {

		loadBMP(m_fileNames[i], data, width, height);

		OGL::texImage2D(types[i], 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, &data[0]);
		
		data.clear();
	}

	OGL::texParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	OGL::texParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	OGL::texParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	OGL::texParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	OGL::texParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	ITexture::bindTextureUnit(GL_TEXTURE_CUBE_MAP, 0);

	return true;
}

void CubemapTexture::activate(unsigned int textureSampler)
{
	activateTextureUnit(textureSampler);
	ITexture::bindTextureUnit(GL_TEXTURE_CUBE_MAP, _resourceID);
}

void CubemapTexture::deactivate(unsigned int textureSampler)
{
	activateTextureUnit(textureSampler);
	ITexture::bindTextureUnit(GL_TEXTURE_CUBE_MAP, 0);
}