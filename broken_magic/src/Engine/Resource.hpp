#pragma once

#include <glm/glm.hpp>
#include <GL/glew.h>

#include <vector>
#include <map>

namespace MagicEngine
{
	class IResource
	{
	public:
		IResource();
		virtual ~IResource();

		virtual void activate() = 0;
		virtual void deactivate() = 0;

		GLuint getResourceID();

	protected:
		GLuint _resourceID;
	};

	class ITexture
	{
	public:
		ITexture();
		virtual ~ITexture();

		virtual void activate(unsigned int textureSampler) = 0;
		virtual void deactivate(unsigned int textureSampler) = 0;

		GLuint getResourceID();

		static void activateTextureUnit(GLuint unitID);
		static void bindTextureUnit(GLenum type, GLuint texture);

		static void loadBMP(std::string path, std::vector<char> &data_out, int &width_out, int &height_out);

	protected:
		GLuint _resourceID;
		static GLuint s_currentActiveTexture;
#ifdef _DEBUG
		static std::map<GLuint, GLuint> s_boundTextures;
#endif // DEBUG

	};

	class VertexArray : public IResource
	{
	public:
		VertexArray();
		virtual ~VertexArray();

		virtual void activate();
		virtual void deactivate();
	};

	class VertexBuffer : public IResource
	{
	public:
		VertexBuffer(const std::vector<glm::vec3> &vertices, int drawType = GL_STATIC_DRAW); // TODO: replace GL_STATIC_DRAW with own enum
		virtual ~VertexBuffer();

		void update(const std::vector<glm::vec3> &vertices, int drawType = GL_STATIC_DRAW);

		void draw();

		virtual void activate();
		virtual void deactivate();

		const std::vector<glm::vec3>& getVertices(){ return _vertices; }

	private:
		std::vector<glm::vec3> _vertices;
	};

	class UVBuffer : public IResource
	{
	public:
		UVBuffer(const std::vector<glm::vec2> &uvs);
		virtual ~UVBuffer();

		virtual void activate();
		virtual void deactivate();

		std::vector<glm::vec2> getUVs(){ return _uvs; }

	private:
		std::vector<glm::vec2> _uvs;
	};

	class NormalBuffer : public IResource
	{
	public:
		NormalBuffer(const std::vector<glm::vec3> &normals);
		virtual ~NormalBuffer();

		virtual void activate();
		virtual void deactivate();

		std::vector<glm::vec3> getNormals(){ return _normals; }

	private:
		std::vector<glm::vec3> _normals;
	};

	class TangentBuffer : public IResource
	{
	public:
		TangentBuffer(const std::vector<glm::vec3> &tangents);
		virtual ~TangentBuffer();

		virtual void activate();
		virtual void deactivate();

		std::vector<glm::vec3> getTangents() { return _tangents; }

	private:
		std::vector<glm::vec3> _tangents;
	};
}

