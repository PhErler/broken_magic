// Based on the OpenGL tutorial http://ogldev.atspace.co.uk/www/tutorial37/tutorial37.html

#include "DeferredRenderingPipeline.hpp"

#include "MagicEngineMain.hpp"
#include "ResourceManager.hpp"
#include "OGL.hpp"

#include <stdio.h>

#include <glm\glm.hpp>

using namespace std;
using namespace glm;
using namespace MagicEngine;

namespace
{
	// according to script
	const mat4 OmniDirFaceViewMatrixPosX =
		mat4(vec4(0, 0, -1, 0), vec4(0, -1, 0, 0), vec4(-1, 0, 0, 0), vec4(0, 0, 0, 1));
	const mat4 OmniDirFaceViewMatrixNegX =
		mat4(vec4(0, 0, +1, 0), vec4(0, -1, 0, 0), vec4(+1, 0, 0, 0), vec4(0, 0, 0, 1));
	const mat4 OmniDirFaceViewMatrixPosY =
		mat4(vec4(+1, 0, 0, 0), vec4(0, 0, -1, 0), vec4(0, +1, 0, 0), vec4(0, 0, 0, 1));
	const mat4 OmniDirFaceViewMatrixNegY =
		mat4(vec4(+1, 0, 0, 0), vec4(0, 0, +1, 0), vec4(0, -1, 0, 0), vec4(0, 0, 0, 1));
	const mat4 OmniDirFaceViewMatrixPosZ =
		mat4(vec4(+1, 0, 0, 0), vec4(0, -1, 0, 0), vec4(0, 0, -1, 0), vec4(0, 0, 0, 1));
	const mat4 OmniDirFaceViewMatrixNegZ =
		mat4(vec4(-1, 0, 0, 0), vec4(0, -1, 0, 0), vec4(0, 0, +1, 0), vec4(0, 0, 0, 1));

	const mat4 CubeFaceMatrizes[6] = {
		OmniDirFaceViewMatrixPosX,
		OmniDirFaceViewMatrixNegX,
		OmniDirFaceViewMatrixPosY,
		OmniDirFaceViewMatrixNegY,
		OmniDirFaceViewMatrixPosZ,
		OmniDirFaceViewMatrixNegZ
	};

	const float CubemapFoV = 90.0f;
	const float Aspect = 1;
	const int CubemapSize = 1024;
}

DeferredRenderingPipeline::DeferredRenderingPipeline(Camera* cam)
	: IRenderPipeline(cam), _quadVertexBuffer(ResourceManager::getQuad())
{
	const CommandLineParams &winParams = MagicEngineMain::getCommandLineParams();

	// Create the GBuffer textures
	_textures.reserve(GBUFFER_NUM_TEXTURES);
	for (unsigned int i = 0; i < GBUFFER_NUM_TEXTURES; ++i) {
		if (i == GBUFFER_TEXTURE_TYPE_DIFFUSE)
		{
			_textures.push_back(new RenderTexture(winParams._xRes, winParams._yRes, RenderTexture::RGB)); // no need for float if texture is int anyway
		}
		else
		{
			_textures.push_back(new RenderTexture(winParams._xRes, winParams._yRes, RenderTexture::RGB32F));
		}
	}

	_finalImage = new RenderTexture(winParams._xRes, winParams._yRes, RenderTexture::RGBA);

	// Create the FBO, share depth buffer with other pipelines
	vector<GLenum> attachments = { GL_COLOR_ATTACHMENT0,
		GL_COLOR_ATTACHMENT1,
		GL_COLOR_ATTACHMENT2 };
	vector<RenderTexture*> textures(_textures);
	_geometryPassFbo = new FrameBuffer(&_textures, attachments, cam->getRenderDepthTexture(), true);

	attachments = { GL_COLOR_ATTACHMENT0 };
	textures = { _finalImage };
	_finalFbo = new FrameBuffer(&textures, attachments, nullptr, false);

	_omniShadowMap = new RenderTexture(CubemapSize, CubemapSize, RenderTexture::DepthCubemap);
	_omniDirShadowFbo = new FrameBuffer(nullptr, vector<GLenum>(), _omniShadowMap, false);

	_geometryPassProgram = ResourceManager::getShader("resources\\shaders\\DeferredGeometry.vs",
		"resources\\shaders\\DeferredGeometry.fs");
	_stencilPassProgram = ResourceManager::getShader("resources\\shaders\\DeferredStencil.vs",
		"resources\\shaders\\DeferredStencil.fs");
	_lightZBufferPassProgram = ResourceManager::getShader("resources\\shaders\\OmniDirShadowMappingZBuffer.vs",
		"resources\\shaders\\OmniDirShadowMappingZBuffer.fs", "resources\\shaders\\OmniDirShadowMappingZBuffer.gs");
	_lightPassProgram = ResourceManager::getShader("resources\\shaders\\DeferredLight.vs",
		"resources\\shaders\\DeferredPointLight.fs");
	_directionalLightPassProgram = ResourceManager::getShader("resources\\shaders\\DeferredLight.vs",
		"resources\\shaders\\DeferredDirLight.fs");

	_geometryPassUniformLocations.MVP = _geometryPassProgram->getUniformLocation("gWVP");
	//_geometryPassUniformLocations.V = _geometryPassProgram->getUniformLocation("gV");
	_geometryPassUniformLocations.M = _geometryPassProgram->getUniformLocation("gWorld");
	_geometryPassUniformLocations.DiffuseTexture = _geometryPassProgram->getUniformLocation("gColorMap");
	//_geometryPassUniformLocations.NormalTexture  = _geometryPassProgram->getUniformLocation("gNormalMap");
	//_geometryPassUniformLocations.normalmapping = _geometryPassProgram->getUniformLocation("normalmapping");

	_stencilPassMVPLocation = _stencilPassProgram->getUniformLocation("gWVP");

	_pointLightZBufferPassUniformLocations.ModelMatrix = _lightZBufferPassProgram->getUniformLocation("modelMatrix");
	_pointLightZBufferPassUniformLocations.CubemapMatrix = _lightZBufferPassProgram->getUniformLocation("cubemapMatrix");
	_pointLightZBufferPassUniformLocations.NearFar = _lightZBufferPassProgram->getUniformLocation("nearFar");
	_pointLightZBufferPassUniformLocations.LightPos = _lightZBufferPassProgram->getUniformLocation("lightPos");

	_pointLightPassUniformLocations.LightPass.MVP = _lightPassProgram->getUniformLocation("uWVP");
	_pointLightPassUniformLocations.LightPass.PosTextureUnit = _lightPassProgram->getUniformLocation("uPositionMap");
	_pointLightPassUniformLocations.LightPass.ColorTextureUnit = _lightPassProgram->getUniformLocation("uColorMap");
	_pointLightPassUniformLocations.LightPass.NormalTextureUnit = _lightPassProgram->getUniformLocation("uNormalMap");
	_pointLightPassUniformLocations.LightPass.EyeWorld = _lightPassProgram->getUniformLocation("uEyeWorldPos");
	_pointLightPassUniformLocations.LightPass.MatSpecularPower = _lightPassProgram->getUniformLocation("uSpecularExponent");
	_pointLightPassUniformLocations.LightPass.ScreenSize = _lightPassProgram->getUniformLocation("uScreenSize");

	_pointLightPassUniformLocations.DiffuseColor = _lightPassProgram->getUniformLocation("uPointLight.Base.DiffuseColor");
	_pointLightPassUniformLocations.AmbientColor = _lightPassProgram->getUniformLocation("uPointLight.Base.AmbientColor");
	_pointLightPassUniformLocations.Position = _lightPassProgram->getUniformLocation("uPointLight.Position");
	_pointLightPassUniformLocations.NearFar = _lightPassProgram->getUniformLocation("uNearFar");
	_pointLightPassUniformLocations.CubeShadowMap = _lightPassProgram->getUniformLocation("uCubeShadowMap");
	_pointLightPassUniformLocations.SpecularColor = _lightPassProgram->getUniformLocation("uPointLight.Base.SpecularColor");
	_pointLightPassUniformLocations.Atten.Constant = _lightPassProgram->getUniformLocation("uPointLight.Atten.Constant");
	_pointLightPassUniformLocations.Atten.Linear = _lightPassProgram->getUniformLocation("uPointLight.Atten.Linear");
	_pointLightPassUniformLocations.Atten.Exp = _lightPassProgram->getUniformLocation("uPointLight.Atten.Exp");

	_directionalLightPassUniformLocations.LightPass.MVP = _directionalLightPassProgram->getUniformLocation("uWVP");
	_directionalLightPassUniformLocations.LightPass.PosTextureUnit = _directionalLightPassProgram->getUniformLocation("uPositionMap");
	_directionalLightPassUniformLocations.LightPass.ColorTextureUnit = _directionalLightPassProgram->getUniformLocation("uColorMap");
	_directionalLightPassUniformLocations.LightPass.NormalTextureUnit = _directionalLightPassProgram->getUniformLocation("uNormalMap");
	_directionalLightPassUniformLocations.LightPass.EyeWorld = _directionalLightPassProgram->getUniformLocation("uEyeWorldPos");
	_directionalLightPassUniformLocations.LightPass.MatSpecularPower = _directionalLightPassProgram->getUniformLocation("uSpecularExponent");
	_directionalLightPassUniformLocations.LightPass.ScreenSize = _directionalLightPassProgram->getUniformLocation("uScreenSize");

	_directionalLightPassUniformLocations.DiffuseColor = _directionalLightPassProgram->getUniformLocation("uDirectionalLight.Base.DiffuseColor");
	_directionalLightPassUniformLocations.AmbientColor = _directionalLightPassProgram->getUniformLocation("uDirectionalLight.Base.AmbientColor");
	_directionalLightPassUniformLocations.Direction = _directionalLightPassProgram->getUniformLocation("uDirectionalLight.Direction");
	_directionalLightPassUniformLocations.SpecularColor = _directionalLightPassProgram->getUniformLocation("uDirectionalLight.Base.SpecularColor");
}

DeferredRenderingPipeline::~DeferredRenderingPipeline()
{
	if (_geometryPassFbo != nullptr) {
		delete _geometryPassFbo;
		_geometryPassFbo = nullptr;
	}
	if (_finalFbo != nullptr) {
		delete _finalFbo;
		_finalFbo = nullptr;
	}
	if (_omniDirShadowFbo != nullptr) {
		delete _omniDirShadowFbo;
		_omniDirShadowFbo = nullptr;
	}

	for (RenderTexture* tex : _textures)
	{
		if (tex) {
			delete tex;
		}
	}
	_textures.clear();

	if (_omniShadowMap != nullptr) {
		delete _omniShadowMap;
		_omniShadowMap = nullptr;
	}

	if (_finalImage != nullptr) {
		delete _finalImage;
		_finalImage = nullptr;
	}
}

RenderTexture * MagicEngine::DeferredRenderingPipeline::getGBufferTexture(GBUFFER_TEXTURE_TYPE type)
{
	int typeIndex = int(type);
	if (typeIndex < int(GBUFFER_NUM_TEXTURES) && typeIndex < int(_textures.size()))
	{
		return _textures[typeIndex];
	}
	else
		return nullptr;
}

void MagicEngine::DeferredRenderingPipeline::render()
{
	if (!_cam)
	{
		return;
	}

	_finalFbo->activate();
	_finalFbo->clear(true, false, false, 0.0f, 0.0f, 0.0f); // clear with 0 or the lighting passes will add onto the clear color
	_finalFbo->deactivate();

	_state = 0;
	geometryPass();

	// We need stencil to be enabled in the stencil pass to get the stencil buffer
	// updated and we also need it in the light pass because we render the light
	// only if the stencil passes.
	vector<Light*> lights = MagicEngineMain::getLights();
	for (unsigned int i = 0; i < lights.size(); ++i)
	{
		if (lights[i]->isVisible())
		{
			_state = 1;
			stencilPass(lights[i]);

			_state = 2;
			lightPassZBuffer(lights[i]);

			_state = 3;
			lightPass(lights[i]);
		}
	}
	// The directional light does not need a stencil test because its volume
	// is unlimited and the final pass simply copies the texture.

	_state = 4;
	directionalLightPass();

	_state = 5;
	finalPass();
}

void DeferredRenderingPipeline::geometryPass()
{
	_geometryPassProgram->activate();

	_geometryPassFbo->activate();
	_geometryPassFbo->clear(true, true, false);

	OGL::enable(GL_DEPTH_TEST);
	OGL::depthMask(GL_TRUE); // Only the geometry pass updates the depth buffer

	OGL::enable(GL_CULL_FACE);
	OGL::cullFace(GL_BACK);

	MagicEngineMain::getSceneGraphRootNode()->render(this);

	OGL::disable(GL_CULL_FACE);

	OGL::disable(GL_DEPTH_TEST);

	_geometryPassFbo->deactivate();
}

void DeferredRenderingPipeline::stencilPass(Light* light)
{
	// must disable the draw buffers 
	_geometryPassFbo->activate();

	_stencilPassProgram->activate();

	// Disable color/depth write and enable stencil
	OGL::enable(GL_STENCIL_TEST);
	OGL::enable(GL_DEPTH_TEST);
	// When we get here the depth buffer is already populated and the stencil pass
	// depends on it, but it does not write to it.
	OGL::depthMask(GL_FALSE);
	_geometryPassFbo->clear(false, false, true);

	// We need the stencil test to be enabled but we want it
	// to succeed always. Only the depth test matters.
	OGL::stencilFunc(GL_ALWAYS, 0, 0);

	OGL::stencilOpSeparate(GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
	OGL::stencilOpSeparate(GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);

	stencilDraw(light);

	OGL::disable(GL_DEPTH_TEST);
	OGL::disable(GL_STENCIL_TEST);

	_stencilPassProgram->deactivate();
	_geometryPassFbo->deactivate();
}

void DeferredRenderingPipeline::lightPassZBuffer(Light* light)
{
	OGL::depthMask(GL_TRUE);
	_omniDirShadowFbo->activate();
	_omniDirShadowFbo->clear(false, true, false);

	_lightZBufferPassProgram->activate();

	vec3 lightPos = light->getPosition();

	float cubemapNearPlane = light->_nearPlane;
	float cubemapFarPlane = light->_farPlane;
	mat4 cubeProjectionMatrix = perspective(CubemapFoV, Aspect, cubemapNearPlane, cubemapFarPlane);

	const int numCubeFaces = 6;
	mat4 cubeMatrizes[numCubeFaces];
	for (int i = 0; i < numCubeFaces; ++i)
	{
		cubeMatrizes[i] = cubeProjectionMatrix * translate(CubeFaceMatrizes[i], -lightPos);
	}

	_lightZBufferPassProgram->setUniform(_pointLightZBufferPassUniformLocations.CubemapMatrix, cubeMatrizes[0], 6);
	_lightZBufferPassProgram->setUniform(_pointLightZBufferPassUniformLocations.NearFar, cubemapNearPlane, cubemapFarPlane);
	_lightZBufferPassProgram->setUniform(_pointLightZBufferPassUniformLocations.LightPos, vec4(lightPos, 1));

	OGL::enable(GL_DEPTH_TEST);

	MagicEngineMain::getSceneGraphRootNode()->render(this);

	OGL::disable(GL_DEPTH_TEST);

	_lightZBufferPassProgram->deactivate();
	_omniDirShadowFbo->deactivate();
}

void DeferredRenderingPipeline::lightPass(Light* light)
{
	_finalFbo->activate();

	_lightPassProgram->activate();

	GLint locations[] = { _pointLightPassUniformLocations.LightPass.PosTextureUnit,
		_pointLightPassUniformLocations.LightPass.ColorTextureUnit,
		_pointLightPassUniformLocations.LightPass.NormalTextureUnit };
	for (unsigned int i = 0; i < GBUFFER_NUM_TEXTURES; ++i) {
		_textures[i]->activate(i);
		_lightPassProgram->setUniform(locations[i], int(i));
	}

	const CommandLineParams &winParams = MagicEngineMain::getCommandLineParams();
	vec3 camPos = MagicEngineMain::getCamera()->getPosition();

	float cubemapNearPlane = light->_nearPlane;
	float cubemapFarPlane = light->_farPlane;

	const int ShadowMapTextureUnit = GBUFFER_NUM_TEXTURES + 1;
	_omniShadowMap->activate(ShadowMapTextureUnit);
	_lightPassProgram->setUniform(_pointLightPassUniformLocations.CubeShadowMap, ShadowMapTextureUnit);

	_lightPassProgram->setUniform(_pointLightPassUniformLocations.NearFar, cubemapNearPlane, cubemapFarPlane);
	_lightPassProgram->setUniform(_pointLightPassUniformLocations.LightPass.EyeWorld, camPos.x, camPos.y, camPos.z);
	_lightPassProgram->setUniform(_pointLightPassUniformLocations.LightPass.MatSpecularPower, 5.0f);
	_lightPassProgram->setUniform(_pointLightPassUniformLocations.LightPass.ScreenSize, static_cast<float>(winParams._xRes), static_cast<float>(winParams._yRes));

	setLight(light);

	OGL::enable(GL_STENCIL_TEST);
	//OGL::stencilFunc(GL_NOTEQUAL, 0, 0xFF);

	OGL::enable(GL_BLEND);
	OGL::blendFunc(GL_ONE, GL_ONE);

	OGL::enable(GL_CULL_FACE);
	OGL::cullFace(GL_FRONT);

	lightDraw(light);

	OGL::cullFace(GL_BACK);
	OGL::disable(GL_CULL_FACE);

	OGL::disable(GL_BLEND);

	OGL::disable(GL_STENCIL_TEST);

	for (unsigned int i = 0; i < GBUFFER_NUM_TEXTURES; ++i) {
		_textures[i]->deactivate(i);
	}

	_omniShadowMap->deactivate(ShadowMapTextureUnit);
	_lightPassProgram->deactivate();
	_finalFbo->deactivate();
}

void MagicEngine::DeferredRenderingPipeline::directionalLightPass()
{
	_finalFbo->activate();

	_directionalLightPassProgram->activate();

	GLint locations[] = { _directionalLightPassUniformLocations.LightPass.PosTextureUnit,
		_directionalLightPassUniformLocations.LightPass.ColorTextureUnit,
		_directionalLightPassUniformLocations.LightPass.NormalTextureUnit };
	for (unsigned int i = 0; i < GBUFFER_NUM_TEXTURES; ++i) {
		_textures[i]->activate(i);
		_directionalLightPassProgram->setUniform(locations[i], int(i));
	}

	const CommandLineParams &winParams = MagicEngineMain::getCommandLineParams();
	vec3 camPos = MagicEngineMain::getCamera()->getPosition();
	mat4 mvp; // is identity on purpose, need mvp for shading area of point light

	_directionalLightPassProgram->setUniform(_directionalLightPassUniformLocations.LightPass.MVP, mvp);
	_directionalLightPassProgram->setUniform(_directionalLightPassUniformLocations.LightPass.EyeWorld, camPos.x, camPos.y, camPos.z);
	_directionalLightPassProgram->setUniform(_directionalLightPassUniformLocations.LightPass.MatSpecularPower, 5.0f);
	_directionalLightPassProgram->setUniform(_directionalLightPassUniformLocations.LightPass.ScreenSize, static_cast<float>(winParams._xRes), static_cast<float>(winParams._yRes));

	DirectionalLight dirLight = MagicEngineMain::getDirectionalLight();
	vec3 dirLightColor = dirLight._baseLight.DiffuseColor;// diffuse light is mostly for volume rendering
	vec3 dirLightDirection = -dirLight._dir;
	vec3 ambientColor = dirLight._baseLight.AmbientColor;
	vec3 specularColor = dirLight._baseLight.SpecularColor;

	_directionalLightPassProgram->setUniform(_directionalLightPassUniformLocations.DiffuseColor, dirLightColor.x, dirLightColor.y, dirLightColor.z);
	_directionalLightPassProgram->setUniform(_directionalLightPassUniformLocations.AmbientColor, ambientColor.x, ambientColor.y, ambientColor.z);
	_directionalLightPassProgram->setUniform(_directionalLightPassUniformLocations.Direction, dirLightDirection.x, dirLightDirection.y, dirLightDirection.z);
	_directionalLightPassProgram->setUniform(_directionalLightPassUniformLocations.SpecularColor, specularColor.x, specularColor.y, specularColor.z);

	OGL::enable(GL_BLEND);
	OGL::blendFunc(GL_ONE, GL_ONE);

	_quadVertexBuffer->activate();
	_quadVertexBuffer->draw();
	_quadVertexBuffer->deactivate();

	OGL::disable(GL_BLEND);

	for (unsigned int i = 0; i < GBUFFER_NUM_TEXTURES; ++i) {
		_textures[i]->deactivate(i);
	}

	_directionalLightPassProgram->deactivate();
	_finalFbo->deactivate();
}

void DeferredRenderingPipeline::finalPass()
{
	Camera* cam = MagicEngineMain::getCamera();
	if (!cam)
	{
		return;
	}

	vector<GLenum> attachments = { GL_COLOR_ATTACHMENT0 };
	cam->getRenderBuffer()->activateForWrite(attachments);

	int debugState = MagicEngineMain::getDeferredDebugRenderingState();
	switch (debugState)
	{
	case 0: // default
		_finalFbo->activateForRead(GL_COLOR_ATTACHMENT0);
		break;
	case 1:
		_geometryPassFbo->activateForRead(GL_COLOR_ATTACHMENT0);
		break;
	case 2:
		_geometryPassFbo->activateForRead(GL_COLOR_ATTACHMENT1);
		break;
	case 3:
		_geometryPassFbo->activateForRead(GL_COLOR_ATTACHMENT2);
		break;
	case 4:
		_geometryPassFbo->activateForRead(GL_DEPTH_ATTACHMENT);
		break;
	default:
		break;
	}

	// deferred has no transparency -> must be first -> can just write to cam buffer
	const CommandLineParams &winParams = MagicEngineMain::getCommandLineParams();
	OGL::blitFramebuffer(0, 0, winParams._xRes, winParams._yRes,
		0, 0, winParams._xRes, winParams._yRes, GL_COLOR_BUFFER_BIT, GL_LINEAR);
	_finalFbo->deactivate();
	cam->getRenderBuffer()->deactivate();
}

void MagicEngine::DeferredRenderingPipeline::draw(GameObject * gameObject)
{
	if (!gameObject->isVisible() || gameObject->isTransparent() || gameObject->isLightSource())
	{
		return;
	}

	shared_ptr<ModelData> modelData = gameObject->getModelData();

	if (_state == 0)
	{
		if (modelData)
		{
			geometryDraw(gameObject);
		}
	}
	else if (_state = 2)
	{
		if (modelData)
		{
			lightDrawZBuffer(gameObject);
		}
	}
	else
	{
		cout << "ShadowMappingPipeline::draw wrong state" << endl;
	}
}

void MagicEngine::DeferredRenderingPipeline::geometryDraw(GameObject* gameObject)
{
	std::shared_ptr<ModelData> modelData = gameObject->getModelData();

	mat4 modelMatrix = gameObject->getWorldspaceMatrix();
	mat4 viewMatrix = _cam->getViewMatrix();
	mat4 projectionMatrix = _cam->getProjectionMatrix();
	glm::mat4 MVP = projectionMatrix * viewMatrix * modelMatrix;

	// assimps bounding boxes are probably wrong
	if (MagicEngineMain::isCullingEnabled() && !gameObject->isInViewFrustum(MVP))
	{
		return;
	}

	_geometryPassProgram->setUniform(_geometryPassUniformLocations.MVP, MVP);
	//_geometryPassProgram->setUniform(_geometryPassUniformLocations.V, viewMatrix, false);
	_geometryPassProgram->setUniform(_geometryPassUniformLocations.M, modelMatrix);

	gameObject->getTexture()->activate(0);
	_geometryPassProgram->setUniform(_geometryPassUniformLocations.DiffuseTexture, 0);

	// Activate Normal Map
	/*if (gameObject->getNormalTexture() && MagicEngineMain::getNormalMapRenderingState()) {
	_geometryPassProgram->setUniform1i(_geometryPassUniformLocations.normalmapping, true);
	gameObject->getNormalTexture()->activate(1);
	_geometryPassProgram->setUniform1i(_geometryPassUniformLocations.NormalTexture, 1);
	}
	else {
	_geometryPassProgram->setUniform1i(_geometryPassUniformLocations.normalmapping, false);
	}*/

	modelData->_vertexBuffer.activate();
	modelData->_uvBuffer.activate();
	modelData->_normalBuffer.activate();

	modelData->_vertexBuffer.draw();

	modelData->_vertexBuffer.deactivate();
	modelData->_uvBuffer.deactivate();
	modelData->_normalBuffer.deactivate();

	gameObject->getTexture()->deactivate(0);

	if (gameObject->getNormalTexture() && MagicEngineMain::getNormalMapRenderingState()) {
		gameObject->getNormalTexture()->deactivate(1);
	}

}

void MagicEngine::DeferredRenderingPipeline::stencilDraw(GameObject* gameObject)
{
	std::shared_ptr<ModelData> modelData = gameObject->getModelData();

	mat4 modelMatrix = gameObject->getWorldspaceMatrix();
	mat4 viewMatrix = _cam->getViewMatrix();
	mat4 projectionMatrix = _cam->getProjectionMatrix();
	glm::mat4 MVP = projectionMatrix * viewMatrix * modelMatrix;
	_stencilPassProgram->setUniform(_stencilPassMVPLocation, MVP);

	modelData->_vertexBuffer.activate();
	modelData->_vertexBuffer.draw();
	modelData->_vertexBuffer.deactivate();
}

void MagicEngine::DeferredRenderingPipeline::lightDrawZBuffer(GameObject* gameObject)
{
	std::shared_ptr<ModelData> modelData = gameObject->getModelData();

	mat4 modelMatrix = gameObject->getWorldspaceMatrix();

	_lightZBufferPassProgram->setUniform(_pointLightZBufferPassUniformLocations.ModelMatrix, modelMatrix);

	modelData->_vertexBuffer.activate();
	modelData->_vertexBuffer.draw();
	modelData->_vertexBuffer.deactivate();
}

void MagicEngine::DeferredRenderingPipeline::lightDraw(Light* light)
{
	std::shared_ptr<ModelData> modelData = light->getModelData();

	mat4 modelMatrix = light->getWorldspaceMatrix();
	mat4 viewMatrix = _cam->getViewMatrix();
	mat4 projectionMatrix = _cam->getProjectionMatrix();
	glm::mat4 MVP = projectionMatrix * viewMatrix * modelMatrix;
	_lightPassProgram->setUniform(_pointLightPassUniformLocations.LightPass.MVP, MVP);

	modelData->_vertexBuffer.activate();
	modelData->_vertexBuffer.draw();
	modelData->_vertexBuffer.deactivate();
}

void MagicEngine::DeferredRenderingPipeline::setLight(Light * light)
{
	vec3 lightPos = light->getPosition();
	vec3 diffuseColor = light->_baseLight.DiffuseColor;
	vec3 specularColor = light->_baseLight.SpecularColor;
	vec3 ambientColor = light->_baseLight.AmbientColor;

	_lightPassProgram->setUniform(_pointLightPassUniformLocations.DiffuseColor, diffuseColor);
	_lightPassProgram->setUniform(_pointLightPassUniformLocations.AmbientColor, ambientColor);
	_lightPassProgram->setUniform(_pointLightPassUniformLocations.SpecularColor, specularColor);
	_lightPassProgram->setUniform(_pointLightPassUniformLocations.Position, lightPos);
	_lightPassProgram->setUniform(_pointLightPassUniformLocations.Atten.Constant, light->_attenuation.Constant);
	_lightPassProgram->setUniform(_pointLightPassUniformLocations.Atten.Linear, light->_attenuation.Linear);
	_lightPassProgram->setUniform(_pointLightPassUniformLocations.Atten.Exp, light->_attenuation.Quadratic);
}
