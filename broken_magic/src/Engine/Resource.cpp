#include "Resource.hpp"

#include "MagicEngineMain.hpp"
#include "OGL.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <iostream>

using namespace std;
using namespace MagicEngine;

IResource::IResource()
: _resourceID(0)
{
}

IResource::~IResource()
{
}

GLuint IResource::getResourceID()
{
	return _resourceID;
}



GLuint ITexture::s_currentActiveTexture = 0;
#ifdef _DEBUG
map<GLuint, GLuint> ITexture::s_boundTextures;
#endif // _DEBUG

ITexture::ITexture()
: _resourceID(0)
{
}

ITexture::~ITexture()
{
}

GLuint ITexture::getResourceID()
{
	return _resourceID;
}

void ITexture::activateTextureUnit(GLuint unitID)
{
	if (s_currentActiveTexture != unitID)
	{
		OGL::activeTexture(GL_TEXTURE0 + unitID);
		s_currentActiveTexture = unitID;
	}
}

void ITexture::bindTextureUnit(GLenum type, GLuint texture)
{
#ifdef _DEBUG
	auto boundCurrentSampler = s_boundTextures.find(s_currentActiveTexture);
	if (boundCurrentSampler != s_boundTextures.end())
	{
		auto boundTextureAtCurrentSampler = boundCurrentSampler->second;
		assert(boundTextureAtCurrentSampler == 0 || texture == 0);
	}
	s_boundTextures[s_currentActiveTexture] = texture;

	OGL::bindTexture(type, texture);
#else

	// if (texture != 0) // not necessary but nice for debugging
		OGL::bindTexture(type, texture);

#endif // _DEBUG
}

// basing on the opengl-tutorial from
// http://www.opengl-tutorial.org/
void ITexture::loadBMP(string path, vector<char> &data_out, int &width_out, int &height_out)
{
	
	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	// Actual RGB data
	//unsigned char * data;

	// Open the file
	FILE * file = fopen(path.c_str(), "rb");
	if (!file)							    
	{ 
		printf("%s could not be opened. Are you in the right directory?\n", path.c_str()); 
		return; 
	}

	// Read the header, i.e. the 54 first bytes

	// If less than 54 bytes are read, problem
	if (fread(header, 1, 54, file) != 54){
		printf("Not a correct BMP file\n");
		return;
	}
	// A BMP files always begins with "BM"
	if (header[0] != 'B' || header[1] != 'M'){
		printf("Not a correct BMP file\n");
		return;
	}
	// Make sure this is a 24bpp file
	if (*(int*)&(header[0x1E]) != 0)         
	{ 
		printf("Not a correct BMP file\n");    
		return; 
	}

	if (*(int*)&(header[0x1C]) != 24)         
	{ 
		printf("Not a correct BMP file\n");    
		return; 
	}

	// Read the information about the image
	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);

	// Some BMP files are misformatted, guess missing information
	if (imageSize == 0)    imageSize = width*height * 3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos == 0)      dataPos = 54; // The BMP header is done that way

	width_out = width;
	height_out = height;

	// Create a buffer
	//data = new unsigned char[imageSize];
	data_out = vector<char>(imageSize);

	// Read the actual data from the file into the buffer
	fread(&data_out[0], 1, imageSize, file);

	// Everything is in memory now, the file wan be closed
	fclose(file);

	// OpenGL has now copied the data. Free our own version
	//delete[] data;
}


VertexArray::VertexArray()
{
	OGL::genVertexArrays(1, &_resourceID);

#ifdef _DEBUG
	assert(_resourceID != 0);
#endif // _DEBUG

	OGL::bindVertexArray(_resourceID);
}

VertexArray::~VertexArray()
{
	OGL::deleteVertexArrays(1, &_resourceID);
}

void VertexArray::activate()
{
}

void VertexArray::deactivate()
{
}



VertexBuffer::VertexBuffer(const std::vector<glm::vec3> &vertices, int drawType)
{
	_vertices = vertices;
	OGL::genBuffers(1, &_resourceID);

	// test
	OGL::bindBuffer(GL_ARRAY_BUFFER, _resourceID);
	OGL::bindBuffer(GL_ARRAY_BUFFER, 0);
	//printf("gen buffer %d \n", _resourceID);

#ifdef _DEBUG
	assert(_resourceID != 0);
#endif // _DEBUG

	update(vertices, drawType);
}

VertexBuffer::~VertexBuffer()
{
	//printf("del buffer %d \n", _resourceID);
	OGL::deleteBuffers(1, &_resourceID);
}

void VertexBuffer::update(const std::vector<glm::vec3> &vertices, int drawType)
{
	if (!vertices.empty())
	{
		OGL::bindBuffer(GL_ARRAY_BUFFER, _resourceID);
		if (!vertices.empty())
		{
			OGL::bufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3),
				&vertices[0], drawType);
			OGL::bindBuffer(GL_ARRAY_BUFFER, 0);
		}
	}
}

void MagicEngine::VertexBuffer::draw()
{
	MagicEngineMain::addRenderedVertices(_vertices.size());

	if (MagicEngineMain::wireFrameMode() == true)
	{
		OGL::drawArrays(GL_LINES, 0, _vertices.size());
	}
	else
	{
		OGL::drawArrays(GL_TRIANGLES, 0, _vertices.size());
	}
}

void VertexBuffer::activate()
{
	// 1st attribute buffer : _vertices
	OGL::enableVertexAttribArray(0);
	OGL::bindBuffer(GL_ARRAY_BUFFER, _resourceID);
	OGL::vertexAttribPointer(
		0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);
}

void VertexBuffer::deactivate()
{
	OGL::bindBuffer(GL_ARRAY_BUFFER, 0);
	OGL::disableVertexAttribArray(0);
}



UVBuffer::UVBuffer(const std::vector<glm::vec2> &uvs)
{
	_uvs = uvs;
	OGL::genBuffers(1, &_resourceID);

#ifdef _DEBUG
	assert(_resourceID != 0);
#endif // _DEBUG

	OGL::bindBuffer(GL_ARRAY_BUFFER, _resourceID);
	if (!uvs.empty())
	{
		OGL::bufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2),
			&uvs[0], GL_STATIC_DRAW);
	}
}

UVBuffer::~UVBuffer()
{
	OGL::deleteBuffers(1, &_resourceID);
}

void UVBuffer::activate()
{
	// 2nd attribute buffer : UVs
	OGL::enableVertexAttribArray(1);
	OGL::bindBuffer(GL_ARRAY_BUFFER, _resourceID);
	OGL::vertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size : U+V => 2
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);
}

void UVBuffer::deactivate()
{
	OGL::disableVertexAttribArray(1);
}



NormalBuffer::NormalBuffer(const std::vector<glm::vec3> &normals)
{
	_normals = normals;
	OGL::genBuffers(1, &_resourceID);

#ifdef _DEBUG
	assert(_resourceID != 0);
#endif // _DEBUG

	OGL::bindBuffer(GL_ARRAY_BUFFER, _resourceID);
	if (!normals.empty())
	{
		OGL::bufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3),
			&normals[0], GL_STATIC_DRAW);
	}
}

NormalBuffer::~NormalBuffer()
{
	OGL::deleteBuffers(1, &_resourceID);
}

void NormalBuffer::activate()
{
	// 3rd attribute buffer : normals
	OGL::enableVertexAttribArray(2);
	OGL::bindBuffer(GL_ARRAY_BUFFER, _resourceID);
	OGL::vertexAttribPointer(
		2,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);
}

void NormalBuffer::deactivate()
{
	OGL::disableVertexAttribArray(2);
}

TangentBuffer::TangentBuffer(const std::vector<glm::vec3> &tangents)
{
	_tangents = tangents;
	OGL::genBuffers(1, &_resourceID);

#ifdef _DEBUG
	assert(_resourceID != 0);
#endif // _DEBUG

	OGL::bindBuffer(GL_ARRAY_BUFFER, _resourceID);
	if (!tangents.empty())
	{
		OGL::bufferData(GL_ARRAY_BUFFER, tangents.size() * sizeof(glm::vec3), &tangents[0], GL_STATIC_DRAW);
	}
}

TangentBuffer::~TangentBuffer()
{
	OGL::deleteBuffers(1, &_resourceID);
}

void TangentBuffer::activate()
{
	// 3rd attribute buffer : normals
	OGL::enableVertexAttribArray(3);
	OGL::bindBuffer(GL_ARRAY_BUFFER, _resourceID);
	OGL::vertexAttribPointer(
		3,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);
}

void TangentBuffer::deactivate()
{
	OGL::disableVertexAttribArray(3);
}