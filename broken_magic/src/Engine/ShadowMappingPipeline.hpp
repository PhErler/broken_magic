#pragma once

#include "Resource.hpp"
#include "Texture.hpp"
#include "GLSLProgram.hpp"
#include "ResourceManager.hpp"
#include "IRenderPipeline.hpp"
#include "GameObject.hpp"

#include <vector>
#include <string>

#include <GL/glew.h>

#include <glm/glm.hpp>


namespace MagicEngine
{
	class ShadowMappingPipeline : public IRenderPipeline
	{
	public:
		ShadowMappingPipeline(Camera* cam);
		virtual ~ShadowMappingPipeline();

		virtual void render() override;
		virtual void draw(GameObject* gameObject) override;

	protected:
		void shadingPass();
		void zBufferPass();

		void shadingDraw(GameObject * gameObject);
		void zBufferDraw(GameObject * gameObject);

		int _state = 0;

		FrameBuffer* _shadowFrameBuffer;
		RenderTexture* _shadowTexture;

		std::shared_ptr<GLSLProgram> _shader;
		std::shared_ptr<GLSLProgram> _depthProgram;

		glm::vec2 _textureTiling = glm::vec2(1, 1);

		GLint _zPassLightMVPLocation = 0;

		struct
		{
			// vertex shader
			GLint MvpMatrix = 0;
			GLint LightBiasMVP = 0;
			GLint ViewMatrix = 0;
			GLint ModelMatrix = 0;
			GLint InvLightDir = 0;

			// fragment shader
			GLint DiffuseTexture = 0;
			GLint ShadowMap = 0;
			GLint DiffuseColor = 0;
			GLint AmbientColor = 0;
			GLint SpecularColor = 0;
			GLint EyeWorld = 0;
			GLint ShadowMappingSampleDistance = 0;
			GLint TextureTiling = 0;
			GLint SpecularExponent = 0;
		} _uniformLocations;
	};
}

