#pragma once

#include "Resource.hpp"

namespace MagicEngine
{
	typedef std::vector<std::vector<std::vector<unsigned char>>> tex3D;

	class VolumeTexture : public ITexture
	{
	public:
		VolumeTexture(std::string file);
		virtual ~VolumeTexture();

		virtual void activate(unsigned int textureSampler);
		virtual void deactivate(unsigned int textureSampler);

	private:
		/*
		void createWindowTestTexture(tex3D &tex);
		void createBricksTestTexture(tex3D &tex);
		void createSlicesTestTexture(tex3D &tex);
		void createShadowTestTexture(tex3D &tex);

		void createWoolTexture(tex3D &tex, int playerNumber);
		*/

		void createCloudTexture(tex3D &tex);

		void saveTextureToFile(std::string fileName, tex3D &tex);
		void loadTextureFromFile(std::string fileName, tex3D &tex);
	};
}
