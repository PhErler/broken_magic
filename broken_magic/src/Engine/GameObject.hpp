#pragma once

#include "ResourceManager.hpp"

#include <vector>
#include <memory>
#include <string>

#include <GL/glew.h>

#include <glm/glm.hpp>

namespace MagicEngine
{
	class IRenderPipeline;

	class GameObject
	{
	public:
		enum SceneParts {
			Outdoor = 0x01,
			Indoor  = 0x02,
			OutsideSnowball = 0x04
		};

		GameObject(); // transparent -> forward rendering; not transparent -> deferred rendering
		virtual ~GameObject();

		virtual void setModelData(std::string name, std::string materialPath);
		virtual void setModelDataForLight(std::shared_ptr<ModelData> sphereForLight);
		void setVolumeTexture(std::shared_ptr<VolumeTexture> volumeTexture) { _volumeTexture = volumeTexture; }
		void setProgram(std::shared_ptr<GLSLProgram> program) { _program = program; }

		std::shared_ptr<ModelData> getModelData() { return _modelData; }
		std::shared_ptr<Texture> getTexture() { return _texture; }
		std::shared_ptr<Texture> getNormalTexture() { return _normalTexture; }
		std::shared_ptr<VolumeTexture> getVolumeTexture() { return _volumeTexture; }
		std::shared_ptr<GLSLProgram> getProgram() { return _program; }

		virtual void update(double dt);
		virtual void render(IRenderPipeline* renderPipeline);

		bool isVisible();
		void setVisibility(bool visible){ _isVisible = visible; }

		bool isTransparent() { return _isTransparent; }

		bool isLightSource() { return _isLightSource; }

		void setVolumetric(bool volumetric) { _isVolumetric = volumetric; }
		bool isVolumetric() { return _isVolumetric; }

		void setInScenePart(unsigned char scenePart) { _inScenePart = scenePart; }
		bool isInScenePart(unsigned char scenePart) { return (_inScenePart & scenePart) != 0; }

		void setParent(GameObject* parent);
		virtual const GameObject* getParent();
		virtual void addChild(GameObject * child);

		virtual void translate(float x, float y, float z);
		virtual void translate(glm::vec3 vec) { translate(vec.x, vec.y, vec.z); }
		virtual void rotate(float x, float y, float z, float angle);
		virtual void scale(float x, float y, float z);
		virtual void scale(float uniformScale);

		void animateRotate(float x, float y, float z, float angle);
		void animateTranslate(float x, float y, float z);
		void animateScale(float scale);
		bool isAnimated();
		glm::vec4 getRotate();

		virtual void setModelMatrix(const glm::mat4 &newMatrix);
		virtual const glm::mat4& getModelMatrix() const; // without parent's modelmatrix
		virtual const glm::mat4 getWorldspaceMatrix() const; // including parent's modelmatrix

		virtual glm::vec3 getPosition();
		virtual void setPosition(glm::vec3 position);

		void setAnimatetReverse(float angle);

		bool isInViewFrustum(const glm::mat4 &MVP);

		float getSpecularExponent() { return _specularExponent; }

		void setGeometryName(std::string name) { _name = name; }
		std::string getGeometryName() { return _name; }
		GameObject* getChildWithName(std::string name);

	protected:
		std::shared_ptr<ModelData> _modelData;
		std::shared_ptr<Texture> _texture;
		std::shared_ptr<Texture> _normalTexture;
		std::shared_ptr<VolumeTexture> _volumeTexture;
		std::shared_ptr<GLSLProgram> _program;

		GameObject* _parent = nullptr;
		std::vector<GameObject*> _children;

		bool _animationRot = false;
		bool _animationTrans = false;
		bool _animationScale = false;
		float _animateAngle[2];
		bool _animateDirection[2];

		bool _isVisible = true;
		bool _isTransparent = false;
		bool _isLightSource = false;
		bool _isVolumetric = false;
		unsigned char _inScenePart = 0;

		float _specularExponent = 5;

		glm::mat4 _modelMatrix;
		glm::vec4 _rotVec;
		glm::vec3 _animateTransVector;
		float _animateScaleFactor;

		std::string _name;
	};
}

