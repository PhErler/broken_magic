#pragma once
// Based on the OpenGL tutorial http://ogldev.atspace.co.uk/www/tutorial37/tutorial37.html

#include "FrameBuffer.hpp"
#include "RenderTexture.hpp"
#include "IRenderPipeline.hpp"

#include <vector>

#include <GL/glew.h>

namespace MagicEngine
{
	class Light;

	class DeferredRenderingPipeline : public IRenderPipeline
	{
	public:
		enum GBUFFER_TEXTURE_TYPE {
			GBUFFER_TEXTURE_TYPE_POSITION = 0,
			GBUFFER_TEXTURE_TYPE_DIFFUSE = 1,
			GBUFFER_TEXTURE_TYPE_NORMAL = 2,
			GBUFFER_NUM_TEXTURES = 3,
		};

		DeferredRenderingPipeline(Camera* cam);
		~DeferredRenderingPipeline();

		virtual void render() override;
		virtual void draw(GameObject* gameObject) override;

		RenderTexture* getGBufferTexture(GBUFFER_TEXTURE_TYPE type);

	protected:
		void geometryPass();
		void stencilPass(Light* light);
		void lightPassZBuffer(Light* light);
		void lightPass(Light* light);
		void finalPass();
		void directionalLightPass();
		
		void geometryDraw(GameObject* gameObject);
		void stencilDraw(GameObject* gameObject);
		void lightDrawZBuffer(GameObject* gameObject);

		void lightDraw(Light* gameObject);

		void setLight(Light* light);

		int _state = 0;

		std::shared_ptr<VertexBuffer> _quadVertexBuffer;

		std::vector<RenderTexture*> _textures;
		RenderTexture* _finalImage;
		RenderTexture* _omniShadowMap;

		FrameBuffer* _geometryPassFbo;
		FrameBuffer* _finalFbo;

		FrameBuffer* _omniDirShadowFbo;

		std::shared_ptr<GLSLProgram> _geometryPassProgram;
		std::shared_ptr<GLSLProgram> _stencilPassProgram;
		std::shared_ptr<GLSLProgram> _lightZBufferPassProgram;
		std::shared_ptr<GLSLProgram> _lightPassProgram;
		std::shared_ptr<GLSLProgram> _directionalLightPassProgram;

		struct LightPassUniformLocations
		{
			GLint MVP;
			GLint EyeWorld;
			GLint PosTextureUnit;
			GLint NormalTextureUnit;
			GLint ColorTextureUnit;
			GLint MatSpecularPower;
			GLint ScreenSize;
		};

		struct
		{
			GLint MVP;
			GLint V;
			GLint M;
			GLint DiffuseTexture;
			GLint NormalTexture;
			GLint normalmapping;
		} _geometryPassUniformLocations;

		GLint _stencilPassMVPLocation;

		struct
		{
			GLint ModelMatrix;
			GLint CubemapMatrix;
			GLint NearFar;
			GLint LightPos;
		} _pointLightZBufferPassUniformLocations;

		struct
		{
			LightPassUniformLocations LightPass;
			GLint DiffuseColor;
			GLint AmbientColor;
			GLint SpecularColor;
			GLint Position;
			GLint NearFar;
			GLint CubeShadowMap;
			struct {
				GLint Constant;
				GLint Linear;
				GLint Exp;
			} Atten;
		} _pointLightPassUniformLocations;

		struct
		{
			LightPassUniformLocations LightPass;
			GLint DiffuseColor;
			GLint AmbientColor;
			GLint SpecularColor;
			GLint Direction;
		} _directionalLightPassUniformLocations;
	};
}
