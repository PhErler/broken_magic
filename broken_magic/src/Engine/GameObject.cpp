#include "GameObject.hpp"

#include "MagicEngineMain.hpp"
#include "Texture.hpp"
#include "IRenderPipeline.hpp"

using namespace std;
using namespace glm;
using namespace MagicEngine;

GameObject::GameObject()
: _modelMatrix(1.0)
{
	// Set default
	_inScenePart = 0xFF;
}

GameObject::~GameObject()
{
	_modelData.reset();
	_texture.reset();
	_normalTexture.reset();
	_volumeTexture.reset();
	_program.reset();

	for (GameObject* &child : _children) {
		delete child;
	}
	_children.clear();
}

void MagicEngine::GameObject::setModelData(std::string name, std::string materialPath)
{
	// Read our mesh
	if (name.length() != 0)
	{
		_modelData = ResourceManager::getModelData(name);
	}

	// Load the texture using any two methods
	if (materialPath.empty())
		_texture = ResourceManager::getTexture(_modelData->_material._path);
	else
		_texture = ResourceManager::getTexture(materialPath);

	// Load the Normal texture
	/*if (materialPath.empty()) {
		string tmp = _modelData->_material._path;
		tmp.replace(tmp.find("."), string(".").size(), "_n.");
		_normalTexture = ResourceManager::getTexture(_modelData->_material._path);
	}		
	else {
		materialPath.replace(materialPath.find("."), string(".").size(), "_n.");
		_normalTexture = ResourceManager::getTexture(materialPath);
	}*/
		
}

void GameObject::setModelDataForLight(shared_ptr<ModelData> sphereForLight) {
	_modelData = sphereForLight; 

	//just to be able to render the sphere light
	_texture = ResourceManager::getTexture(_modelData->_material._path);
}

void GameObject::setParent(GameObject* parent) {
	_parent = parent;
}

const GameObject* GameObject::getParent() {
	return _parent;
}

void GameObject::addChild(GameObject* child) {
	if (child->getParent() != 0)
		cout << "child can only have one parent!";

	child->setParent(this);
	_children.push_back(child);
}

void GameObject::translate(float x, float y, float z)
{
	_modelMatrix = glm::translate(_modelMatrix, glm::vec3(x, y, z));
}

void GameObject::rotate(float x, float y, float z, float angle)
{
	_modelMatrix = glm::rotate<float>(_modelMatrix, angle, glm::vec3(x, y, z));
}

void GameObject::scale(float x, float y, float z)
{
	_modelMatrix = glm::scale(_modelMatrix, glm::vec3(x, y, z));
}

void GameObject::scale(float uniformScale)
{
	_modelMatrix = glm::scale(_modelMatrix, glm::vec3(uniformScale, uniformScale, uniformScale));
}

void GameObject::animateRotate(float x, float y, float z, float angle)
{
	_rotVec = glm::vec4(x, y, z, angle);
	_animationRot = true;
}

void GameObject::animateTranslate(float x, float y, float z)
{
	_animateTransVector = glm::vec3(x, y, z);
	_animationTrans = true;
}

void GameObject::animateScale(float scale)
{
	_animateScaleFactor = scale;
	_animationScale = true;
}

bool GameObject::isAnimated(){
	return _animationRot || _animationTrans || _animationScale;
}

glm::vec4 GameObject::getRotate(){
	return _rotVec;
}

void GameObject::setAnimatetReverse(float angle)
{
	_animateAngle[0] = angle;
	_animateAngle[1] = angle;
	_animateDirection[0] = true;
	_animateDirection[1] = true;
}

bool GameObject::isInViewFrustum(const glm::mat4 &MVP)
{
	if (!_modelData)
	{
		return false;
	}

	for (vec3 &vertex : _modelData->_aabbVertices)
	{
		vec4 posNDC = MVP * vec4(vertex, 1);

		// no perspective division, test with w to keep signs
		if (posNDC.x >= -posNDC.w && posNDC.x < posNDC.w ||
			posNDC.y >= -posNDC.w && posNDC.y < posNDC.w ||
			posNDC.z >= -posNDC.w && posNDC.z < posNDC.w)
		{
			return true;
		}
	}

	return false;
}

GameObject* GameObject::getChildWithName(std::string name)
{
	GameObject* foundObject = nullptr;
	if (!_children.empty())
	{
		for (GameObject* &child : _children)
		{
			if (child->getGeometryName() == name)
			{
				foundObject = child;
			}
			else
			{
				foundObject = child->getChildWithName(name);
			}
			if (foundObject)
			{
				break;
			}
		}
	}

	return foundObject;
}

void GameObject::update(double dt)
{
	if (_animationRot == true)
	{
		if (_animateDirection[0])
		{
			if (_animateDirection[1])
			{
				_modelMatrix = glm::rotate<float>(_modelMatrix, _rotVec[3] * (float)dt, vec3(_rotVec));
				_animateAngle[1] -= _rotVec[3] * (float)dt;

			}
			else
			{
				_modelMatrix = glm::rotate<float>(_modelMatrix, (-1) * _rotVec[3] * (float)dt, vec3(_rotVec));
				_animateAngle[1] += _rotVec[3] * (float)dt;
			}

			if (_animateAngle[1] <= 0.0f)  _animateDirection[1] = false;
			if (_animateAngle[1] >= _animateAngle[0]) _animateDirection[1] = true;
		}
		else
		{
			_modelMatrix = glm::rotate<float>(_modelMatrix, _rotVec[3] * (float)dt, vec3(_rotVec));
		}
	}

	if (_animationTrans == true)
	{
		_modelMatrix = glm::translate(_modelMatrix, _animateTransVector * (float)dt);
	}

	if (_animationScale == true)
	{
		_modelMatrix = _modelMatrix * glm::scale(mat4(), vec3(exp((float)-dt * _animateScaleFactor)));
	}

	// update children
	if (!_children.empty())
	{
		//cout << "update children!" << endl;
		for (GameObject* &child : _children) {
			child->update(dt);
		}
	}
}

void MagicEngine::GameObject::render(IRenderPipeline* renderPipeline)
{
	if (!_isVisible)
	{
		return;
	}

	// render children first
	if (!_children.empty())
	{
		//cout << "render children!" << endl;
		for (GameObject* &child : _children) 
		{
			child->render(renderPipeline);
		}
	}

	renderPipeline->draw(this);
}

bool MagicEngine::GameObject::isVisible() 
{ 
	return _isVisible && isInScenePart(MagicEngineMain::getCurrentSceneState()); 
}

const glm::mat4& GameObject::getModelMatrix() const
{
	return _modelMatrix;
}

const glm::mat4 GameObject::getWorldspaceMatrix() const
{
	glm::mat4 renderModelMatrix = _modelMatrix;
	if (_parent)
	{
		renderModelMatrix = _parent->getWorldspaceMatrix() * renderModelMatrix;
	}
	return renderModelMatrix;
}

void GameObject::setModelMatrix(const mat4 &newMatrix)
{
	_modelMatrix = newMatrix;
}

glm::vec3 GameObject::getPosition()
{ 
	return vec3(getWorldspaceMatrix()[3]);
}

void GameObject::setPosition(glm::vec3 position)
{
	_modelMatrix[3][0] = position.x;
	_modelMatrix[3][1] = position.y;
	_modelMatrix[3][2] = position.z;
}
