#include "RenderTexture.hpp"

#include "OGL.hpp"

using namespace std;
using namespace MagicEngine;

RenderTexture::RenderTexture(GLuint width, GLuint height, RenderTextureType type)
{
	// render texture. Slower than a render buffer, but you can sample it later in your shader
	OGL::genTextures(1, &_resourceID);
	if (type == DepthCubemap)
	{
		ITexture::bindTextureUnit(GL_TEXTURE_CUBE_MAP, _resourceID);

		OGL::texParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		OGL::texParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		OGL::texParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		// works only with shadow sampler
		OGL::texParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
		OGL::texParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

		OGL::texParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		OGL::texParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		for (unsigned int face = 0; face < 6; ++face)
		{
			OGL::texImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, 0);
		}

		ITexture::bindTextureUnit(GL_TEXTURE_CUBE_MAP, 0);
	}
	else
	{
		ITexture::bindTextureUnit(GL_TEXTURE_2D, _resourceID);

		if (type == RenderTextureType::RGB)
		{
			OGL::texImage2D(GL_TEXTURE_2D, 0, GL_RGB,
				width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);

			OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		}
		else if (type == RenderTextureType::RGB32F)
		{
			OGL::texImage2D(GL_TEXTURE_2D, 0, GL_RGB32F,
				width, height, 0, GL_RGB, GL_FLOAT, NULL);

			OGL::texParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			OGL::texParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		}
		else if (type == RenderTextureType::RGBA)
		{
			OGL::texImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
				width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

			OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		}
		else if (type == RenderTextureType::DepthWithStencil)
		{
			OGL::texImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_STENCIL,
				width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0);

			OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		}
		else if (type == RenderTextureType::Depth)
		{
			OGL::texImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32,
				width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

			// works only with shadow sampler
			OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LESS);
			OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);

			OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		}

		// edge handling
		OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		OGL::texParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		ITexture::bindTextureUnit(GL_TEXTURE_2D, 0);
	}

	_width = width;
	_height = height;

	_type = type;
}

RenderTexture::~RenderTexture()
{
	OGL::deleteTextures(1, &_resourceID);
}

void RenderTexture::activate(unsigned int textureSampler)
{
	activateTextureUnit(textureSampler);
	if (_type == DepthCubemap)
	{
		ITexture::bindTextureUnit(GL_TEXTURE_CUBE_MAP, _resourceID);
	}
	else
	{
		ITexture::bindTextureUnit(GL_TEXTURE_2D, _resourceID);
	}

#ifdef _DEBUG
	_boundForRead = true;
#endif
}

void RenderTexture::deactivate(unsigned int textureSampler)
{
	activateTextureUnit(textureSampler);
	if (_type == DepthCubemap)
	{
		ITexture::bindTextureUnit(GL_TEXTURE_CUBE_MAP, 0);
	}
	else
	{
		ITexture::bindTextureUnit(GL_TEXTURE_2D, 0);
	}

#ifdef _DEBUG
	_boundForRead = false;
#endif
}

#ifdef _DEBUG
void MagicEngine::RenderTexture::setBoundForWrite(bool boundForWrite) 
{
	_boundForWrite = boundForWrite;

	// can't write to texture if it is bound for read
	assert(!(_boundForWrite && _boundForRead));
}
#endif
