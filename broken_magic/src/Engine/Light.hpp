#pragma once

#include "GameObject.hpp"

#include <glm/glm.hpp>

namespace MagicEngine
{
	struct BaseLight
	{
		BaseLight(glm::vec3 diffuseColor,
				  glm::vec3 ambientColor,
				  glm::vec3 specularColor)
		: DiffuseColor(diffuseColor)
		, AmbientColor(ambientColor)
		, SpecularColor(specularColor)
		{
		}

		glm::vec3 DiffuseColor;
		glm::vec3 AmbientColor;
		glm::vec3 SpecularColor;
	};

	struct Attenuation
	{
		Attenuation(float constant, float linear, float quadratic)
			: Constant(constant), Linear(linear), Quadratic(quadratic)
		{
		}

		float Constant;
		float Linear;
		float Quadratic;
	};

	class Light : public GameObject
	{
	public:
		Light(glm::vec3 position,
			Attenuation attenuation,
			BaseLight baseLight,
			float angleInnerCone,
			float angleOuterCone,
			glm::vec3 direction,
			int type,
			float nearPlane,
			float farPlane);

		virtual ~Light();

		virtual void setModelData(std::string name, std::string materialPath) override;

		float getScale() { return _scale; }

		virtual glm::vec3 getLocalPosition();
		virtual const glm::mat4 getWorldspaceMatrix() const override; // including parent's modelmatrix

		glm::vec3 _position;
		BaseLight _baseLight;
		Attenuation _attenuation;
		float _angleInnerCone;
		float _angleOuterCone;
		glm::vec3 _direction;
		int _type;
		float _nearPlane;
		float _farPlane;

	protected:
		float calcPointLightBSphere();
		void updateScale();

		float _scale; // for stencil pass

	private:
		void init();
	};
}

