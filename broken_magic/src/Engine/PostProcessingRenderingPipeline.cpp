#include "PostProcessingRenderingPipeline.hpp"

#include "MagicEngineMain.hpp"
#include "OGL.hpp"
#include "ResourceManager.hpp"

#include <glm\glm.hpp>

using namespace MagicEngine;
using namespace glm;
using namespace std;

	float PostProcessingRenderingPipeline::s_focalDistance = 0.6f;
	float PostProcessingRenderingPipeline::s_focalRange = 0.1f;
	float PostProcessingRenderingPipeline::s_exposure = 0.0f;

	PostProcessingRenderingPipeline::PostProcessingRenderingPipeline(Camera* cam)
		: IRenderPipeline(cam), _quadVertexBuffer(ResourceManager::getQuad())
	{

		const CommandLineParams &winParams = MagicEngineMain::getCommandLineParams();
		_width = winParams._xRes;
		_height = winParams._yRes;

		/* ------------------------ DEPTH OF FIELD ------------------------ */
		_blurValueProgram = ResourceManager::getShader("resources\\shaders\\DOFpassthrough.vs", "resources\\shaders\\DOFBlurValue.fs");
		_gaussianProgram = ResourceManager::getShader("resources\\shaders\\Passthrough.vs", "resources\\shaders\\BloomGaussian.fs");
		//_dofFinalProgram = ResourceManager::getShader("resources\\shaders\\DOFpassthrough.vs", "resources\\shaders\\DOFfinal.fs");

		_blurValueUniformLocations.Tex0 = _blurValueProgram->getUniformLocation("Tex0");
		_blurValueUniformLocations.focalDistance = _blurValueProgram->getUniformLocation("focalDistance");
		_blurValueUniformLocations.focalRange = _blurValueProgram->getUniformLocation("focalRange");
		_blurValueUniformLocations.nearFar = _blurValueProgram->getUniformLocation("nearFar");

		_gaussianUniformLocations.Tex0 = _gaussianProgram->getUniformLocation("Tex0");
		_gaussianUniformLocations.horizontal = _gaussianProgram->getUniformLocation("horizontal");
		_gaussianUniformLocations.width = _gaussianProgram->getUniformLocation("Width");
		_gaussianUniformLocations.height = _gaussianProgram->getUniformLocation("Height");

		/*_dofFinalProgramUniformLocations.colorTex = _dofFinalProgram->getUniformLocation("colorTex");
		_dofFinalProgramUniformLocations.gaussianTex = _dofFinalProgram->getUniformLocation("gaussianTex");
		_dofFinalProgramUniformLocations.blurValueTex = _dofFinalProgram->getUniformLocation("blurValueTex");
		*/

		_dofDepthTex = new RenderTexture(_width, _height, RenderTexture::RGBA);
		_dofGaussianTex = new RenderTexture(_width, _height, RenderTexture::RGBA);
		_bloomBrightnessTex = new RenderTexture(_width, _height, RenderTexture::RGBA);
		_bloomGaussianTex = new RenderTexture(_width, _height, RenderTexture::RGBA);
		_compositeTex = new RenderTexture(_width, _height, RenderTexture::RGBA);

		std::vector<GLenum> attachments = { GL_COLOR_ATTACHMENT0 };

		_dofDepthFbo = new FrameBuffer(&vector<RenderTexture*>({ _dofDepthTex }), attachments, nullptr, false);
		_dofGaussianFbo = new FrameBuffer(&vector<RenderTexture*>({ _dofGaussianTex }), attachments, nullptr, false);

		/* ------------------------ BLOOM ------------------------ */

		_brightColorProgram = ResourceManager::getShader("resources\\shaders\\DOFpassthrough.vs", "resources\\shaders\\BloomBrightColor.fs");
		//_blendingProgram = ResourceManager::getShader("resources\\shaders\\DOFpassthrough.vs", "resources\\shaders\\BloomBlending.fs");

		_brightColorUniformLocations.Tex0 = _brightColorProgram->getUniformLocation("Tex0");

		/*_blendingUniformLocations.Tex0 = _blendingProgram->getUniformLocation("Tex0");
		_blendingUniformLocations.Tex1 = _blendingProgram->getUniformLocation("Tex1");
		//_blendingUniformLocations.exposure = _blendingProgram->getUniformLocation("exposure"); // Only necessary for HDR
		*/

		_bloomBrightnessFbo = new FrameBuffer(&vector<RenderTexture*>({ _bloomBrightnessTex }), attachments, nullptr, false);
		_bloomGaussianFbo = new FrameBuffer(&vector<RenderTexture*>({ _bloomGaussianTex }), attachments, nullptr, false);

		/* ------------------------ Blending all effects together ------------------------ */
		_finalProgram = ResourceManager::getShader("resources\\shaders\\DOFpassthrough.vs", "resources\\shaders\\PostProcessComposition.fs");
		_finalProgramUniformLocations.color             = _finalProgram->getUniformLocation("colorTex");
		_finalProgramUniformLocations.dof_blurValueTex  = _finalProgram->getUniformLocation("DOF_blurValueTex");
		_finalProgramUniformLocations.dof_gaussianTex   = _finalProgram->getUniformLocation("DOF_gaussianTex");
		_finalProgramUniformLocations.bloom_gaussianTex = _finalProgram->getUniformLocation("BLOOM_gaussian");
		_finalProgramUniformLocations.exposure          = _finalProgram->getUniformLocation("exposure");

		_compositeFbo = new FrameBuffer(&vector<RenderTexture*>({ _compositeTex }), attachments, nullptr, false);

}

PostProcessingRenderingPipeline::~PostProcessingRenderingPipeline() 
{
	_blurValueProgram.reset();
	_gaussianProgram.reset();
	_finalProgram.reset();
	_brightColorProgram.reset();
	//_blendingProgram.reset();
	//_dofFinalProgram.reset();

	if (_dofDepthTex)
	{
		delete _dofDepthTex;
		_dofDepthTex = nullptr;
	}
	if (_dofGaussianTex)
	{
		delete _dofGaussianTex;
		_dofGaussianTex = nullptr;
	}
	if (_bloomBrightnessTex)
	{
		delete _bloomBrightnessTex;
		_bloomBrightnessTex = nullptr;
	}
	if (_bloomGaussianTex)
	{
		delete _bloomGaussianTex;
		_bloomGaussianTex = nullptr;
	}
	if (_compositeTex)
	{
		delete _compositeTex;
		_compositeTex = nullptr;
	}

	if (_dofDepthFbo)
	{
		delete _dofDepthFbo;
		_dofDepthFbo = nullptr;
	}
	if (_dofGaussianFbo)
	{
		delete _dofGaussianFbo;
		_dofGaussianFbo = nullptr;
	}
	if (_bloomBrightnessFbo)
	{
		delete _bloomBrightnessFbo;
		_bloomBrightnessFbo = nullptr;
	}
	if (_bloomGaussianFbo)
	{
		delete _bloomGaussianFbo;
		_bloomGaussianFbo = nullptr;
	}
	if (_compositeFbo)
	{
		delete _compositeFbo;
		_compositeFbo = nullptr;
	}
}

void PostProcessingRenderingPipeline::render() 
{
	if (!_cam)
	{
		return;
	}

	OGL::disable(GL_CULL_FACE);

	/* --------------- DEPTH OF FIELD --------------- */
	// First Pass: scene rendering

	_dofDepthFbo->activate();
	_dofDepthFbo->clear(true, true, false, 0.0f, 0.0f, 0.0f);
	_blurValueProgram->activate();
	_cam->getRenderDepthTexture()->activate(0);
	_blurValueProgram->bindTexture(_blurValueUniformLocations.Tex0, 0);
	_blurValueProgram->setUniform(_blurValueUniformLocations.focalDistance, s_focalDistance);
	_blurValueProgram->setUniform(_blurValueUniformLocations.focalRange, s_focalRange);
	_blurValueProgram->setUniform(_blurValueUniformLocations.nearFar, _cam->getNearFar());

	_quadVertexBuffer->activate();
	_quadVertexBuffer->draw();
	_quadVertexBuffer->deactivate();

	_cam->getRenderDepthTexture()->deactivate(0);
	_blurValueProgram->deactivate();
	_dofDepthFbo->deactivate();

	// Second Pass: Gaussian Filtering

	bool first_iteration = true;
	bool horizontal = false;
	int amount = 5;

	_dofGaussianFbo->activate();
	_dofGaussianFbo->clear(true, true, false, 0.0f, 0.0f, 0.0f);
	_gaussianProgram->activate();

	for (int i = 0; i < amount; i++) {
		if (first_iteration) {
			_cam->getRenderColorTexture()->activate(0);
			_gaussianProgram->bindTexture(_gaussianUniformLocations.Tex0, 0);
		}
		else {
			_dofGaussianTex->activate(0);
			_gaussianProgram->bindTexture(_gaussianUniformLocations.Tex0, 0);
		}

		_gaussianProgram->setUniform(_gaussianUniformLocations.horizontal, (horizontal) ? GL_TRUE : GL_FALSE);
		_gaussianProgram->setUniform(_gaussianUniformLocations.width, _width);
		_gaussianProgram->setUniform(_gaussianUniformLocations.height, _height);

		_quadVertexBuffer->activate();
		_quadVertexBuffer->draw();
		_quadVertexBuffer->deactivate();

		horizontal = !horizontal;

		if (first_iteration)
		{
			_cam->getRenderColorTexture()->deactivate(0);
			first_iteration = false;
		}
		else
		{
			_dofGaussianTex->deactivate(0);
		}

	}
	_gaussianProgram->deactivate();
	_dofGaussianFbo->deactivate();

	
	// Third pass: final compositing
	/*
	attachments = { GL_COLOR_ATTACHMENT2 };
	_dofFbo->activateForWrite(attachments);
	_dofFbo->clear(true, true, false, 0.0f, 0.0f, 0.0f);
	_dofFinalProgram->activate();
	_cam->getRenderColorTexture()->activate(0);
	_dofFinalProgram->bindTexture(_dofFinalProgramUniformLocations.colorTex, 0);

	_texturesDof[DepthOfFieldRenderingPipeline_TEXTURE_TYPE_DEPTH]->activate(1);
	_dofFinalProgram->bindTexture(_dofFinalProgramUniformLocations.blurValueTex, 1);

	_texturesDof[DepthOfFieldRenderingPipeline_TEXTURE_TYPE_GAUSSIAN]->activate(2);
	_dofFinalProgram->bindTexture(_dofFinalProgramUniformLocations.gaussianTex, 2);

	_quadVertexBuffer.activate();
	_quadVertexBuffer.draw();
	_quadVertexBuffer.deactivate();

	_cam->getRenderColorTexture()->deactivate(0);
	_texturesDof[DepthOfFieldRenderingPipeline_TEXTURE_TYPE_DEPTH]->deactivate(1);
	_texturesDof[DepthOfFieldRenderingPipeline_TEXTURE_TYPE_GAUSSIAN]->deactivate(2);
	_dofFinalProgram->deactivate();
	_dofFbo->deactivate();
	*/
	/* --------------- BLOOM --------------- */

	// First Pass: extract bright regions

	_bloomBrightnessFbo->activate();
	_bloomBrightnessFbo->clear(true, true, false, 0.0f, 0.0f, 0.0f);
	_brightColorProgram->activate();
	_cam->getRenderColorTexture()->activate(0);
	_brightColorProgram->bindTexture(_brightColorUniformLocations.Tex0, 0);

	_quadVertexBuffer->activate();
	_quadVertexBuffer->draw();
	_quadVertexBuffer->deactivate();

	_cam->getRenderColorTexture()->deactivate(0);
	_brightColorProgram->deactivate();
	_bloomBrightnessFbo->deactivate();

	// Second Pass: Gaussian Filtering

	first_iteration = true;
	horizontal = false;
	amount = 5;

	_bloomGaussianFbo->activate();
	_bloomGaussianFbo->clear(true, true, false, 0.0f, 0.0f, 0.0f);
	_gaussianProgram->activate();

	for (int i = 0; i < amount; i++) {
		if (first_iteration) {
			_bloomBrightnessTex->activate(0);
			_gaussianProgram->bindTexture(_gaussianUniformLocations.Tex0, 0);
		}
		else {
			_bloomGaussianTex->activate(0);
			_gaussianProgram->bindTexture(_gaussianUniformLocations.Tex0, 0);
		}

		_gaussianProgram->setUniform(_gaussianUniformLocations.horizontal, (horizontal) ? GL_TRUE : GL_FALSE);
		_gaussianProgram->setUniform(_gaussianUniformLocations.width, _width);
		_gaussianProgram->setUniform(_gaussianUniformLocations.height, _height);

		_quadVertexBuffer->activate();
		_quadVertexBuffer->draw();
		_quadVertexBuffer->deactivate();

		horizontal = !horizontal;

		if (first_iteration)
		{
			_bloomBrightnessTex->deactivate(0);
			first_iteration = false;
		}
		else
		{
			_bloomGaussianTex->deactivate(0);
		}

	}
	_gaussianProgram->deactivate();
	_bloomGaussianFbo->deactivate();

	// Third pass: Bloom blending
	/*
	attachments = { GL_COLOR_ATTACHMENT2 };
	_bloomFbo->activateForWrite(attachments);
	_bloomFbo->clear(true, true, false, 0.0f, 0.0f, 0.0f);
	_blendingProgram->activate();
	_cam->getRenderColorTexture()->activate(0);
	_blendingProgram->bindTexture(_blendingUniformLocations.Tex0, 0);

	_texturesBloom[DepthOfFieldRenderingPipeline_TEXTURE_TYPE_GAUSSIAN]->activate(1);
	_blendingProgram->bindTexture(_blendingUniformLocations.Tex1, 1);

	_quadVertexBuffer.activate();
	_quadVertexBuffer.draw();
	_quadVertexBuffer.deactivate();

	_cam->getRenderColorTexture()->deactivate(0);
	_texturesBloom[DepthOfFieldRenderingPipeline_TEXTURE_TYPE_GAUSSIAN]->deactivate(1);
	_blendingProgram->deactivate();
	_bloomFbo->deactivate();
	*/
	/* ----------- BLEND DOF AND BLOOM TOGETHER ------------- */

	_compositeFbo->activate();
	_compositeFbo->clear(true, true, false, 0.0f, 0.0f, 0.0f);
	_finalProgram->activate();
	_cam->getRenderColorTexture()->activate(0);
	_finalProgram->bindTexture(_finalProgramUniformLocations.color, 0);

	_dofDepthTex->activate(1);
	_finalProgram->bindTexture(_finalProgramUniformLocations.dof_blurValueTex, 1);

	_dofGaussianTex->activate(2);
	_finalProgram->bindTexture(_finalProgramUniformLocations.dof_gaussianTex, 2);

	_bloomGaussianTex->activate(3);
	_finalProgram->bindTexture(_finalProgramUniformLocations.bloom_gaussianTex, 3);

	_finalProgram->setUniform(_finalProgramUniformLocations.exposure, s_exposure);

	_quadVertexBuffer->activate();
	_quadVertexBuffer->draw();
	_quadVertexBuffer->deactivate();

	_cam->getRenderColorTexture()->deactivate(0);
	_dofDepthTex->deactivate(1);
	_dofGaussianTex->deactivate(2);
	_bloomGaussianTex->deactivate(3);
	_finalProgram->deactivate();
	_compositeFbo->deactivate();


	/* ----------- RESULTS TO THE CAMERA ------------- */
	// Write Results to the Camera
	_cam->getRenderBuffer()->activateForWrite(vector<GLenum>({ GL_COLOR_ATTACHMENT0 }));

	int debugState = MagicEngineMain::getDepthOfFieldRenderingState();
	bool debugStateOutput = MagicEngineMain::getOutputDepthOfFieldRenderingState();
	string text = "";
	switch (debugState)
	{
	case 0:
		_compositeFbo->activateForRead(GL_COLOR_ATTACHMENT0);
		text = "Result with Post Processing Effects" + to_string(s_focalDistance) + " and focalRange: " + to_string(s_focalRange);
		break;
	case 1:
		_dofDepthFbo->activateForRead(GL_COLOR_ATTACHMENT0);
		text = "Depth of field : focaldistance: " + to_string(s_focalDistance) + " and focalRange: " + to_string(s_focalRange);
		break;
	case 2:/*
		_dofFbo->activateForRead(GL_COLOR_ATTACHMENT2);
		text = "DOF with focalDistance: " + to_string(s_focalDistance) + " and focalRange: " + to_string(s_focalRange);
		break;
	case 3:*/
		_bloomGaussianFbo->activateForRead(GL_COLOR_ATTACHMENT0);
		text = "Bloom result";
		break;
	case 3:
		_cam->getRenderBuffer()->activateForRead(GL_COLOR_ATTACHMENT0);
		text = "Deferred Shading result";
		break;
	default:
		break;
	}
	// Output on console
	if (debugStateOutput)
		cout << text << endl;
	MagicEngineMain::DepthOfFieldRenderingStateOutputFalse(); // Set Output back to false

	const CommandLineParams &winParams = MagicEngineMain::getCommandLineParams();
	OGL::blitFramebuffer(0, 0, winParams._xRes, winParams._yRes,
		0, 0, winParams._xRes, winParams._yRes, GL_COLOR_BUFFER_BIT, GL_LINEAR);

	_dofDepthFbo->deactivate();
	_bloomGaussianFbo->deactivate();
	_compositeFbo->deactivate();
	_cam->getRenderBuffer()->deactivate();

}

void PostProcessingRenderingPipeline::draw(GameObject* gameObject) 
{
}
