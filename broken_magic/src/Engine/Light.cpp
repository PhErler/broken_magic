#include "Light.hpp"

#include "MagicEngineMain.hpp"
#include "Texture.hpp"

#include "glm\gtc\constants.hpp"

using namespace std;
using namespace glm;
using namespace MagicEngine;

Light::Light(
	glm::vec3 position,
	Attenuation attenuation,
	BaseLight baseLight,
	float angleInnerCone,
	float angleOuterCone,
	glm::vec3 direction,
	int type,
	float nearPlane,
	float farPlane)
	: _position(position),
	_attenuation(attenuation),
	_baseLight(baseLight),
	_angleInnerCone(angleInnerCone),
	_angleOuterCone(angleOuterCone),
	_direction(direction),
	_type(type),
	_nearPlane(nearPlane),
	_farPlane(farPlane)
{
	init();
}

void Light::init()
{
	translate(_position.x, _position.y, _position.z);

	updateScale();

	//_isTransparent = true; // test for debug rendering
	_isLightSource = true;

	MagicEngineMain::addLight(this);
}

Light::~Light()
{
	MagicEngineMain::removeLight(this);
}

// The calculation solves a quadratic equation (see http://en.wikipedia.org/wiki/Quadratic_equation)
float Light::calcPointLightBSphere()
{
	// color type doesn't matter, its all the same from assimp / blender
	vec3 colorSum = _baseLight.DiffuseColor + _baseLight.AmbientColor + _baseLight.SpecularColor;
	float MaxChannel = fmax(fmax(colorSum.x, colorSum.y), colorSum.z);

	// accuracy
	const float ValuesPerChannel = 256;

	float a = _attenuation.Quadratic;
	float b = _attenuation.Linear;
	float c = 1 + _attenuation.Constant - ValuesPerChannel * MaxChannel;

	// solutions are symmetrical. we need only one, the positive.
	float ret = 0;
	
	if (a == 0)
	{
		ret = abs(c / b);
	}
	else
	{
		ret = (-b + sqrtf(b * b - 4 * a * c)) / (2 * a);
	}

	return ret;
}

void Light::updateScale()
{
	_scale = calcPointLightBSphere();
}

void Light::setModelData(std::string name, std::string materialPath) {
	// Just to be sure that Lights stay with the Sphere Object and are not trying to search for any other ModelData
	// Load the texture using any two methods 
}

glm::vec3 Light::getLocalPosition()
{
	return vec3(_modelMatrix[3]);
}

const glm::mat4 Light::getWorldspaceMatrix() const
{
	glm::mat4 renderModelMatrix = glm::scale(_modelMatrix, vec3(_scale, _scale, _scale));
	if (_parent)
	{
		renderModelMatrix = _parent->getWorldspaceMatrix() * renderModelMatrix;
	}
	return renderModelMatrix;
}
