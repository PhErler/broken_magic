#pragma once

#include <string>
#include <assert.h>

#include <GL/glew.h>

namespace MagicEngine
{
	class OGL
	{
	public:

		static void checkError()
		{
#ifdef _DEBUG
			GLint err = glGetError();
			if (err != 0)
			{
				printf("OpenGL Error: %x \n", err);
				assert(err == 0);
			}
#endif // _DEBUG
		}

		static void activeTexture(GLenum texture) { glActiveTexture(texture); checkError(); };
		static void attachShader(GLuint program, GLuint shader) { glAttachShader(program, shader); checkError(); };
		static void bindBuffer(GLenum target, GLuint buffer) { glBindBuffer(target, buffer); checkError(); };
		static void bindFramebuffer(GLenum target, GLuint framebuffer) { glBindFramebuffer(target, framebuffer); checkError(); };
		static void bindTexture(GLenum target, GLuint texture) { glBindTexture(target, texture); checkError(); };
		static void bindVertexArray(GLuint array) { glBindVertexArray(array); checkError(); };
		static void blendFunc(GLenum sfactor, GLenum dfactor) { glBlendFunc(sfactor, dfactor); checkError(); };
		static void blitFramebuffer(GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter) { glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter); checkError(); };
		static void blendFuncSeparate(GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha) { glBlendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha); checkError(); };
		static void bufferData(GLenum target, GLsizeiptr size, const GLvoid * data, GLenum usage) { glBufferData(target, size, data, usage); checkError(); };
		static void bufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid * data) { glBufferSubData(target, offset, size, data); checkError(); };
		static void clear(GLbitfield mask) { glClear(mask); checkError(); };
		static void clearColor(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha) { glClearColor(red, green, blue, alpha); checkError(); };
		static void clearDepth(GLdouble depth) { glClearDepth(depth); checkError(); };
		static void clearDepth(GLfloat depth) { glClearDepth(depth); checkError(); };
		static void clearStencil(GLint s) { glClearStencil(s); checkError(); };
		static GLenum checkFramebufferStatus(GLenum target) { GLenum res = glCheckFramebufferStatus(target); checkError(); return res; }
		// named framebuffer static GLenum checkFramebufferStatus(GLuint framebuffer, GLenum target) { GLenum res = glCheckFramebufferStatus(framebuffer, target); checkError(); return res; }
		static void compileShader(GLuint shader) { glCompileShader(shader); checkError(); };
		static void compressedTexImage2D(GLenum target, GLint level, GLenum internalFormat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const GLvoid * data) { glCompressedTexImage2D(target, level, internalFormat, width, height, border, imageSize, data); checkError(); };
		static GLuint createProgram() { GLuint res = glCreateProgram(); checkError(); return res; }
		static GLuint createShader(GLenum shaderType) { GLuint res = glCreateShader(shaderType); checkError(); return res; }
		static void cullFace(GLenum mode) { glCullFace(mode); checkError(); };
		static void deleteBuffers(GLsizei n, const GLuint * buffers) { glDeleteBuffers(n, buffers); checkError(); };
		static void deleteFramebuffers(GLsizei n, const GLuint * framebuffers) { glDeleteFramebuffers(n, framebuffers); checkError(); };
		static void deleteProgram(GLuint program) { glDeleteProgram(program); checkError(); };
		static void deleteShader(GLuint shader) { glDeleteShader(shader); checkError(); };
		static void deleteTextures(GLsizei n, const GLuint * textures) { glDeleteTextures(n, textures); checkError(); };
		static void deleteVertexArrays(GLsizei n, const GLuint * arrays) { glDeleteVertexArrays(n, arrays); checkError(); };
		static void depthMask(GLboolean flag) { glDepthMask(flag); checkError(); };
		static void depthFunc(GLenum func) { glDepthFunc(func); checkError(); };
		static void disable(GLenum cap) { glDisable(cap); checkError(); };
		static void disableVertexAttribArray(GLuint index) { glDisableVertexAttribArray(index); checkError(); };
		static void drawArrays(GLenum mode, GLint first, GLsizei count) { glDrawArrays(mode, first, count); checkError(); };
		static void drawArraysInstanced(GLenum mode, GLint first, GLsizei count, GLsizei primcount) { glDrawArraysInstanced(mode, first, count, primcount); checkError(); };
		static void drawBuffer(GLenum buf) { glDrawBuffer(buf); checkError(); };
		static void drawBuffers(GLsizei n, const GLenum *bufs) { glDrawBuffers(n, bufs); checkError(); };
		static void enable(GLenum cap) { glEnable(cap); checkError(); };
		static void enableVertexAttribArray(GLuint index) { glEnableVertexAttribArray(index); checkError(); };
		static void framebufferTexture(GLenum target, GLenum attachment, GLuint texture, GLint level) { glFramebufferTexture(target, attachment, texture, level); checkError(); };
		static void framebufferTexture2D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level) { glFramebufferTexture2D(target, attachment, textarget, texture, level); checkError(); };
		static void genBuffers(GLsizei n, GLuint * buffers) { glGenBuffers(n, buffers); checkError(); };
		static void generateMipmap(GLenum target) { glGenerateMipmap(target); checkError(); };
		static void genFramebuffers(GLsizei n, GLuint *ids) { glGenFramebuffers(n, ids); checkError(); };
		static void genTextures(GLsizei n, GLuint *textures) { glGenTextures(n, textures); checkError(); };
		static void genVertexArrays(GLsizei n, GLuint *arrays) { glGenVertexArrays(n, arrays); checkError(); };
		static GLenum getError() { return glGetError(); };
		static void getProgramInfoLog(GLuint program, GLsizei maxLength, GLsizei *length, GLchar *infoLog) { glGetProgramInfoLog(program, maxLength, length, infoLog); checkError(); };
		static void getProgramiv(GLuint program, GLenum pname, GLint *params) { glGetProgramiv(program, pname, params); checkError(); };
		static void getShaderiv(GLuint shader, GLenum pname, GLint *params) { glGetShaderiv(shader, pname, params); checkError(); };
		static void getShaderInfoLog(GLuint shader, GLsizei maxLength, GLsizei *length, GLchar *infoLog) { glGetShaderInfoLog(shader, maxLength, length, infoLog); checkError(); };
		static GLint getUniformLocation(GLuint program, const GLchar* name) { GLint res = glGetUniformLocation(program, name); checkError(); return res; }
		static void linkProgram(GLuint program) { glLinkProgram(program); checkError(); };
		static void pixelStorei(GLenum pname, GLint param) { glPixelStorei(pname, param); checkError(); };
		static void polygonOffset(GLfloat factor, GLfloat units) { glPolygonOffset(factor, units); checkError(); };
		static void readBuffer(GLenum mode) { glReadBuffer(mode); checkError(); };
		static void shaderSource(GLuint shader, GLsizei count, const GLchar **string, const GLint *length) { glShaderSource(shader, count, string, length); checkError(); };
		static void stencilFunc(GLenum func, GLint ref, GLuint mask) { glStencilFunc(func, ref, mask); checkError(); };
		static void stencilOpSeparate(GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass) { glStencilOpSeparate(face, sfail, dpfail, dppass); checkError(); };
		static void texImage2D(GLenum target, GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid * data) { glTexImage2D(target, level, internalFormat, width, height, border, format, type, data); checkError(); };
		static void texImage3D(GLenum target, GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const GLvoid * data) { glTexImage3D(target, level, internalFormat, width, height, depth, border, format, type, data); checkError(); };
		static void texParameteri(GLenum target, GLenum pname, GLint param) { glTexParameteri(target, pname, param); checkError(); };
		static void texParameterf(GLenum target, GLenum pname, GLfloat param) { glTexParameterf(target, pname, param); checkError(); };
		static void uniform1i(GLint location, GLint v0) { glUniform1i(location, v0); checkError(); };
		static void uniform2i(GLint location, GLint v0, GLint v1) { glUniform2i(location, v0, v1); checkError(); };
		static void uniform3i(GLint location, GLint v0, GLint v1, GLint v2) { glUniform3i(location, v0, v1, v2); checkError(); };
		static void uniform4i(GLint location, GLint v0, GLint v1, GLint v2, GLint v3) { glUniform4i(location, v0, v1, v2, v3); checkError(); };
		static void uniform1f(GLint location, GLfloat v0) { glUniform1f(location, v0); checkError(); };
		static void uniform2f(GLint location, GLfloat v0, GLfloat v1) { glUniform2f(location, v0, v1); checkError(); };
		static void uniform3f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2) { glUniform3f(location, v0, v1, v2); checkError(); };
		static void uniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) { glUniform4f(location, v0, v1, v2, v3); checkError(); };
		static void uniform1ui(GLint location, GLuint v0) { glUniform1ui(location, v0); checkError(); };
		static void uniform2ui(GLint location, GLuint v0, GLuint v1) { glUniform2ui(location, v0, v1); checkError(); };
		static void uniform3ui(GLint location, GLuint v0, GLuint v1, GLuint v2) { glUniform3ui(location, v0, v1, v2); checkError(); };
		static void uniform4ui(GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3) { glUniform4ui(location, v0, v1, v2, v3); checkError(); };
		static void uniform1fv(GLint location, GLsizei count, const GLfloat *value) { glUniform1fv(location, count, value); checkError(); };
		static void uniform2fv(GLint location, GLsizei count, const GLfloat *value) { glUniform2fv(location, count, value); checkError(); };
		static void uniform3fv(GLint location, GLsizei count, const GLfloat *value) { glUniform3fv(location, count, value); checkError(); };
		static void uniform4fv(GLint location, GLsizei count, const GLfloat *value) { glUniform4fv(location, count, value); checkError(); };
		static void uniform1iv(GLint location, GLsizei count, const GLint *value) { glUniform1iv(location, count, value); checkError(); };
		static void uniform2iv(GLint location, GLsizei count, const GLint *value) { glUniform2iv(location, count, value); checkError(); };
		static void uniform3iv(GLint location, GLsizei count, const GLint *value) { glUniform3iv(location, count, value); checkError(); };
		static void uniform4iv(GLint location, GLsizei count, const GLint *value) { glUniform4iv(location, count, value); checkError(); };
		static void uniform1uiv(GLint location, GLsizei count, const GLuint *value) { glUniform1uiv(location, count, value); checkError(); };
		static void uniform2uiv(GLint location, GLsizei count, const GLuint *value) { glUniform2uiv(location, count, value); checkError(); };
		static void uniform3uiv(GLint location, GLsizei count, const GLuint *value) { glUniform3uiv(location, count, value); checkError(); };
		static void uniform4uiv(GLint location, GLsizei count, const GLuint *value) { glUniform4uiv(location, count, value); checkError(); };
		static void uniformMatrix2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { glUniformMatrix2fv(location, count, transpose, value); checkError(); };
		static void uniformMatrix3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { glUniformMatrix3fv(location, count, transpose, value); checkError(); };
		static void uniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { glUniformMatrix4fv(location, count, transpose, value); checkError(); };
		static void uniformMatrix2x3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { glUniformMatrix2x3fv(location, count, transpose, value); checkError(); };
		static void uniformMatrix3x2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { glUniformMatrix3x2fv(location, count, transpose, value); checkError(); };
		static void uniformMatrix2x4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { glUniformMatrix2x4fv(location, count, transpose, value); checkError(); };
		static void uniformMatrix4x2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { glUniformMatrix4x2fv(location, count, transpose, value); checkError(); };
		static void uniformMatrix3x4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { glUniformMatrix3x4fv(location, count, transpose, value); checkError(); };
		static void uniformMatrix4x3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { glUniformMatrix4x3fv(location, count, transpose, value); checkError(); };
		static void useProgram(GLuint program) { glUseProgram(program); checkError(); };
		static void vertexAttribDivisor(GLuint index, GLuint divisor) { glVertexAttribDivisor(index, divisor); checkError(); };
		static void vertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid * pointer) { glVertexAttribPointer(index, size, type, normalized, stride, pointer); checkError(); };
		static void viewport(GLint x, GLint y, GLsizei width, GLsizei height) { glViewport(x, y, width, height); checkError(); };

	};
}

