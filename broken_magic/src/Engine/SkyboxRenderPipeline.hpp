#pragma once

#include "IRenderPipeline.hpp"
#include "ResourceManager.hpp"
#include "CubemapTexture.hpp"

namespace MagicEngine
{
	class SkyboxRenderPipeline : public IRenderPipeline
	{
	public:
		SkyboxRenderPipeline(Camera* cam);
		virtual ~SkyboxRenderPipeline();

		virtual void render() override;
		virtual void draw(GameObject* gameObject) override;

	protected:
		std::shared_ptr<GLSLProgram> _cubemappingShader;
		std::shared_ptr<ModelData> _modelData;
		std::shared_ptr<CubemapTexture> _cubeTexture;

		struct
		{
			// vertex shader
			GLint projection = 0;
			GLint view = 0;

			// fragment shader
			GLint cubemap = 0;
		} _uniformLocations;
	};
}
