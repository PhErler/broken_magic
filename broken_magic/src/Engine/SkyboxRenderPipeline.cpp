#include "SkyboxRenderPipeline.hpp"

#include "MagicEngineMain.hpp"
#include "OGL.hpp"

#include "glm\glm.hpp"
#include "glm\gtc\type_ptr.hpp"

using namespace std;
using namespace glm;
using namespace MagicEngine;

SkyboxRenderPipeline::SkyboxRenderPipeline(Camera* cam)
	: IRenderPipeline(cam)
{
	_cubemappingShader = ResourceManager::getShader("resources\\shaders\\Skybox.vs",
		"resources\\shaders\\Skybox.fs");

	GameObject* skybox = MagicEngineMain::getGameObjectWithName("outside_skybox");
	if (skybox)
	{
		skybox->setInScenePart(0); // always invisible
		_modelData = skybox->getModelData();
	}

	_cubeTexture = make_shared<CubemapTexture>("resources\\texures\\",
		"posx.bmp", "negx.bmp", "posy.bmp",
		"negy.bmp", "posz.bmp", "negz.bmp");

	_uniformLocations.projection = _cubemappingShader->getUniformLocation("projection");
	_uniformLocations.view = _cubemappingShader->getUniformLocation("view");
	_uniformLocations.cubemap = _cubemappingShader->getUniformLocation("cubemap");
}

SkyboxRenderPipeline::~SkyboxRenderPipeline()
{
	_cubemappingShader.reset();
}

void SkyboxRenderPipeline::render()
{
	if (!_cam)
	{
		return;
	}

	_cubemappingShader->activate();

	_cam->getRenderBuffer()->activate();

	OGL::enable(GL_DEPTH_TEST);

	OGL::disable(GL_CULL_FACE);

	OGL::blendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	OGL::depthMask(GL_FALSE);

	// Draw skybox as last
	OGL::depthFunc(GL_LEQUAL);  // Change depth function so depth test passes when values are equal to depth buffer's content
	
	// Remove any translation component of the view matrix
	mat4 view = mat4(mat3(_cam->getViewMatrix()));
	mat4 proj = _cam->getProjectionMatrix();
	_cubemappingShader->setUniform(_uniformLocations.view, view);
	_cubemappingShader->setUniform(_uniformLocations.projection, proj);
	
	_cubeTexture->activate(0);

	_modelData->_vertexBuffer.activate();
	_modelData->_vertexBuffer.draw();
	_modelData->_vertexBuffer.deactivate();

	_cubeTexture->deactivate(0);

	OGL::depthFunc(GL_LESS); // Set depth function back to default

	OGL::depthMask(GL_TRUE);

	OGL::enable(GL_CULL_FACE);

	OGL::disable(GL_DEPTH_TEST);

	_cam->getRenderBuffer()->deactivate();

	_cubemappingShader->deactivate();
}

void SkyboxRenderPipeline::draw(GameObject *gameObject)
{
}
