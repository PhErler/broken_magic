// Based on the smoke particles cuda example by NVidia

#pragma once

#include "Resource.hpp"

#include "OGL.hpp"

#include "glm\glm.hpp"
#include "glm\gtc\type_ptr.hpp"

#include <GL/glew.h>

#include <stdio.h>
#include <string>
#include <map>

namespace MagicEngine
{
	class GLSLProgram : public IResource
	{
	public:
		// construct program from strings
		GLSLProgram(const std::string &vertexSourcePath, const std::string &fragmentSourcePath, const std::string &geometrySourcePath = "",
			GLenum gsInput = GL_POINTS, GLenum gsOutput = GL_TRIANGLE_STRIP);
		~GLSLProgram();

		void activate();
		void deactivate();

		GLint getUniformLocation(const std::string &uniformName);

		static void setUniform(GLint location, GLint v0) { OGL::uniform1i(location, v0); };
		static void setUniform(GLint location, GLint v0, GLint v1) { OGL::uniform2i(location, v0, v1); };
		static void setUniform(GLint location, GLint v0, GLint v1, GLint v2) { OGL::uniform3i(location, v0, v1, v2); };
		static void setUniform(GLint location, GLint v0, GLint v1, GLint v2, GLint v3) { OGL::uniform4i(location, v0, v1, v2, v3); };
		static void setUniform(GLint location, GLfloat v0) { OGL::uniform1f(location, v0); };
		static void setUniform(GLint location, GLfloat v0, GLfloat v1) { OGL::uniform2f(location, v0, v1); };
		static void setUniform(GLint location, GLfloat v0, GLfloat v1, GLfloat v2) { OGL::uniform3f(location, v0, v1, v2); };
		static void setUniform(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) { OGL::uniform4f(location, v0, v1, v2, v3); };
		static void setUniform(GLint location, GLuint v0) { OGL::uniform1ui(location, v0); };
		static void setUniform(GLint location, GLuint v0, GLuint v1) { OGL::uniform2ui(location, v0, v1); };
		static void setUniform(GLint location, GLuint v0, GLuint v1, GLuint v2) { OGL::uniform3ui(location, v0, v1, v2); };
		static void setUniform(GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3) { OGL::uniform4ui(location, v0, v1, v2, v3); };
		static void setUniform(GLint location, const glm::vec2 &v0) { OGL::uniform2f(location, v0.x, v0.y); };
		static void setUniform(GLint location, const glm::vec3 &v0) { OGL::uniform3f(location, v0.x, v0.y, v0.z); };
		static void setUniform(GLint location, const glm::vec4 &v0) { OGL::uniform4f(location, v0.x, v0.y, v0.z, v0.w); };
		static void setUniform(GLint location, const glm::mat3 &v0, GLsizei count = 1, GLboolean transpose = false) { OGL::uniformMatrix3fv(location, count, transpose, glm::value_ptr(v0)); };
		static void setUniform(GLint location, const glm::mat4 &v0, GLsizei count = 1, GLboolean transpose = false) { OGL::uniformMatrix4fv(location, count, transpose, glm::value_ptr(v0)); };

		void bindTexture(GLint location, GLint unit);

	private:
		static std::string readFile(const std::string& filePath);

		GLuint checkCompileStatus(GLuint shader, GLint *status);
		GLuint compileProgram(const std::string &vertexSourcePath, const std::string &fragmentSourcePath, const std::string &geometrySourcePath = "",
			GLenum gsInput = GL_POINTS, GLenum gsOutput = GL_TRIANGLE_STRIP);

		std::map<std::string, int> s_uniformLocationMap;
	};
}
