/*
Taken from the Volumetric Clouds Tutorial on
http://www.futuredatalab.com/volumetricloud/

Repository:
https://github.com/slsdo/VolumetricClouds
*/

#ifndef VOXELGRID_H
#define VOXELGRID_H

#include <string>
#include <vector>
#include <fstream>
#include <sstream>

#include "glm\glm.hpp"

struct Voxel {
	glm::vec3 color;
	float density;
	float transmissivity;
};

class VoxelGrid {
public:
	// Constructors and destructor
	VoxelGrid();
	VoxelGrid(float size, int x, int y, int z, float d);
	VoxelGrid(std::string filename);
	~VoxelGrid();
	
	void setVoxelColor(int x, int y, int z, glm::vec3 rgb); // Set color of voxel given grid coordinate
	void setVoxelColor(glm::vec3 xyz, glm::vec3 rgb); // Set color of voxel given x, y, z coordinate
	void setVoxelDensity(int x, int y, int z, float q); // Set density of voxel given grid coordinate
	void setVoxelDensity(glm::vec3 xyz, float q); // Set density of voxel given x, y, z coordinate
	void setVoxelTransmissivity(int x, int y, int z, float q); // Set transmittance of voxel given grid coordinate
	void setVoxelTransmissivity(glm::vec3 xyz, float q); // Set transmittance of voxel given x, y, z coordinate
		
	glm::vec3 getVoxelColor(int x, int y, int z); // Get color of voxel given grid coordinate
	glm::vec3 getVoxelColor(glm::vec3 xyz); // Get color of voxel given x, y, z coordinate
	float getVoxelDensity(int x, int y, int z); // Get density of voxel given grid coordinate
	float getVoxelDensity(glm::vec3 xyz); // Get density of voxel given x, y, z coordinate
	float getVoxelTransmissivity(int x, int y, int z); // Get transmittance of voxel given grid coordinate
	float getVoxelTransmissivity(glm::vec3 xyz); // Get transmittance of voxel given x, y, z coordinate
	
	glm::vec3 interpVoxelColor(float x, float y, float z); // Interpolate color of voxel given grid coordinate
	glm::vec3 interpVoxelColor(glm::vec3 xyz); // Interpolate color of voxel given x, y, z coordinate
	float interpVoxelDensity(float x, float y, float z); // Interpolate density using tri-linear interpolation
	float interpVoxelDensity(glm::vec3 xyz); // Interpolate density using tri-linear interpolation
	float interpVoxelTransmissivity(float x, float y, float z); // Interpolate transmittance using tri-linear interpolation
	float interpVoxelTransmissivity(glm::vec3 xyz); // Interpolate transmittance using tri-linear interpolation
	
	int getVoxelIndex(int x, int y, int z); // Get array index of voxel given grid index
	std::vector<Voxel*> getVoxelGrid(); // Get voxel grid
	float getVoxelSize(); // Get voxel size
	float getDefaultDensity();	// Get default density
	int getMaxX(); int getMaxY(); int getMaxZ(); // Get X/Y/Z count
	glm::mat4 getTInv(); glm::mat4 getTStarInv(); // Get transformation matrices for intersection test

	bool isInsideGrid(glm::vec3 xyz); // Check if point is inside the voxel grid
	glm::vec3 world2voxel(glm::vec3 world); // World coordinate to voxel coordinate
	glm::vec3 voxel2world(glm::vec3 voxel); // Voxel coordinate to world coordinate

private:
	float voxelSize;
	int xCount, yCount, zCount;
	float defaultDensity;
	std::vector<Voxel*> voxelGrid;

	glm::mat4 t_inv;
	glm::mat4 tstar_inv;

	friend class VolumeRender;
};

#endif