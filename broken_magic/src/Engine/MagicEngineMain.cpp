#include "MagicEngineMain.hpp"
#include "ResourceManager.hpp"

#include "OGL.hpp"

#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"

#include <string>
#include <iostream>

using namespace std;
using namespace glm;
using namespace MagicEngine;

// init static members
bool MagicEngineMain::s_initialized = false;
int MagicEngineMain::s_errorCode = EXIT_SUCCESS;
CommandLineParams MagicEngineMain::s_commandLineParams = CommandLineParams(1024, 768, 60, false, 1);
GLFWwindow* MagicEngineMain::s_window = nullptr;
GameObject* MagicEngineMain::s_sceneGraphRoot = nullptr;
IGameManager* MagicEngineMain::s_gameManager = nullptr;
Camera* MagicEngineMain::s_camera = nullptr;

ShadowMappingPipeline* MagicEngineMain::s_shadowMappingPipeline = nullptr;
DeferredRenderingPipeline* MagicEngineMain::s_deferredRenderingPipeline = nullptr;
PostProcessingRenderingPipeline* MagicEngineMain::s_depthOfFieldRenderingPipeline = nullptr;
VolumeRenderingPipeline* MagicEngineMain::s_volumeRenderingPipeline = nullptr;
SkyboxRenderPipeline* MagicEngineMain::s_skyboxRenderPipeline = nullptr;

VertexArray* MagicEngineMain::s_vertexArray = nullptr;

bool MagicEngineMain::s_wireFrameMode = false;
double MagicEngineMain::s_fps = 0;

bool MagicEngineMain::s_cullingEnabled = true;
int MagicEngineMain::s_verticesRenderedThisFrame = 0;
int MagicEngineMain::s_deferredDebugRenderingState = 0;
int MagicEngineMain::s_depthOfFieldRenderingState = 0;
bool MagicEngineMain::s_depthOfFieldRenderingStateOutput = false;
bool MagicEngineMain::s_normalMapRenderingState = false;

std::vector<Light*> MagicEngineMain::s_lights;

GameObject::SceneParts MagicEngineMain::s_currentSceneState = GameObject::Outdoor;

// lighting
std::vector<Light> s_lights;
DirectionalLight MagicEngineMain::s_directionalLight = DirectionalLight(
	BaseLight(vec3(0.8f, 0.6f, 0.6f), glm::vec3(0.1f, 0.1f, 0.1f), glm::vec3(0.3f, 0.3f, 0.3f)), 
	vec3(0, 1, 1), ortho<float>(-100, 100, -650, 200, -400, 15));

Rect MagicEngineMain::s_currViewport(0, 0, 0, 0);

// init music
SoundEngine* MagicEngineMain::s_music = new SoundEngine();

int MagicEngineMain::init(int argc, char* argv[])
{
	cout << "MagicEngine init start" << endl;

	if (s_initialized)
	{
		return EXIT_FAILURE;
	}

	s_initialized = true;

	s_commandLineParams = parseCommandLineParams(argc, argv);

	// Initialise GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		s_errorCode = EXIT_FAILURE;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_REFRESH_RATE, s_commandLineParams._refreshRate);

	GLFWmonitor* monitor = NULL;
	if (s_commandLineParams._fullScreen)
	{
		monitor = glfwGetPrimaryMonitor();
	}

	// Open a window and create its OpenGL context
	s_window = glfwCreateWindow(s_commandLineParams._xRes, s_commandLineParams._yRes, "Broken Magic", monitor, NULL);
	if (s_window == nullptr){
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they might not be 3.3 compatible.\n");
		glfwTerminate();
		s_errorCode = EXIT_FAILURE;
	}
	glfwMakeContextCurrent(s_window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		s_errorCode = EXIT_FAILURE;
	}

	OGL::getError(); // clear error because glfw creates one

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(s_window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetCursorPos(s_window, s_commandLineParams._xRes / 2, s_commandLineParams._yRes / 2);

	// Enable depth test
	OGL::enable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	//OGL::depthFunc(GL_LESS);// default setting
	
	// Enable blending for transparency
	OGL::enable(GL_BLEND);
	//OGL::blendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);// default setting

	// Cull back-facing triangles -> draw only front-facing triangles
	OGL::enable(GL_CULL_FACE);
	//OGL::cullFace(GL_BACK);// default setting

	if (s_gameManager)
	{
		s_gameManager->init();
	}

	s_shadowMappingPipeline = new ShadowMappingPipeline(s_camera);
	s_deferredRenderingPipeline = new DeferredRenderingPipeline(s_camera);
	s_depthOfFieldRenderingPipeline = new PostProcessingRenderingPipeline(s_camera);
	s_volumeRenderingPipeline = new VolumeRenderingPipeline(s_camera, 
		s_deferredRenderingPipeline->getGBufferTexture(DeferredRenderingPipeline::GBUFFER_TEXTURE_TYPE_POSITION));
	s_skyboxRenderPipeline = new SkyboxRenderPipeline(s_camera);

	s_vertexArray = new VertexArray();
	
	cout << "MagicEngine init end" << endl;

	return s_errorCode;
}

int MagicEngineMain::deinit()
{
	cout << "MagicEngine deinit start" << endl;

	ResourceManager::clearCache();

	s_sceneGraphRoot = nullptr;
	s_camera = nullptr;

	if (s_gameManager)
	{
		s_gameManager->deinit();
	}

	if (s_shadowMappingPipeline)
	{
		delete s_shadowMappingPipeline;
		s_shadowMappingPipeline = nullptr;
	}

	if (s_deferredRenderingPipeline)
	{
		delete s_deferredRenderingPipeline;
		s_deferredRenderingPipeline = nullptr;
	}

	if (s_depthOfFieldRenderingPipeline)
	{
		delete s_depthOfFieldRenderingPipeline;
		s_depthOfFieldRenderingPipeline = nullptr;
	}

	if (s_volumeRenderingPipeline)
	{
		delete s_volumeRenderingPipeline;
		s_volumeRenderingPipeline = nullptr;
	}

	if (s_skyboxRenderPipeline)
	{
		delete s_skyboxRenderPipeline;
		s_skyboxRenderPipeline = nullptr;
	}

	if (s_vertexArray)
	{
		delete s_vertexArray;
		s_vertexArray = nullptr;
	}

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	// Terminate music
	delete s_music;

	cout << "MagicEngine deinit end" << endl;
	return s_errorCode;
}

int MagicEngineMain::run()
{
	cout << "MagicEngine run start" << endl;

	// Start Music
	s_music->play();

	// main loop
	do{
		update();

		render();

	} // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(s_window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
	glfwWindowShouldClose(s_window) == 0);

	cout << "MagicEngine run end" << endl;

	return s_errorCode;
}

void MagicEngine::MagicEngineMain::update()
{
	double deltaT = 0.0f;
	static double lastTime = 0.0f;

	double time = glfwGetTime();
	deltaT = time - lastTime;
	lastTime = time;

	// frames per second
	s_fps = 1 / deltaT;

	// skip update if dt is too large to avoid instable physics (can be caused by loading)
	if (deltaT < 0.25f)
	{
		// update gamemanager
		if (s_gameManager)
		{
			s_gameManager->update(deltaT);
		}

		// update all entities, hierarchically
		if (s_sceneGraphRoot)
		{
			s_sceneGraphRoot->update(deltaT);
		}

		// update camera
		if (s_camera)
		{
			s_camera->update(deltaT);
		}
	}
}

void MagicEngine::MagicEngineMain::render()
{
	s_verticesRenderedThisFrame = 0;

	if (s_camera && s_sceneGraphRoot)
	{
		s_camera->getRenderBuffer()->activate();
		s_camera->getRenderBuffer()->clear(true, true, true, 0.1f, 0.5f, 0.1f);
		s_camera->getRenderBuffer()->deactivate();

		s_deferredRenderingPipeline->render();
		s_shadowMappingPipeline->render();
		s_volumeRenderingPipeline->render();
		s_skyboxRenderPipeline->render();
		s_depthOfFieldRenderingPipeline->render();
		
		// GUI
		if (s_gameManager)
		{
			s_gameManager->render();
		}

		// get it all on the screen
		s_camera->renderToScreen();
	}

	// Swap buffers
	glfwSwapBuffers(s_window);
	glfwPollEvents();
}

GLFWwindow* MagicEngineMain::getWindow()
{
	return s_window;
}

CommandLineParams MagicEngineMain::parseCommandLineParams(int argc, char* argv[])
{
	int xRes = 1024;
	int yRes = 768;
	int refreshRate = 60;
	int fullScreen = 0;
	float quality = 1.0f;

	// Note: first param is our program's name
	try
	{
		if (argc >= 3)
		{
			xRes = std::stoi(string(argv[1]));
			yRes = std::stoi(string(argv[2]));
		}
		if (argc >= 4)
		{
			refreshRate = std::stoi(string(argv[3]));
		}
		if (argc >= 5)
		{
			fullScreen = std::stoi(string(argv[4]));
		}
		if (argc >= 6)
		{
			quality = abs(std::stof(string(argv[5])));
		}
	}
	catch (const std::invalid_argument& ia) {
		std::cout << "Invalid argument: " << ia.what() << '\n';
	}
	catch (const std::out_of_range& oor) {
		std::cerr << "Out of Range error: " << oor.what() << '\n';
	}

	return CommandLineParams(xRes, yRes, refreshRate, fullScreen == 0 ? false : true, quality);
}

const CommandLineParams& MagicEngineMain::getCommandLineParams()
{
	return s_commandLineParams;
}

void MagicEngine::MagicEngineMain::setCamera(Camera * camera) {
	s_camera = camera; 
}

std::vector<Light*> MagicEngineMain::getLights()
{ 
	return s_lights; 
}

void MagicEngineMain::addLight(Light* light)
{ 
	s_lights.push_back(light); 
}

void MagicEngine::MagicEngineMain::removeLight(Light * light)
{
	for (auto it(s_lights.begin()); it != s_lights.end(); ++it)
	{
		if (*it == light)
		{
			s_lights.erase(it);
			return;
		}
	}
}

void MagicEngineMain::changeWireFrameMode()
{
	s_wireFrameMode = !s_wireFrameMode;
	cout << "wireframe mode " << (MagicEngineMain::wireFrameMode() ? "on" : "off") << endl;
}

void MagicEngineMain::addRenderedVertices(int count)
{ 
	s_verticesRenderedThisFrame += count; 
}

int MagicEngineMain::getRenderedVertices()
{
	return s_verticesRenderedThisFrame;
}

void MagicEngineMain::toggleCulling()
{
	s_cullingEnabled = !s_cullingEnabled;
	cout << "culling " << (MagicEngineMain::s_cullingEnabled ? "on" : "off") << endl;
}

double MagicEngineMain::getFPS()
{
	return s_fps;
}

void MagicEngineMain::setViewPort(Rect viewPort)
{
	if (viewPort._left != s_currViewport._left ||
		viewPort._top != s_currViewport._top ||
		viewPort._width != s_currViewport._width ||
		viewPort._height != s_currViewport._height)
	{
		OGL::viewport(viewPort._left, viewPort._top, viewPort._width, viewPort._height);
		s_currViewport = viewPort;
	}
}

GameObject * MagicEngine::MagicEngineMain::getGameObjectWithName(std::string name)
{
	GameObject* foundObject = nullptr;

	if (s_sceneGraphRoot)
	{
		if (s_sceneGraphRoot->getGeometryName() == name)
		{
			foundObject = s_sceneGraphRoot;
		}
		else
		{
			foundObject = s_sceneGraphRoot->getChildWithName(name);
		}
	}

	return foundObject;
}
