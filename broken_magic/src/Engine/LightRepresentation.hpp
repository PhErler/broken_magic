#pragma once

#include "GameObject.hpp"
#include "Light.hpp"

namespace MagicEngine
{
	class LightRepresentation : public GameObject
	{
	public:
		LightRepresentation(MagicEngine::Light* belongsToThis);
		virtual ~LightRepresentation();

		virtual void update(double dt) override;

		virtual void render(IRenderPipeline* renderPipeline);

		float _ambientFactor;
		float _diffuseFactor;
		float _specularFactor;

	protected:
		Light* _belongsToThis;

		void setUniforms();

		struct
		{
			GLint mvp = 0;
			GLint v = 0;
			GLint m = 0;
			GLint mv = 0;
			GLint posLightVS = 0;
			GLint colDiffuseLight = 0;
			GLint colAmbientLight = 0;
			GLint colSpecularLight = 0;
			GLint ambientFactor = 0;
			GLint diffuseFactor = 0;
			GLint specularFactor = 0;
		} _uniformLocations;
	};
}

