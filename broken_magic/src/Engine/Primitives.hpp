#pragma once

#include "Resource.hpp"

#include <vector>
#include <memory>
#include <string>

#include <GL/glew.h>

#include <glm/glm.hpp>

namespace MagicEngine
{
	class IRenderPipeline;

	class Primitives
	{
	public:
		static std::shared_ptr<VertexBuffer> getUnitCubeVertexBuffer();
		static std::shared_ptr<VertexBuffer> getQuadVertexBuffer();
	};
}

