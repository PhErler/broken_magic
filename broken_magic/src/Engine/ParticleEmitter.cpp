#include "ParticleEmitter.hpp"

#include "MagicEngineMain.hpp"
#include "ResourceManager.hpp"
#include "Camera.hpp"
#include "OGL.hpp"

#include <algorithm>

#include <glm/gtx/norm.hpp>

using namespace std;
using namespace glm;
using namespace MagicEngine;

bool ParticleEmitter::enableBlending = true;

namespace
{
	const int MAX_PARTICLES = 25;

	const GLfloat vertex_buffer[] = {
		-0.5f, -0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		-0.5f, 0.5f, 0.0f,
		0.5f, 0.5f, 0.0f,
	};
}

ParticleEmitter::ParticleEmitter()
: _particles(MAX_PARTICLES)
, _particle_position(MAX_PARTICLES * 4)
, _particle_color(MAX_PARTICLES * 4)
{
	_isTransparent = true;

	OGL::genVertexArrays(1, &VertexArray);
	OGL::bindVertexArray(VertexArray);

	// Create and compile our GLSL program from the shaders
	program = ResourceManager::getShader("resources\\shaders\\Particle.vs", "resources\\shaders\\Particle.fs");

	// Vertex shader
	CameraRight_worldspace_ID = OGL::getUniformLocation(program->getResourceID(), "CameraRight_worldspace");
	CameraUp_worldspace_ID = OGL::getUniformLocation(program->getResourceID(), "CameraUp_worldspace");
	ViewProjMatrixID = OGL::getUniformLocation(program->getResourceID(), "VP");

	// fragment shader
	TextureID = OGL::getUniformLocation(program->getResourceID(), "myTextureSampler");

	for (int i = 0; i < MAX_PARTICLES; i++){
		_particles[i].life = -1.0f;
		_particles[i].cameradistance = -1.0f;
	}

	// particle texture
	texture = make_shared<Texture>(string("resources\\texures\\dustParticle.DDS"));

	OGL::genBuffers(1, &_billboard_vertex_buffer);
	OGL::bindBuffer(GL_ARRAY_BUFFER, _billboard_vertex_buffer);
	OGL::bufferData(GL_ARRAY_BUFFER, sizeof(vertex_buffer), vertex_buffer, GL_STATIC_DRAW);


	// VBO containing positions and sizes of particles
	OGL::genBuffers(1, &_particles_position_buffer);
	OGL::bindBuffer(GL_ARRAY_BUFFER, _particles_position_buffer);
	OGL::bufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);

	// VBO containing colors of particles
	OGL::genBuffers(1, &_particles_color_buffer);
	OGL::bindBuffer(GL_ARRAY_BUFFER, _particles_color_buffer);
	OGL::bufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 4 * sizeof(GLubyte), NULL, GL_STREAM_DRAW);
}

void ParticleEmitter::update(double dt)
{
	GameObject::update(dt);

	// add new particles
	_particleToSpawn += float((rand() % 40 + 10) * dt);
	int newParticles = int(_particleToSpawn);
	for (int i = 0; i < newParticles; i++)
	{
		addParticle();
	}

	// update particles
	_particleCount = 0;
	for (int i = 0; i < MAX_PARTICLES; ++i)
	{
		Particle& p = _particles[i];

		// decrease life, transparency and size
		p.life -= float(dt);
		p.color.a -= 25 * static_cast<unsigned char>(dt);
		p.size -= 0.05f * float(dt);

		if (p.life > 0 || p.color.a > 0 || p.size > 0)
		{
			// random direction
			float x = (rand() % 10) / 10.0f;
			float y = (rand() % 2) / 10.0f;
			float z = (rand() % 10) / 10.0f;
			if (rand() % 2) x *= (-1);
			if (rand() % 2) z *= (-1);
			p.position += vec3(x, y, z) * p.velocity;

			vec3 CameraPosition;
			/*CameraInfo* camInfo = CameraManager::getCameraInfo(0);
			if (camInfo)
			{
				CameraPosition = camInfo->_camera->getPosition();
			}*/
			p.cameradistance = glm::length2(p.position - CameraPosition);

			// Fill the GPU buffer
			_particle_position[4 * _particleCount + 0] = p.position.x;
			_particle_position[4 * _particleCount + 1] = p.position.y;
			_particle_position[4 * _particleCount + 2] = p.position.z;
			_particle_position[4 * _particleCount + 3] = p.size;

			_particle_color[4 * _particleCount + 0] = unsigned char(p.color.r);
			_particle_color[4 * _particleCount + 1] = unsigned char(p.color.g);
			_particle_color[4 * _particleCount + 2] = unsigned char(p.color.b);
			_particle_color[4 * _particleCount + 3] = unsigned char(p.color.a);
		}
		else
		{
			// Particles that just died will be put at the end of the buffer in SortParticles();
			p.cameradistance = -1.0f;
		}
		++_particleCount;
	}

	//sortParticles(); // currently not needed
}

ParticleEmitter::~ParticleEmitter()
{
	_particle_position.clear();

	// Cleanup VBO and shader
	OGL::deleteBuffers(1, &_particles_position_buffer);
	OGL::deleteBuffers(1, &_billboard_vertex_buffer);
	OGL::deleteBuffers(1, &_particles_color_buffer);
	program.reset();
	texture.reset();
	OGL::deleteVertexArrays(1, &VertexArray);
}


int ParticleEmitter::getNextParticleIndex(){

	for (int i = _lastParticle; i < MAX_PARTICLES; i++)
	{
		if (_particles[i].life < 0 || _particles[i].color.a < 0 || _particles[i].size < 0){
			_lastParticle = i;
			return i;
		}
	}
	for (int i = 0; i < _lastParticle; i++)
	{
		if (_particles[i].life < 0 || _particles[i].color.a < 0 || _particles[i].size < 0){
			_lastParticle = i;
			return i;
		}
	}
	return 0;
}

void ParticleEmitter::sortParticles(){
	sort(_particles.begin(), _particles.end());
}

void ParticleEmitter::addParticle()
{
	int index = getNextParticleIndex();

	_particles[index].position = getPosition();
	_particles[index].life = float(rand() % 5 + 1);	
	_particles[index].velocity = (rand() % 2 + 1) / 10.0f;

	// random brownish color
	/*_particles[index].color.r = 225;
	_particles[index].color.g = rand() % 80 + 100;
	_particles[index].color.b = rand() % 150 + 5;
	_particles[index].color.a = 150;*/

	_particles[index].color = vec4(206, 176, 140, 125);
	_particles[index].size = (rand() % 1000) / 2000.0f + 0.1f;
	
	_particleToSpawn -= 1;
}