#pragma once

#include "IRenderPipeline.hpp"
#include "VolumeTexture.hpp"
#include "GLSLProgram.hpp"
#include "ResourceManager.hpp"
#include "IRenderPipeline.hpp"
#include "RenderTexture.hpp"
#include "FrameBuffer.hpp"

#include <string>
#include <memory>

#include <GL/glew.h>

namespace MagicEngine
{
	class VolumeRenderingPipeline : public IRenderPipeline
	{
	public:
		VolumeRenderingPipeline(Camera* cam, RenderTexture* opaqueGeometryWorldSpacePositions);
		virtual ~VolumeRenderingPipeline();

		virtual void render() override;
		virtual void draw(GameObject* gameObject) override;

	protected:
		Camera* _cam;
		unsigned int _bufferSizeX = 0;
		unsigned int _bufferSizeY = 0;

		glm::mat4 removeRotation(const glm::mat4 &m);

		std::shared_ptr<GLSLProgram> _worldSpacePositionShader;
		std::shared_ptr<GLSLProgram> _rayCastShader;

		std::shared_ptr<VertexBuffer> _unitCube;
		std::shared_ptr<VertexBuffer> _quadVertexBuffer;

		RenderTexture* _posBBFrontWS = nullptr;
		RenderTexture* _posBBBackWS = nullptr;
		RenderTexture* _opaqueGeometryWorldSpacePositions = nullptr;
		FrameBuffer* _posBBFrontWSFbo;
		FrameBuffer* _posBBBackWSFbo;

		std::shared_ptr<Texture> _noiseTexture;

		struct
		{
			GLint volTex;
			GLint noiseTex;
			GLint posBackFaceWSTex;
			GLint posFrontFaceWSTex;
			GLint posOpaqueGeometryWSTex;

			GLint stepSize;
			GLint lightRayStepSize;
			GLint opacityFactor;
			GLint worldToModel;

			GLint posCamMS;
			GLint posLightMS;
			GLint colLight;

			GLint noisePosOffset;
			GLint noiseOffsetFactor;
		} _raycastLocations;

		struct
		{
			GLint mvp;
			GLint modelToWorld;
		} _wsPosLocations;

	private:
		void renderBoundingBox(bool frontFace, glm::mat4 &modelToWorldMatrix, glm::mat4 &mvp, glm::vec3 camPos);
	};
}

