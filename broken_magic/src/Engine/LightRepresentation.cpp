#include "LightRepresentation.hpp"

#include "IRenderPipeline.hpp"
#include "MagicEngineMain.hpp"

using namespace std;
using namespace glm;
using namespace MagicEngine;

LightRepresentation::LightRepresentation(MagicEngine::Light* belongsToThis)
	: _belongsToThis(belongsToThis), _diffuseFactor(1.0), _ambientFactor(1.0), _specularFactor(1.0)
{
	_isTransparent = true;

	_modelData = ResourceManager::getModelData("resources\\models\\icosphere3.obj");
	_program = ResourceManager::getShader("resources\\shaders\\LightRepresentation.vs", "resources\\shaders\\LightRepresentation.fs");

	_program->activate();
	_uniformLocations.mvp = _program->getUniformLocation("mvp");
	_uniformLocations.v = _program->getUniformLocation("v");
	_uniformLocations.m = _program->getUniformLocation("m");
	_uniformLocations.mv = _program->getUniformLocation("mv");
	_uniformLocations.posLightVS = _program->getUniformLocation("posLightVS");
	_uniformLocations.colDiffuseLight = _program->getUniformLocation("colDiffuseLight");
	_uniformLocations.colAmbientLight = _program->getUniformLocation("colAmbientLight");
	_uniformLocations.colSpecularLight = _program->getUniformLocation("colSpecularLight");
	_uniformLocations.ambientFactor = _program->getUniformLocation("ambientFactor");
	_uniformLocations.diffuseFactor = _program->getUniformLocation("diffuseFactor");
	_uniformLocations.specularFactor = _program->getUniformLocation("specularFactor");
	_program->deactivate();
}

MagicEngine::LightRepresentation::~LightRepresentation()
{
	_belongsToThis = nullptr;
}

void MagicEngine::LightRepresentation::update(double dt)
{
	GameObject::update(dt);

	vec3 posLight;
	if (_belongsToThis)
	{
		posLight = _belongsToThis->getPosition();
	}

	_modelMatrix[3][0] = posLight.x;
	_modelMatrix[3][1] = posLight.y;
	_modelMatrix[3][2] = posLight.z;
}

void MagicEngine::LightRepresentation::setUniforms()
{
	Camera* cam = MagicEngineMain::getCamera();
	const mat4& v = cam->getViewMatrix();
	const mat4& m = getWorldspaceMatrix();
	const mat4& p = cam->getProjectionMatrix();
	mat4 mv = v * m;
	mat4 mvp = p * v * m;

	vec3 posLightVS;
	vec3 colDiffuseLight;
	vec3 colAmbientLight;
	vec3 colSpecularLight;
	if (_belongsToThis)
	{
		posLightVS = vec3(v * vec4(_belongsToThis->getPosition(), 1.0f));
		colDiffuseLight = _belongsToThis->_baseLight.DiffuseColor;
		colAmbientLight = _belongsToThis->_baseLight.AmbientColor;
		colSpecularLight = _belongsToThis->_baseLight.SpecularColor;
	}

	_program->activate();
	GLSLProgram::setUniform(_uniformLocations.mvp, mvp);
	GLSLProgram::setUniform(_uniformLocations.v, v);
	GLSLProgram::setUniform(_uniformLocations.m, m);
	GLSLProgram::setUniform(_uniformLocations.mv, mv);
	GLSLProgram::setUniform(_uniformLocations.posLightVS, posLightVS);
	GLSLProgram::setUniform(_uniformLocations.colDiffuseLight, colDiffuseLight);
	GLSLProgram::setUniform(_uniformLocations.colAmbientLight, colAmbientLight);
	GLSLProgram::setUniform(_uniformLocations.colSpecularLight, colSpecularLight);
	GLSLProgram::setUniform(_uniformLocations.ambientFactor, _ambientFactor);
	GLSLProgram::setUniform(_uniformLocations.diffuseFactor, _diffuseFactor);
	GLSLProgram::setUniform(_uniformLocations.specularFactor, _specularFactor);
	_program->deactivate();
}

void MagicEngine::LightRepresentation::render(IRenderPipeline* renderPipeline)
{
	if (!_isVisible)
	{
		return;
	}

	// render children first
	if (!_children.empty())
	{
		//cout << "render children!" << endl;
		for (GameObject* &child : _children)
		{
			child->render(renderPipeline);
		}
	}

	setUniforms();

	renderPipeline->draw(this);
}