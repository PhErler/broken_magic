#pragma once

#include <glm/glm.hpp>

#include "FrameBuffer.hpp"
#include "RenderTexture.hpp"
#include "Texture.hpp"
#include "GLSLProgram.hpp"
#include "Resource.hpp"
#include "Camera.hpp"
#include "GameObject.hpp"

namespace MagicEngine
{
	class IRenderPipeline
	{
	public:
		IRenderPipeline(Camera* cam) : _cam(cam) {}

		virtual void setCamera(Camera* cam) { _cam = cam; }
		virtual void render() = 0;
		virtual void draw(GameObject* gameObject) = 0;

	protected:
		Camera* _cam;
	};
}

