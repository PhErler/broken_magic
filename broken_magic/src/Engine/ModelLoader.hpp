#pragma once

#include <GL/glew.h>

#include <vector>

#include <glm/glm.hpp>

#include "assimp\mesh.h"

namespace MagicEngine
{
	class ModelLoader
	{
	public:
		static bool loadMesh(
			aiMesh *mesh,
			std::vector<glm::vec3> & out_vertices,
			std::vector<glm::vec2> & out_uvs,
			std::vector<glm::vec3> & out_normals,
			std::vector<glm::vec3> & out_tangents,
			float & width,
			float & height,
			float & depth,
			glm::vec3 & minBB,
			glm::vec3 & maxBB,
			int & material_index
			);

		static bool loadOBJ(
			const char * path,
			std::vector<glm::vec3> & out_vertices,
			std::vector<glm::vec2> & out_uvs,
			std::vector<glm::vec3> & out_normals
			);
	};
}