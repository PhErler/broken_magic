#pragma once

#include "../Engine/Camera.hpp"

#include "PidController.hpp"

#include <vector>
#include <string>

#include <glm\glm.hpp>

#include "glm\gtx\quaternion.hpp"

#include "assimp\postprocess.h"
#include "assimp\mesh.h"
#include "assimp\Importer.hpp"

namespace BrokenMagic
{
	struct KeyPoint {
		glm::vec3 _position;
		glm::vec4 _quat;
		glm::vec2 _depthOfField; // focalDistance, focalRange
		float _fov = 0;
		float _duration = 1;

		KeyPoint(glm::vec3 pos, glm::vec4 quat, float duration, float fov, glm::vec2 depthOfField)
			: _position(pos), _quat(quat), _duration(duration), _fov(fov), _depthOfField(depthOfField)
		{	}

		KeyPoint()
			: _position(0, 0, 0), _quat(0, 0, 0, 0), _duration(1), _fov(0), _depthOfField(0, 0)
		{	}

	};

	class PresentationCamera : public MagicEngine::Camera
	{
	public:
		PresentationCamera();
		virtual ~PresentationCamera();

		virtual void update(double dt);

		//void saveKeyPoint();
		//void saveKeyPointsToFile(std::string fileName);
		
		bool isRunning() { return _running; }
		void loadFromFile(std::string fileName);
		void loadFromCollada();
		void startPresentation();
		void stopPresentation();
		void restart();
		void advance(float dt);

		float getProgress() { return _overallProgress; }

		// TODO delete - just for Debugging reasons
		static float x_factor;
		static float y_factor;
		static float z_factor;

	protected:
		int getNextKeyPointIndex();
		int getPreviousKeyPointsIndex();
		float mixValuesForCurrentProgress(float valA, float valB);
		float mixValues(float valA, float valB, float mixFactor);

	private:
		std::vector<KeyPoint> _keyPoints;

		int _keyPointSize = 0;

		bool _running;
		bool _finished = false;
		float _keyPointProgress;
		unsigned int _currentKeyPointIndex;
		float _timeSinceLastKeyPoint;
		float _overallProgress = 0;

		glm::vec3 _lastFramePosition;
		static const unsigned int s_velocityBufferSize = 5;
		float _lastVelocities[s_velocityBufferSize];
		int _lastVelocityIndex = 0;

		PidController _fovController;

		PidController _xPosController;
		PidController _yPosController;
		PidController _zPosController;
		PidController _xViewDirController;
		PidController _yViewDirController;
		PidController _zViewDirController;
		PidController _xViewRightController;
		PidController _yViewRightController;
		PidController _zViewRightController;

	};
}

