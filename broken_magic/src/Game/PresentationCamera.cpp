#include "PresentationCamera.hpp"

#include "../Engine/MagicEngineMain.hpp"
#include "../Engine/ResourceManager.hpp"

#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\quaternion.hpp"
#include "glm\gtx\constants.hpp"

#include <iostream>
#include <fstream>
#include <sstream>


using namespace std;
using namespace glm;
using namespace MagicEngine;
using namespace BrokenMagic;

namespace
{
	float PID_CONTROLLER_PROPORTIONAL_FACTOR = 0.08f;
	float PID_CONTROLLER_INTEGRAL_FACTOR = 0.5f;
	float PID_CONTROLLER_DIFFERENTIAL_FACTOR = 0.001f;

	float FOV_PID_CONTROLLER_PROPORTIONAL_FACTOR = 0.2f;
	float FOV_PID_CONTROLLER_INTEGRAL_FACTOR = 0.0f;
	float FOV_PID_CONTROLLER_DIFFERENTIAL_FACTOR = 0.000f;

	const float MinSpeed = 10;
	const float MaxSpeed = 25;
	const float MinFov = 60;
	const float MaxFov = 90;
	const float FovAcceleration = (MaxFov - MinFov) * 0.5f; // x sec from min to max fov
}

float PresentationCamera::x_factor = 0.0f;
float PresentationCamera::y_factor = 0.0f;
float PresentationCamera::z_factor = 0.0f;

PresentationCamera::PresentationCamera()
	: _keyPoints(std::vector<KeyPoint>())
	, _running(false)
	, _keyPointProgress(0)
	, _currentKeyPointIndex(0)
	, _timeSinceLastKeyPoint(0)
	, _fovController(  MinFov, FOV_PID_CONTROLLER_PROPORTIONAL_FACTOR, FOV_PID_CONTROLLER_INTEGRAL_FACTOR, FOV_PID_CONTROLLER_DIFFERENTIAL_FACTOR)
	, _xPosController(		0, PID_CONTROLLER_PROPORTIONAL_FACTOR, PID_CONTROLLER_INTEGRAL_FACTOR, PID_CONTROLLER_DIFFERENTIAL_FACTOR)
	, _yPosController(		0, PID_CONTROLLER_PROPORTIONAL_FACTOR, PID_CONTROLLER_INTEGRAL_FACTOR, PID_CONTROLLER_DIFFERENTIAL_FACTOR)
	, _zPosController(		0, PID_CONTROLLER_PROPORTIONAL_FACTOR, PID_CONTROLLER_INTEGRAL_FACTOR, PID_CONTROLLER_DIFFERENTIAL_FACTOR)
	, _xViewDirController(	0, PID_CONTROLLER_PROPORTIONAL_FACTOR, PID_CONTROLLER_INTEGRAL_FACTOR, PID_CONTROLLER_DIFFERENTIAL_FACTOR)
	, _yViewDirController(	0, PID_CONTROLLER_PROPORTIONAL_FACTOR, PID_CONTROLLER_INTEGRAL_FACTOR, PID_CONTROLLER_DIFFERENTIAL_FACTOR)
	, _zViewDirController(	0, PID_CONTROLLER_PROPORTIONAL_FACTOR, PID_CONTROLLER_INTEGRAL_FACTOR, PID_CONTROLLER_DIFFERENTIAL_FACTOR)
	, _xViewRightController(0, PID_CONTROLLER_PROPORTIONAL_FACTOR, PID_CONTROLLER_INTEGRAL_FACTOR, PID_CONTROLLER_DIFFERENTIAL_FACTOR)
	, _yViewRightController(0, PID_CONTROLLER_PROPORTIONAL_FACTOR, PID_CONTROLLER_INTEGRAL_FACTOR, PID_CONTROLLER_DIFFERENTIAL_FACTOR)
	, _zViewRightController(0, PID_CONTROLLER_PROPORTIONAL_FACTOR, PID_CONTROLLER_INTEGRAL_FACTOR, PID_CONTROLLER_DIFFERENTIAL_FACTOR)
{
}

PresentationCamera::~PresentationCamera()
{
}

/*
void PresentationCamera::saveKeyPoint()
{
	_keyPoints.push_back(KeyPoint(_position, _viewDir, _viewRight, _timeSinceLastKeyPoint));
	_timeSinceLastKeyPoint = 0;
	cout << "Save key point " << _keyPoints.size() - 1 << " with _timeSinceLastKeyPoint = " << _timeSinceLastKeyPoint <<
		", _viewDir.x = " << _viewDir.x << ", _viewDir.y = " << _viewDir.y << ", _viewDir.z = " << _viewDir.z <<
		", _viewRight.x = " << _viewRight.x << ", _viewRight.y = " << _viewRight.y << ", _viewRight.z = " << _viewRight.z <<
		", _position.x = " << _position.x << ", _position.y = " << _position.y << ", _position.z = " << _position.z << endl;
}

void PresentationCamera::saveKeyPointsToFile(std::string fileName)
{
	ofstream keyPointFile;
	keyPointFile.open(fileName, ios::out | ios::trunc ); //| ios::binary
	if (!keyPointFile.good())
	{
		cout << "Exception opening camera path file " << fileName << endl;
		return;
	}

	for each (const KeyPoint& keyPoint in _keyPoints)
	{
		keyPointFile << keyPoint._duration << " ";
		keyPointFile << keyPoint._viewRight.x << " ";
		keyPointFile << keyPoint._viewRight.y << " ";
		keyPointFile << keyPoint._viewRight.z << " ";
		keyPointFile << keyPoint._viewDir.x << " ";
		keyPointFile << keyPoint._viewDir.y << " ";
		keyPointFile << keyPoint._viewDir.z << " ";
		keyPointFile << keyPoint._position.x << " ";
		keyPointFile << keyPoint._position.y << " ";
		keyPointFile << keyPoint._position.z << endl;
	}

	keyPointFile.close();

	cout << "Saved " << _keyPoints.size() << " key points to the file " << fileName << endl;
}
*/
void PresentationCamera::loadFromFile(std::string fileName)
{

	// Load Camera Position and Rotation Vectors
	loadFromCollada();

	/*
	ifstream keyPointFile;
	keyPointFile.open(fileName, ios::in ); //| ios::binary
	if (!keyPointFile.good())
	{
		cout << "Exception opening camera path file " << fileName << endl;
		return;
	}

	_keyPoints.clear();
	KeyPoint newKeyPoint;

	string temp;
	float f = 0.0f;

	while (getline(keyPointFile, temp)) {
		istringstream iss(temp);

		int valueCounter = 0;

		while (getline(iss, temp, ' ')) {
			istringstream ist(temp);
			ist >> f;

			switch (valueCounter)
			{
			case 0:
				newKeyPoint._duration = f;
				break;
			default:
				break;
			}

			++valueCounter;
		}

		_keyPoints.push_back(newKeyPoint);
	}

	keyPointFile.close();
	*/

	// Reset Pidregler
	restart();

}

void PresentationCamera::startPresentation() 
{
	_running = true;
	update(0.0000001f); // move cam to start
	cout << "Start presentation camera" << endl;
}

void PresentationCamera::stopPresentation() 
{ 
	_running = false; 
	//_keyPoints.clear();
	cout << "Stop presentation camera. Remove old key points." << endl;
}

void PresentationCamera::restart() 
{ 
	_keyPointProgress = 0; 
	_currentKeyPointIndex = 0; 

	if (!_keyPoints.empty())
	{
		_xPosController.reset(_position.x);
		_yPosController.reset(_position.y);
		_zPosController.reset(_position.z);
		_xViewDirController.reset(0);
		_yViewDirController.reset(0);
		_zViewDirController.reset(0);
	}
}

void PresentationCamera::update(double dt)
{
	float dtf = static_cast<float>(dt);

	vec3 pos = getPosition();

	// set focus for depth of field
	float roofHeight = 54;
	vec3 posWithoutY(pos);
	posWithoutY.y = 0;
	float distToYAxis = length(posWithoutY);
	float MaxDistForInside  = 6.1f;

	vec3 snowGlobeCenter = vec3(0, 34, 0);
	float distToGlobeCenter = length(snowGlobeCenter - pos);

	float MaxDistForOutside   = 53.0f;
	float MaxDistForInBetween = 49.0f;
	if (distToYAxis < MaxDistForInside && pos.y < roofHeight) // inside
	{
		MagicEngineMain::setCurrentSceneState(GameObject::Indoor);

		BaseLight baseLight(vec3(0.03f, 0.03f, 0.08f), vec3(0.2f, 0.2f, 0.2f), vec3(0.0f, 0.0f, 0.0f));
		if (_overallProgress > 634)
		{
			baseLight = BaseLight(vec3(0.05f, 0.03f, 0.03f), vec3(0.2f, 0.1f, 0.1f), vec3(0.0f, 0.0f, 0.0f));
		}
		vec3 lightDir(normalize(vec3(0, -0.9f, 0.1f)));
		// make ProjectionMatrix size fit the track size per texture view in gDEBugger
		mat4 lightProjMatrix(glm::ortho<float>(-50, 50, 0, 100, -50, 50));
		MagicEngineMain::setDirectionalLight(DirectionalLight(baseLight, lightDir, lightProjMatrix));
	}
	else if (distToGlobeCenter < MaxDistForInBetween) 
	{
		MagicEngineMain::setCurrentSceneState(GameObject::Outdoor);

		BaseLight baseLight(vec3(0.04f, 0.03f, 0.03f), vec3(0.3f, 0.2f, 0.2f), vec3(0.0f, 0.0f, 0.0f));
		if (_overallProgress > 634)
		{
			baseLight = BaseLight(vec3(0.05f, 0.02f, 0.02f), vec3(0.3f, 0.1f, 0.1f), vec3(0.0f, 0.0f, 0.0f));
		}
		vec3 lightDir(normalize(vec3(0, -0.9f, 0.1f)));
		// make ProjectionMatrix size fit the track size per texture view in gDEBugger
		mat4 lightProjMatrix(glm::ortho<float>(-50, 50, 0, 100, -50, 50));
		MagicEngineMain::setDirectionalLight(DirectionalLight(baseLight, lightDir, lightProjMatrix));
	}
	else if (distToGlobeCenter < MaxDistForOutside) 
	{
		MagicEngineMain::setCurrentSceneState(GameObject::OutsideSnowball);

		// Make light black
		BaseLight baseLight(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 0.0f, 0.0f));
		vec3 lightDir(normalize(vec3(0.6f, -0.3f, 0.9f)));
		// make ProjectionMatrix size fit the track size per texture view in gDEBugger
		mat4 lightProjMatrix(glm::ortho<float>(-50, 50, 0, 100, -50, 50));
		MagicEngineMain::setDirectionalLight(DirectionalLight(baseLight, lightDir, lightProjMatrix));
	}
	else
	{
		MagicEngineMain::setCurrentSceneState(GameObject::OutsideSnowball);

		BaseLight baseLight(vec3(0.448f, 0.361f, 0.60f), vec3(0.1f, 0.1f, 0.3f), vec3(0.0f, 0.0f, 0.0f));
		vec3 lightDir(normalize(vec3(0.6f, -0.3f, 0.9f)));
		// make ProjectionMatrix size fit the track size per texture view in gDEBugger
		mat4 lightProjMatrix(glm::ortho<float>(-50, 50, 0, 100, -50, 50));
		MagicEngineMain::setDirectionalLight(DirectionalLight(baseLight, lightDir, lightProjMatrix));
	}

	/*
	// adapt fov to velocity
	const CommandLineParams& commandLineParams = MagicEngineMain::getCommandLineParams();
	float windowAspect = static_cast<float>(commandLineParams._xRes) / static_cast<float>(commandLineParams._yRes);

	float filteredDt = clamp(dtf, 0.016f, 0.033f); // irregular deltatime somehow
	
	float speed = length(_lastFramePosition - pos) / filteredDt;
	_lastVelocities[_lastVelocityIndex] = speed;
	_lastVelocityIndex = (_lastVelocityIndex + 1) % s_velocityBufferSize;
	float avgSpeed = 0;
	for (unsigned int i = 0; i < s_velocityBufferSize; ++i)
	{
		avgSpeed += _lastVelocities[i];
	}
	avgSpeed /= float(s_velocityBufferSize);

	float speedFactor = (avgSpeed - MinSpeed) / (MaxSpeed - MinSpeed);
	speedFactor = clamp(speedFactor, 0.0f, 1.0f);

	float targetFoV = MinFov + (MaxFov - MinFov) * speedFactor;
	_foV = _fovController.tick(filteredDt, targetFoV);
	_foV = clamp(_foV, MinFov, MaxFov);
	_projectionMatrix = glm::perspective(_foV, windowAspect, _zNearPlane, _zFarPlane);
	*/
	if (!_running)
	{
		_timeSinceLastKeyPoint += dtf;
		Camera::update(dt);
	}
	else
	{
		advance(dtf);
		
		// linear interpolation
		float mixFactor = _keyPointProgress / _keyPoints[_currentKeyPointIndex]._duration;
		_overallProgress = (float)getPreviousKeyPointsIndex() + mixFactor;
		int nextKeyPointIndex = getNextKeyPointIndex();

		//******** Position *****
		vec3 vPresentPosition(0, 0, 0);
		if (_overallProgress > 633 && _overallProgress < 634) // Remove middle frame so that transition is smooth
			vPresentPosition = _keyPoints[nextKeyPointIndex+1]._position;
		else if (_overallProgress > 634 && _overallProgress < 635) // Remove middle frame so that transition is smooth
			vPresentPosition = _keyPoints[nextKeyPointIndex]._position;
		else 
			vPresentPosition = _keyPoints[_currentKeyPointIndex]._position + (_keyPoints[nextKeyPointIndex]._position - _keyPoints[_currentKeyPointIndex]._position) * mixFactor;

		//******** Rotation *********
		aiQuaternion qPresentRotation(1, 0, 0, 0);
		aiQuaternion keyPoint     = aiQuaternion(_keyPoints[_currentKeyPointIndex]._quat.w, _keyPoints[_currentKeyPointIndex]._quat.x, _keyPoints[_currentKeyPointIndex]._quat.y, _keyPoints[_currentKeyPointIndex]._quat.z);
		aiQuaternion nextkeyPoint = aiQuaternion(_keyPoints[nextKeyPointIndex]._quat.w, _keyPoints[nextKeyPointIndex]._quat.x, _keyPoints[nextKeyPointIndex]._quat.y, _keyPoints[nextKeyPointIndex]._quat.z);
		aiQuaternion::Interpolate(qPresentRotation, keyPoint, nextkeyPoint, mixFactor);

		//build a transformation matrix from it
		aiMatrix4x4 mTransformation = aiMatrix4x4(qPresentRotation.GetMatrix());
		//mTransformation.a1 *= vPresentScaling.x; mTransformation.b1 *= vPresentScaling.x; mTransformation.c1 *= vPresentScaling.x;
		//mTransformation.a2 *= vPresentScaling.y; mTransformation.b2 *= vPresentScaling.y; mTransformation.c2 *= vPresentScaling.y;
		//mTransformation.a3 *= vPresentScaling.z; mTransformation.b3 *= vPresentScaling.z; mTransformation.c3 *= vPresentScaling.z;
		mTransformation.a4 = vPresentPosition.x; mTransformation.b4 = vPresentPosition.y; mTransformation.c4 = vPresentPosition.z;

		glm::mat4 transformation = glm::mat4(
			mTransformation.a1, mTransformation.a2, mTransformation.a3, mTransformation.a4, 
			mTransformation.b1, mTransformation.b2, mTransformation.b3, mTransformation.b4, 
			mTransformation.c1, mTransformation.c2, mTransformation.c3, mTransformation.c4, 
			mTransformation.d1, mTransformation.d2, mTransformation.d3, mTransformation.d4);

		vec4 rotatedFor = transformation * vec4(0, 1, 0, 0);
		vec3 viewDir = normalize(vec3(-rotatedFor.x, rotatedFor.y, rotatedFor.z));

		//vec4 rotatedUp = transformation * vec4(0, 0, 1, 0);
		//vec3 up = normalize(vec3(-rotatedUp.x, rotatedUp.y, rotatedUp.z));

		vec3 up = vec3(0, 1, 0);

		// use linear interpolation as input for PID controller
		/*_position.x = _xPosController.tick(dtf, vPresentPosition.x);
		_position.y = _yPosController.tick(dtf, vPresentPosition.y);
		_position.z = _zPosController.tick(dtf, vPresentPosition.z);
		_viewDir.x = _xViewDirController.tick(dtf, viewDir.x);
		_viewDir.y = _yViewDirController.tick(dtf, viewDir.y);
		_viewDir.z = _zViewDirController.tick(dtf, viewDir.z);
		
		_viewDir = normalize(_viewDir);
		*/

		_position = vPresentPosition;
		_viewDir = viewDir;

		_viewMatrix = glm::lookAt(
			_position,				// Camera is here
			_position + _viewDir,	// and looks here : at the same position, plus "direction"
			up						// Head is up (set to 0,-1,0 to look upside-down)
			);

		//******** DOF *********
		float focaldistance = _keyPoints[_currentKeyPointIndex]._depthOfField.x + (_keyPoints[nextKeyPointIndex]._depthOfField.x - _keyPoints[_currentKeyPointIndex]._depthOfField.x) * mixFactor;
		float focalrange    = _keyPoints[_currentKeyPointIndex]._depthOfField.y + (_keyPoints[nextKeyPointIndex]._depthOfField.y - _keyPoints[_currentKeyPointIndex]._depthOfField.y) * mixFactor;


		PostProcessingRenderingPipeline::s_focalDistance = focaldistance;
		PostProcessingRenderingPipeline::s_focalRange = focalrange;

		//******** FOV *********
		float fov           = _keyPoints[_currentKeyPointIndex]._fov + (_keyPoints[nextKeyPointIndex]._fov - _keyPoints[_currentKeyPointIndex]._fov) * mixFactor;
		_foV = fov;

		const CommandLineParams& commandLineParams = MagicEngineMain::getCommandLineParams();
		float windowAspect = static_cast<float>(commandLineParams._xRes) / static_cast<float>(commandLineParams._yRes);
		_projectionMatrix = glm::perspective(_foV, windowAspect, _zNearPlane, _zFarPlane);

		//cout << "DOF " << focaldistance << ", " << focalrange << ", FOV " << fov << endl;

	}

	_lastFramePosition = pos;
}


void PresentationCamera::advance(float dt)
{
	if (_keyPoints.empty() || _finished)
	{
		return;
	}

	_keyPointProgress += dt;
	if (_keyPointProgress >= _keyPoints[_currentKeyPointIndex]._duration)
	{
		_keyPointProgress -= _keyPoints[_currentKeyPointIndex]._duration;
		++_currentKeyPointIndex;

		if (_currentKeyPointIndex >= _keyPoints.size() - 1)
		{
			_currentKeyPointIndex = _keyPoints.size() - 2; // stay in last frame
			_finished = true;
			//_currentKeyPointIndex -= _keyPoints.size(); // loop
		}
	}
}

int PresentationCamera::getNextKeyPointIndex()
{
	if (_currentKeyPointIndex + 1 >= _keyPoints.size())
	{
		return _currentKeyPointIndex + 1 - _keyPoints.size();
	}
	else
	{
		return _currentKeyPointIndex + 1;
	}
}

int PresentationCamera::getPreviousKeyPointsIndex()
{
	if (_currentKeyPointIndex - 1 < 0)
	{
		return _currentKeyPointIndex - 1 + _keyPoints.size();
	}
	else
	{
		return _currentKeyPointIndex - 1;
	}
}

float PresentationCamera::mixValuesForCurrentProgress(float valA, float valB)
{
	int nextKeyPointIndex = getNextKeyPointIndex();

	float mixFactor = _keyPointProgress / _keyPoints[_currentKeyPointIndex]._duration;

	return mixValues(valA, valB, mixFactor);
}

float PresentationCamera::mixValues(float valA, float valB, float mixFactor)
{ 
	return valA * (1 - mixFactor) + valB * mixFactor;
}

void PresentationCamera::loadFromCollada() {

	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile("resources\\models\\camera.dae", 0x0);

	/* LOAD SCENE - Load all meshes of the scene */
	for (unsigned int i = 0; i < scene->mNumCameras; ++i) {
		aiCamera *cam = scene->mCameras[i];
		aiAnimation *animation = scene->mAnimations[i];

		_keyPoints.clear();
		// Only on animation channel
		for (unsigned int x = 0; x < animation->mChannels[0]->mNumPositionKeys; x++) {

			KeyPoint newKeyPoint;
			
			newKeyPoint._position.x = animation->mChannels[i]->mPositionKeys[x].mValue.x;
			newKeyPoint._position.y = animation->mChannels[i]->mPositionKeys[x].mValue.z;
			newKeyPoint._position.z = -animation->mChannels[i]->mPositionKeys[x].mValue.y;

			newKeyPoint._quat.w = animation->mChannels[i]->mRotationKeys[x].mValue.w;
			newKeyPoint._quat.x = animation->mChannels[i]->mRotationKeys[x].mValue.x;
			newKeyPoint._quat.y = animation->mChannels[i]->mRotationKeys[x].mValue.y;
			newKeyPoint._quat.z = animation->mChannels[i]->mRotationKeys[x].mValue.z;

			newKeyPoint._fov = animation->mChannels[i]->mScalingKeys[x].mValue.x;

			newKeyPoint._depthOfField.x = animation->mChannels[i]->mScalingKeys[x].mValue.y;
			newKeyPoint._depthOfField.y = animation->mChannels[i]->mScalingKeys[x].mValue.z;

			newKeyPoint._duration = 0.3f;

			// Set the initial position
			if (x == 0)
				_position = vec3(newKeyPoint._position.x, newKeyPoint._position.y, newKeyPoint._position.z);

			_keyPoints.push_back(newKeyPoint);
		}

	}

}
