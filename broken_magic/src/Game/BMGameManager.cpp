#include "BMGameManager.hpp"

#include "../Engine/MagicEngineMain.hpp"
#include "../Engine/ShadowMappingPipeline.hpp"
#include "../Engine/VolumetricObject.hpp"
#include "../Engine/GameObject.hpp"
#include "../Engine/Light.hpp"
#include "../Engine/LightRepresentation.hpp"

#include <sstream>
#include <string>
#include <stack>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace std;
using namespace glm;
using namespace MagicEngine;
using namespace BrokenMagic;

namespace
{
	string CAMERA_PATH_FILE = "resources\\cameraPath.txt";
}

bool BMGameManager::s_frameTime = false;
bool BMGameManager::s_toggleRunning = false;
bool BMGameManager::s_saveKeyPoint = false;
bool BMGameManager::s_saveKeyPointsToFile = false;
bool BMGameManager::s_fastForward = false;

BMGameManager::BMGameManager()
{
}

BMGameManager::~BMGameManager()
{
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	static int minSamplingMethod = GL_LINEAR;
	static int maxSamplingMethod = GL_LINEAR;

	if (key == GLFW_KEY_F2 && action == GLFW_PRESS)
	{
		BMGameManager::s_frameTime = !BMGameManager::s_frameTime;
	}

	if (key == GLFW_KEY_F3 && action == GLFW_PRESS)
	{
		MagicEngineMain::changeWireFrameMode();
	}

	if (key == GLFW_KEY_F5 && action == GLFW_PRESS)
	{
		BMGameManager::s_toggleRunning = true;
	}

	if (key == GLFW_KEY_F6 && action == GLFW_PRESS)
	{
		BMGameManager::s_saveKeyPoint = true;
	}

	if (key == GLFW_KEY_F7 && action == GLFW_PRESS)
	{
		BMGameManager::s_saveKeyPointsToFile = true;
	}

	if (key == GLFW_KEY_F8 && action == GLFW_PRESS)
	{
		MagicEngineMain::incrementDeferredDebugRenderingState();
	}

	if (key == GLFW_KEY_F9 && action == GLFW_PRESS)
	{
		MagicEngineMain::incrementDepthOfFieldRenderingState();
	}

	if (key == GLFW_KEY_F10 && action == GLFW_PRESS)
	{
		MagicEngineMain::decrementDepthOfFieldRenderingState();
	}

	if (key == GLFW_KEY_F11 && action == GLFW_PRESS)
	{
		MagicEngineMain::switchNormalMapRenderingState();
	}

	if (key == GLFW_KEY_8 && action == GLFW_PRESS)
	{
		MagicEngineMain::setCurrentSceneState(GameObject::Indoor);
	}

	if (key == GLFW_KEY_9 && action == GLFW_PRESS)
	{
		MagicEngineMain::setCurrentSceneState(GameObject::Outdoor);
	}
		
	if (key == GLFW_KEY_1 && action == GLFW_PRESS)
	{
		// increase focal distance
		PostProcessingRenderingPipeline::s_focalDistance += 3.0f / 25.5f;
	}

	if (key == GLFW_KEY_2 && action == GLFW_PRESS)
	{
		// increase focal distance 
		PostProcessingRenderingPipeline::s_focalRange += 3.0f / 25.5f;
	}

	if (key == GLFW_KEY_3 && action == GLFW_PRESS)
	{
		// focal distance 14, range 3
		PostProcessingRenderingPipeline::s_focalDistance = 0.0f;
		PostProcessingRenderingPipeline::s_focalRange = 0.1f;
	}

	if (key == GLFW_KEY_4 && action == GLFW_PRESS)
	{
		MagicEngineMain::DepthOfFieldRenderingStateOutput();
	}

	if (key == GLFW_KEY_5 && action == GLFW_PRESS)
	{
		PostProcessingRenderingPipeline::s_exposure += 0.1f;
	}

	if (key == GLFW_KEY_6 && action == GLFW_PRESS)
	{
		PostProcessingRenderingPipeline::s_exposure -= 0.1f;
	}

	if (key == GLFW_KEY_7 && action == GLFW_PRESS)
	{
		BMGameManager::s_fastForward = true;
	}
}

void BMGameManager::init()
{
	cout << "Starting Broken Magic Demo" << endl;
	cout << "_____________________________________________________________" << endl;
	cout << "F2: Toggle frame time display" << endl;
	cout << "F3: Toggle wireframe mode" << endl;
	cout << "F5: Toggle presentation camera running" << endl;
	//cout << "F6: Save key point for presentation camera" << endl;
	//cout << "F7: Save key points of presentation camera to file" << endl;
	cout << "F8: Switch through deferred render buffers" << endl;
	cout << "F9: Switch through depth of field buffers" << endl;
	cout << "F10:Switch back through depth of field buffers" << endl;
	cout << "F11:Switch through bloom buffers" << endl;
	cout << "\"1\": increase depth of field focal distance to change the circle of confusion" << endl;
	cout << "\"2\": increase depth of field focal range    to change the circle of confusion" << endl;
	cout << "\"3\": Reset the focaldistance and focal range" << endl;
	cout << "\"4\": Output on depth of field buffer informations" << endl;
	cout << "\"5\": increase HDR exposure" << endl;
	cout << "\"6\": decrease HDR exposure" << endl;
	cout << "\"7\": fast forward 10s" << endl;

	const CommandLineParams &commandLineParams = MagicEngineMain::getCommandLineParams();

	GLFWwindow* window = MagicEngineMain::getWindow();
	glfwSetKeyCallback(window, key_callback);

	//BaseLight baseLight(vec3(0.8f, 0.6f, 0.6f), vec3(0.1f, 0.1f, 0.1f), vec3(0.3f, 0.3f, 0.3f));
	BaseLight baseLight(vec3(0.03f, 0.03f, 0.03f), vec3(0.2f, 0.2f, 0.2f), vec3(0.0f, 0.0f, 0.0f));
	vec3 lightDir(normalize(vec3(0, -0.9f, 0.1f)));
	// make ProjectionMatrix size fit the track size per texture view in gDEBugger
	mat4 lightProjMatrix(glm::ortho<float>(-50, 50, 0, 100, -50, 50));
	MagicEngineMain::setDirectionalLight(DirectionalLight(baseLight, lightDir, lightProjMatrix));

	_presentationCamera = new PresentationCamera();
	if (_presentationCamera)
	{
		_presentationCamera->loadFromFile(CAMERA_PATH_FILE);
		_presentationCamera->startPresentation();
	}
	MagicEngineMain::setCamera(_presentationCamera);

	// Load in Scene (Voraussetzung m�ssen UV Layout beinhalten!)
	//ResourceManager::loadCompleteScene(&_gameObjects, "resources\\models\\cube.fbx");
	ResourceManager::loadCompleteScene(&_gameObjects, "resources\\models\\magician_tower.fbx");
	//ResourceManager::loadCompleteScene(&_gameObjects, "resources\\models\\light - 03-12-15.fbx");
	//ResourceManager::loadCompleteScene(&_gameObjects, "resources\\models\\simple_tower.fbx");
	//ResourceManager::loadCompleteScene(&_gameObjects, "resources\\models\\light.fbx");
	//ResourceManager::loadCompleteScene(&_gameObjects, "resources\\models\\tower_with_cubes.fbx");	

	GameObject* root = MagicEngineMain::getSceneGraphRootNode();

	GameObject* lightRoot = new GameObject();
	lightRoot->animateRotate(0, 1, 0, 20);
	lightRoot->setInScenePart(0xFF);
	root->addChild(lightRoot);

	{
		vec3 position(0.871365f, 47.873268f, 0.021784f);
		vec3 ambient(0.111997f, 0.115312f, 0.250000f);
		vec3 diffuse(0.447988f, 0.461247f, 1.000000f);
		vec3 specular(0.447988f, 0.461247f, 1.000000f);
		vec3 direction(0.000000f, 0.000000f, 0.000000f);
		Attenuation attenuation(0.000000f, 0.050000f, 0.100000f);
		BaseLight baseLight(diffuse * 2.0f, ambient * 3.0f, specular * 2.0f);
		float nearPlane = 0.2f;
		float farPlane = 10;
		float innerCone = 6.283185f;
		float outerCone = 6.283185f;
		int type = 2;

		_indoorLight = new Light(position, attenuation, baseLight,
			innerCone, outerCone, direction, type, nearPlane, farPlane);
		_indoorLight->setModelDataForLight(ResourceManager::getModelData("resources\\models\\icosphere3.obj"));
		_indoorLight->setInScenePart(GameObject::Indoor);
		_indoorLight->setGeometryName("pointlight_indoor");
		lightRoot->addChild(_indoorLight);

		_indoorLightRep = new LightRepresentation(_indoorLight);
		_indoorLightRep->scale(0.25f);
		_indoorLightRep->setInScenePart(GameObject::Indoor);
		_indoorLightRep->_ambientFactor = 0.5f;
		_indoorLightRep->_diffuseFactor = 0.7f;
		_indoorLightRep->_specularFactor = 0.2f;
		_indoorLightRep->setGeometryName("pointlight_rep_indoor");
		root->addChild(_indoorLightRep);
	}

	{
		vec3 position(9.513367f, 69.762032f, 5.375236f);
		vec3 ambient(0.250000f, 0.145369f, 0.145369f);
		vec3 diffuse(1.000000f, 0.581474f, 0.581474f);
		vec3 specular(1.000000f, 0.581474f, 0.581474f);
		vec3 direction(0.000000f, 0.000000f, 0.000000f);
		Attenuation attenuation(0.000000f, 0.005000f, 0.005000f);
		BaseLight baseLight(diffuse * 2.0f, ambient * 3.0f, specular * 2.0f);
		float nearPlane = 5.0f;
		float farPlane = 150.0f;
		float innerCone = 6.283185f;
		float outerCone = 6.283185f;
		int type = 2;

		_outdoorLight = new Light(position, attenuation, baseLight,
			innerCone, outerCone, direction, type, nearPlane, farPlane);
		_outdoorLight->setModelDataForLight(ResourceManager::getModelData("resources\\models\\icosphere3.obj"));
		_outdoorLight->setInScenePart(GameObject::Outdoor);
		_outdoorLight->setGeometryName("point_outdoor");
		lightRoot->addChild(_outdoorLight);

		_outdoorLightRep = new LightRepresentation(_outdoorLight);
		_outdoorLightRep->scale(3.0f);
		_outdoorLightRep->setInScenePart(GameObject::Outdoor);
		_outdoorLightRep->_ambientFactor = 0.5f;
		_outdoorLightRep->_diffuseFactor = 0.7f;
		_outdoorLightRep->_specularFactor = 0.2f;
		_outdoorLightRep->setGeometryName("pointlight_rep_outdoor");
		root->addChild(_outdoorLightRep);
	}
	
	_indoorSnowglobe = MagicEngineMain::getGameObjectWithName("indoor_snowball_small");
	_indoorSnowglobeBroken = MagicEngineMain::getGameObjectWithName("indoor_snowball_broken");
	_indoorSnowglobeBroken->setVisibility(false);

	_outdoorEagle = MagicEngineMain::getGameObjectWithName("outdoor_eagleCam");
	_outdoorEagle->setPosition(_outdoorEagle->getPosition() + vec3(0, 200, +200));
	_outdoorEagle->scale(50, 50, 50);
	_outdoorEagle->setInScenePart(GameObject::Outdoor | GameObject::OutsideSnowball);

	_indoorEagle = MagicEngineMain::getGameObjectWithName("indoor_eagleCam");
	_indoorEagle->setInScenePart(GameObject::Indoor);

	// add clouds in globe
	{
		_globeClouds = new VolumetricObject();
		_globeClouds->translate(0, -10, 0);
		_globeClouds->scale(120.0f);
		_globeClouds->setInScenePart(GameObject::Outdoor);
#ifdef _DEBUG
		_globeClouds->setVolumeTexture(ResourceManager::getVolumeTexture("resources\\cloud2.txt")); // loading 1 texture for debug is enough
#else
		_globeClouds->setVolumeTexture(ResourceManager::getVolumeTexture("resources\\cloud.txt"));
#endif
		_globeClouds->setStepSize(0.012f / commandLineParams._quality);
		_globeClouds->setLightRayStepSize(0.012f * 10.0f / commandLineParams._quality);
		_globeClouds->setOpacityFactor(20.0f);
		_globeClouds->setLightInfo(VolumetricLightInfo(false, vec3(0.5f, 0.4f, 0.4f), normalize(vec3(0, -0.9f, 0.1f))));
		_globeClouds->setNoiseOffsetFactor(0.1f);
		_globeClouds->setNoiseSpeed(vec2(0.02f, 0.02f));
		_globeClouds->setLockedObject(_outdoorLight);
		root->addChild(_globeClouds);

		// add clouds in room
		_roomClouds = new VolumetricObject();
		_roomClouds->translate(0, 52.5f, 0);
		_roomClouds->scale(10.0f);
		_roomClouds->setInScenePart(GameObject::Indoor);
		_roomClouds->setVolumeTexture(ResourceManager::getVolumeTexture("resources\\cloud2.txt"));
		_roomClouds->setStepSize(0.015f / commandLineParams._quality);
		_roomClouds->setLightRayStepSize(0.015f * 10.0f / commandLineParams._quality);
		_roomClouds->setOpacityFactor(10.0f);
		_roomClouds->setNoiseOffsetFactor(0.15f);
		_roomClouds->setNoiseSpeed(vec2(-0.05f, 0.03f));
		_roomClouds->setLockedObject(_indoorLight);
		_roomClouds->setLightInfo(VolumetricLightInfo(false, vec3(0.3f, 0.3f, 0.5f), normalize(vec3(0, 0.9f, 0.1f))));
		root->addChild(_roomClouds);
	}

	// cloud debug rendering
#ifdef _DEBUG
	shared_ptr<ModelData> sphere = ResourceManager::getSphereModelData();
	if (sphere)
	{
		GameObject* cloudRep = new GameObject();
		cloudRep->scale(0.25f, 0.25f, 0.25f);
		cloudRep->setModelDataForLight(sphere);
		_roomClouds->addChild(cloudRep);
	}
#endif
}

void BMGameManager::deinit()
{
	for (GameObject* &child : _gameObjects) {
		delete child;
	}
	_gameObjects.clear();

	if (_presentationCamera)
	{
		delete _presentationCamera;
	}
	if (_globeClouds)
	{
		delete _globeClouds;
		_globeClouds = nullptr;
	}
	if (_roomClouds)
	{
		delete _roomClouds;
		_roomClouds = nullptr;
	}
	if (_indoorLight)
	{
		delete _indoorLight;
		_indoorLight = nullptr;
	}
	if (_outdoorLight)
	{
		delete _outdoorLight;
		_outdoorLight = nullptr;
	}
	if (_indoorLightRep)
	{
		delete _indoorLightRep;
		_indoorLightRep = nullptr;
	}
	if (_outdoorLightRep)
	{
		delete _outdoorLightRep;
		_outdoorLightRep = nullptr;
	}
}

void BMGameManager::update(double dt)
{
	GLFWwindow* window = MagicEngineMain::getWindow();
	ostringstream titleStream;
	titleStream << "Broken Magic" << " | FPS = " << MagicEngineMain::getFPS() << " | Frametime = " << (dt * 1000.0) << " ms | " << " progress = " << _presentationCamera->getProgress()
		<< " | pos(" << _presentationCamera->getPosition().x << ", " << _presentationCamera->getPosition().y << ", " << _presentationCamera->getPosition().z << ")";
	glfwSetWindowTitle(window, titleStream.str().c_str());

	if (_presentationCamera)
	{
		float progress = _presentationCamera->getProgress();
		if (progress > 971 && !_broken)
		{
			_indoorSnowglobe->animateTranslate(-1.2f,0,-1.4f);
			_indoorSnowglobe->animateRotate(0, 1, 0, -90);
			_indoorSnowglobeBroken->setVisibility(true);
			_broken = true;
		}

		if (progress > 975)
		{
			_indoorSnowglobe->setVisibility(false);
		}

		if (progress > 634 && !_secondCycle)
		{
			if (_globeClouds) _globeClouds->setLightInfo(VolumetricLightInfo(false, vec3(0.6f, 0.2f, 0.2f), normalize(vec3(0, -0.9f, 0.1f))));
			if (_roomClouds) _roomClouds->setLightInfo(VolumetricLightInfo(false, vec3(0.7f, 0.1f, 0.1f), normalize(vec3(0, 0.9f, 0.1f))));

			_indoorLight->_baseLight.DiffuseColor = vec3(0.5f, 0.2f, 0.2f);
			_indoorLight->_baseLight.SpecularColor = vec3(0.5f, 0.2f, 0.2f);
			_indoorLight->_baseLight.AmbientColor = vec3(0.25f, 0.1f, 0.1f);
			_outdoorLight->_baseLight.DiffuseColor = vec3(0.7f, 0.2f, 0.2f);
			_outdoorLight->_baseLight.SpecularColor = vec3(0.5f, 0.2f, 0.2f);
			_outdoorLight->_baseLight.AmbientColor = vec3(0.35f, 0.1f, 0.1f);

			_secondCycle = true;
		}

		if (progress > 75)
		{
			_outdoorEagle->animateTranslate(0, -4.0f, 0);
			_outdoorEagle->animateScale(.9f);
		}

		if (progress > 605)
		{
			_indoorEagle->animateTranslate(+3.0f, 0, 0);
		}

		if (s_toggleRunning)
		{
			if (_presentationCamera->isRunning())
			{
				_presentationCamera->stopPresentation();
			}
			else
			{
				_presentationCamera->startPresentation();
			}

			s_toggleRunning = false;
		}

		if (s_fastForward)
		{
			_presentationCamera->advance(10);
			s_fastForward = false;
		}

		/*
		if (s_saveKeyPoint)
		{
			_presentationCamera->saveKeyPoint();
			s_saveKeyPoint = false;
		}

		if (s_saveKeyPointsToFile)
		{
			_presentationCamera->saveKeyPointsToFile(CAMERA_PATH_FILE);
			s_saveKeyPointsToFile = false;
		}*/
	}
}

void BMGameManager::render()
{
}
