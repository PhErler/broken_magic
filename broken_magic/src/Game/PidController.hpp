#pragma once

namespace BrokenMagic
{
	class PidController
	{
	public:
		PidController(float initValue, float proportionalFactor, float integralFactor, float differentialFactor);
		virtual ~PidController();

		void reset(float resetValue);

		float tick(float dt, float targetValue);

	protected:
		float _proportionalFactor;
		float _integralFactor;
		float _differentialFactor;

		float _currentValue;
		float _integratedError;
		float _previousError;
	};
}

