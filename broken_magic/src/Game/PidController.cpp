#include "PidController.hpp"

using namespace BrokenMagic;

namespace
{
}

PidController::PidController(float initValue, float proportionalFactor, float integralFactor, float differentialFactor)
	: _proportionalFactor(proportionalFactor)
	, _integralFactor(integralFactor)
	, _differentialFactor(differentialFactor)
	, _currentValue(initValue)
	, _integratedError(0)
	, _previousError(0)
{
}

PidController::~PidController()
{
}

void PidController::reset(float resetValue)
{
	_currentValue = resetValue;
	_integratedError = 0;
	_previousError = 0;
}

float PidController::tick(float dt, float targetValue)
{
	// calculate the difference between
	// the desired value and the actual value
	float error = targetValue - _currentValue;
	// track error over time, scaled to the timer interval
	_integratedError += error * dt;
	// determine the amount of change from the last time checked
	float derivative = (error - _previousError) / dt;

	// calculate how much to drive the output in order to get to the 
	// desired setpoint. 
	float newValue = _currentValue + (_proportionalFactor * error) + (_integralFactor * _integratedError) + (_differentialFactor * derivative);

	// remember the current value and the error for the next time around
	_previousError = error;
	_currentValue = newValue;

	return newValue;
}