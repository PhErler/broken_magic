#pragma once

#include "PresentationCamera.hpp"

#include "../Engine/IGameManager.hpp"
#include "../Engine/GameObject.hpp"
#include "../Engine/texture.hpp"
#include "../Engine/GLSLProgram.hpp"
#include "../Engine/Camera.hpp"

#include <vector>
#include <memory>

#include <glm/glm.hpp>

namespace MagicEngine
{
	class GameObject;
	class VolumetricObject;
	class Light;
	class LightRepresentation;
}

namespace BrokenMagic
{
	class BMGameManager : public MagicEngine::IGameManager
	{
	public:
		BMGameManager();
		virtual ~BMGameManager();

		virtual void init();
		virtual void deinit();

		virtual void update(double dt);
		virtual void render();

		static bool s_frameTime;
		static bool s_toggleRunning;
		static bool s_saveKeyPoint;
		static bool s_saveKeyPointsToFile;
		static bool s_fastForward;

	protected:
		std::vector<MagicEngine::GameObject*> _gameObjects;
		PresentationCamera* _presentationCamera;

		MagicEngine::GameObject* _indoorSnowglobe = nullptr;
		MagicEngine::GameObject* _indoorSnowglobeBroken = nullptr;
		MagicEngine::VolumetricObject* _globeClouds = nullptr;
		MagicEngine::VolumetricObject* _roomClouds = nullptr;
		MagicEngine::Light* _outdoorLight = nullptr;
		MagicEngine::Light* _indoorLight = nullptr;
		MagicEngine::GameObject* _outdoorEagle = nullptr;
		MagicEngine::GameObject* _indoorEagle = nullptr;
		MagicEngine::LightRepresentation* _indoorLightRep = nullptr;
		MagicEngine::LightRepresentation* _outdoorLightRep = nullptr;

		bool _broken = false;
		bool _secondCycle = false;
	};
}

