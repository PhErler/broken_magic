#include "BMGameManager.hpp"
#include "../Engine/MagicEngineMain.hpp"

using namespace MagicEngine;
using namespace BrokenMagic;

int main(int argc, char* argv[])
{
	BMGameManager gameManager;
	MagicEngineMain::setGameManager(&gameManager);

	int error = MagicEngineMain::init(argc, argv);
	if (error == EXIT_SUCCESS)
	{
		error = MagicEngineMain::run();
	}
	error = MagicEngineMain::deinit();

	assert(error == 0);

	return error;
}