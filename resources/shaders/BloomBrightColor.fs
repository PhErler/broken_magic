#version 330

uniform sampler2D Tex0;
in vec2 FragTexCoord;

out vec4 BrightColor;

void main (void)
{
	vec4 color = texture2D(Tex0, FragTexCoord);
	
    // Check whether fragment output is higher than threshold, 
	// if so output as brightness color
    float brightness = dot(color.rgb, vec3(0.2126, 0.7152, 0.0722));
    
	if(brightness > 0.3)
        BrightColor = vec4(color.rgb, 1.0);

}
