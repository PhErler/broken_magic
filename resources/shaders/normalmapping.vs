#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec3 tangent;

out mat3 tbnMatrix;
out vec2 UV;
out vec3 Position_worldspace;
out vec3 EyeDirection_cameraspace;
out vec3 LightDirection_cameraspace;
out vec4 ShadowCoord;

// Can be set by the CPU
uniform mat4 MVP;
uniform mat4 V;
uniform mat4 M;
uniform vec3 LightPosition_worldspace;
uniform mat4 DepthBiasMVP;

void main() {
	gl_Position = MVP * vec4(position, 1.0);
	
	ShadowCoord = DepthBiasMVP * vec4(position,1);

	// Position of the vertex, in worldspace : M * position
	Position_worldspace = (M * vec4(position,1)).xyz;
	
	// Vector that goes from the vertex to the camera, in camera space.
	// In camera space, the camera is at the origin (0,0,0).
	vec3 vertexPosition_cameraspace = ( V * M * vec4(position,1)).xyz;
	EyeDirection_cameraspace = vec3(0,0,0) - vertexPosition_cameraspace;
	
	// Vector that goes from the vertex to the light, in camera space. M is ommited because it's identity.
	vec3 LightPosition_cameraspace = ( V * vec4(LightPosition_worldspace,1)).xyz;
	LightDirection_cameraspace = LightPosition_cameraspace + EyeDirection_cameraspace;
	
	// Normal of the the vertex, in camera space
	vec3 Normal_cameraspace = normalize(( V * M * vec4(normal,0)).xyz); // Only correct if ModelMatrix does not scale the model ! Use its inverse transpose if not.
	vec3 Tangent_cameraspace = normalize(( V * M * vec4(tangent,0)).xyz);

	Tangent_cameraspace = normalize(Tangent_cameraspace -  dot(Tangent_cameraspace, Normal_cameraspace) * Normal_cameraspace);

	vec3 biTangent = cross(Tangent_cameraspace, Normal_cameraspace);

	tbnMatrix = mat3(Tangent_cameraspace, biTangent, Normal_cameraspace);

	// UV of the vertex. No special space for this one.
	UV = texCoord;
	
}