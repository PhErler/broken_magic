#version 330 core

uniform sampler2D Tex0;
uniform float focalDistance;
uniform float focalRange;
uniform vec2 nearFar;


in vec2 FragTexCoord;

layout(location = 0) out vec4 color;

float LinearizeDepth(float zoverw){
	float n = nearFar.x; // camera z near
	float f = nearFar.y; // camera z far
	return (2.0 * n) / (f + n - zoverw * (f - n));
}

void main (void)
{	
	float depth = LinearizeDepth(texture(Tex0, FragTexCoord).r)*4;
	
	float Blur = clamp(abs(depth - focalDistance) / focalRange, 0.0, 1.0);
	
	color = vec4(Blur,Blur,Blur,1.0);

	
	// OLD computation
	//float Blur = clamp(abs(-depth - focalDistance) / focalRange, 0.0, 1.0);
}
