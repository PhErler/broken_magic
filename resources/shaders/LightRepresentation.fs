#version 330 core

// Interpolated values from the vertex shaders
in vec3 normalVS;
in vec3 eyeVS;
in vec3 lightDirVS;

// Ouput data
out vec3 color;

// Values that stay constant for the whole mesh.
uniform vec3 colDiffuseLight;
uniform vec3 colAmbientLight;
uniform vec3 colSpecularLight;
uniform float ambientFactor;
uniform float diffuseFactor;
uniform float specularFactor;

void main()
{
	// Normal of the computed fragment, in camera space
	vec3 n = normalize( normalVS );
	// Direction of the light (from the fragment to the light)
	vec3 l = normalize( lightDirVS );
	// Cosine of the angle between the normal and the light direction, 
	// clamped above 0
	//  - light is at the vertical of the triangle -> 1
	//  - light is perpendicular to the triangle -> 0
	//  - light is behind the triangle -> 0
	float cosTheta = clamp( abs(dot( n,l )), 0,1 );
	
	// Eye vector (towards the camera)
	vec3 E = normalize(eyeVS);
	// Direction in which the triangle reflects the light
	vec3 R = reflect(-l,n);
	// Cosine of the angle between the Eye vector and the Reflect vector,
	// clamped to 0
	//  - Looking into the reflection -> 1
	//  - Looking elsewhere -> < 1
	float cosAlpha = clamp( abs(dot( E,R )), 0,1 );
	
	vec3 ambient = colAmbientLight; // Ambient : simulates indirect lighting
	vec3 diffuse = colDiffuseLight * cosTheta; // Diffuse : "color" of the object
	vec3 specular = colSpecularLight * pow(cosAlpha, 5); // Specular : reflective highlight, like a mirror

	color = ambient * ambientFactor + diffuse * diffuseFactor + specular * specularFactor;
}