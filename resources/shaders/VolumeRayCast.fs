#version 330 core

// Ouput data
layout(location = 0) out vec4 color;

uniform sampler3D volTex;
uniform sampler2D noiseTexture;
uniform sampler2D posBackFaceWSTex;
uniform sampler2D posFrontFaceWSTex;
uniform sampler2D posOpaqueGeometryWSTex;

uniform float stepSize;
uniform float lightRayStepSize;
uniform float opacityFactor;
uniform mat4 worldToModel;

uniform vec2 noisePosOffset;
uniform float noiseOffsetFactor;

uniform vec3 posCamMS;
uniform vec3 posLightMS;
uniform vec3 colLight;

noperspective in vec2 textureCoordinatesSS;

const float sqrt2 = 1.4142135624;
const float sqrt2Times2 = sqrt2 * 2.0;

// https://developer.nvidia.com/gpugems/GPUGems3/gpugems3_ch30.html
// http://old.cescg.org/CESCG-2005/papers/VRVis-Scharsach-Henning.pdf
// http://scivis.itn.liu.se/publications/2009/HLRR09/eg09-tutorialnotes.pdf
// http://graphicsrunner.blogspot.co.at/2009/01/volume-rendering-101.html

float rayCastToLight(vec3 fromMS)
{
		vec3 dirMS = posLightMS - fromMS;

		float finalA = 0.0;
		vec3 samplePosMS = fromMS;
		do
		{
			samplePosMS = lightRayStepSize * dirMS + samplePosMS;

			// has ray exceeded borders of bounding box?
			vec3 absSamplePosMS = abs(samplePosMS);
			if (absSamplePosMS.x > 1.0 || absSamplePosMS.y > 1.0 || absSamplePosMS.z > 1.0)
			{
				break;
			}

			vec2 noiseLookupPos = noisePosOffset + samplePosMS.xy;
			vec3 volTexOffset = (texture(noiseTexture, noiseLookupPos).xyz - 0.5) * noiseOffsetFactor;
			//vec3 volTexOffset = vec3(0.0, 0.0, 0.0); // light-weight variant, no cloud movement
			
			vec3 sampleTextureCoordinates = (samplePosMS * sqrt2Times2 + 1.0) * 0.5 + volTexOffset;
			float sampleA = texture(volTex, sampleTextureCoordinates).x * lightRayStepSize * opacityFactor;
			
			finalA += sampleA * (1 - finalA);

			// is almost opaque?
			if (finalA > 0.95)
			{
				finalA = 1.0;
				break;
			}

		} while (true);

		return finalA;
}

void main()
{
	// world space
	vec3 posBackFaceWS = texture(posBackFaceWSTex, textureCoordinatesSS).xyz;
	vec3 posFrontFaceWS = texture(posFrontFaceWSTex, textureCoordinatesSS).xyz;

	if (posBackFaceWS.x == 0.0 && posBackFaceWS.y == 0.0 && posBackFaceWS.z == 0.0)
	{
		color = vec4(0.0, 0.0, 0.0, 0.0);
	}
	else
	{
		vec3 posOpaqueGeometryWS = texture(posOpaqueGeometryWSTex, textureCoordinatesSS).xyz;

		// model space
		vec3 posBackFaceMS = (worldToModel * vec4(posBackFaceWS, 1.0)).xyz;
		vec3 posFrontFaceMS = (worldToModel * vec4(posFrontFaceWS, 1.0)).xyz;
		vec3 posOpaqueGeometryMS = (worldToModel * vec4(posOpaqueGeometryWS, 1.0)).xyz;
		
		vec3 camToOpaqueGeometryMS = posOpaqueGeometryMS - posCamMS;
		float distCamToOpaqueGeometryMS = abs(dot(camToOpaqueGeometryMS, camToOpaqueGeometryMS));
		
		vec3 dirMS = normalize(posBackFaceMS - posFrontFaceMS);

		// ray cast
		vec4 colFinal = vec4(0.0, 0.0, 0.0, 0.0);

		vec3 samplePosMS = posFrontFaceMS;
		do
		{
			// sample at surface to avoid banding
			vec3 camToSampleMS = samplePosMS - posCamMS;
			float distCamToSampleMS = abs(dot(camToSampleMS, camToSampleMS));
			if (distCamToSampleMS > distCamToOpaqueGeometryMS) // has ray hit opaque geometry?
			{
				samplePosMS = posOpaqueGeometryMS;
			}

			vec2 noiseLookupPos = noisePosOffset + samplePosMS.xy;
			vec3 volTexOffset = (texture(noiseTexture, noiseLookupPos).xyz - 0.5) * noiseOffsetFactor;
			//vec3 volTexOffset = vec3(0.0, 0.0, 0.0); // light-weight variant, no cloud movement
			
			vec3 sampleTextureCoordinates = (samplePosMS * sqrt2Times2 + 1.0) * 0.5 + volTexOffset;
			float sampleA = min(1.0, texture(volTex, sampleTextureCoordinates).x * stepSize * opacityFactor);
			
			// skip if almost completely transparent
			if (sampleA > 0.03)
			{
				float lightAbsorptionFactor = rayCastToLight(samplePosMS);
				float lightFactor = max(1.0 - lightAbsorptionFactor, 0.3); // consider ambient light
				vec4 colSample = vec4(colLight * lightFactor, 1.0);

				// front to back blending
				colFinal += colSample * sampleA * (1 - colFinal.w);
				
				// is almost opaque?
				if (colFinal.w > 0.95)
				{
					colFinal.w = 1.0;
					break;
				}
			}
			
			samplePosMS = stepSize * dirMS + samplePosMS;

			// has ray exceeded borders of bounding box?
			vec3 absSamplePosMS = abs(samplePosMS);
			if (absSamplePosMS.x > 1.0 || absSamplePosMS.y > 1.0 || absSamplePosMS.z > 1.0)
			{
				break;
			}
			// has ray hit opaque geometry?
			if (distCamToSampleMS > distCamToOpaqueGeometryMS)
			{
				break;
			}

		} while (true);

		color = colFinal;
	}
}