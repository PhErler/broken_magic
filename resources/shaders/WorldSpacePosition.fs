#version 330

in vec3 WorldSpacePos;

layout (location = 0) out vec3 WorldPosOut;

void main()
{
	WorldPosOut = WorldSpacePos;
}
