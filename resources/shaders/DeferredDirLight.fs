#version 330

struct BaseLight
{
    vec3 DiffuseColor;
    vec3 AmbientColor;
    vec3 SpecularColor;
};

struct DirectionalLight
{
    BaseLight Base;
    vec3 Direction;
};

uniform sampler2D uPositionMap;
uniform sampler2D uColorMap;
uniform sampler2D uNormalMap;

uniform DirectionalLight uDirectionalLight;

uniform vec3 uEyeWorldPos;
uniform float uSpecularExponent;
uniform vec2 uScreenSize;

out vec4 FragColor;

// Blinn-Phong-Shading
vec4 CalcLightInternal(BaseLight light,
					   vec3 lightDirection,
					   vec3 worldPos,
					   vec3 normal)
{
	// ambient
    vec4 ambientColor = vec4(light.AmbientColor, 1.0);
	
	// diffuse
    float diffuseFactor = clamp( dot(normal, lightDirection), 0, 1 );
	vec4 diffuseColor = vec4(light.DiffuseColor * diffuseFactor, 1.0);

	// specular
    vec4 specularColor = vec4(0, 0, 0, 0);
	vec3 vertexToEye = normalize(uEyeWorldPos - worldPos);
	vec3 lightReflect = normalize(reflect(lightDirection, normal));
	float specularFactor = clamp( dot(vertexToEye, lightReflect), 0, 1 );
	specularFactor = pow(specularFactor, uSpecularExponent);
	specularColor = vec4(light.SpecularColor * specularFactor, 1.0);

    return (ambientColor + diffuseColor + specularColor);
}

vec4 CalcDirectionalLight(vec3 worldPos, vec3 normal)
{
    return CalcLightInternal(uDirectionalLight.Base,
							 normalize(uDirectionalLight.Direction),
							 worldPos,
							 normal);
}

vec2 CalcTexCoord()
{
    return gl_FragCoord.xy / uScreenSize;
}

void main()
{
    vec2 texCoord = CalcTexCoord();
	vec3 worldPos = texture(uPositionMap, texCoord).xyz;
	vec3 diffuseColor = texture(uColorMap, texCoord).xyz;
	vec3 normal = texture(uNormalMap, texCoord).xyz;
	normal = normalize(normal);

	FragColor = vec4(diffuseColor, 1.0) * CalcDirectionalLight(worldPos, normal);
}
