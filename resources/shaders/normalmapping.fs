#version 330

uniform sampler2D diffuse_texture;
uniform sampler2D normalmap_texture;
uniform vec3 LightPosition_worldspace;
uniform sampler2DShadow shadowMap;

// Interpolated values from the vertex shaders
in mat3 tbnMatrix;
in vec2 UV;
in vec3 Position_worldspace;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;
in vec4 ShadowCoord;

layout(location = 0) out vec4 gl_FragColor;

void main() {
	
	// Light emission properties
	// You probably want to put them as uniforms
	vec3 LightColor = vec3(1,1,1);
	float LightPower = 1.0f;
	
	// Material properties
	vec3 MaterialDiffuseColor = (texture( diffuse_texture, UV )).xyz;
	vec3 MaterialAmbientColor = vec3(0.1,0.1,0.1) * MaterialDiffuseColor;
	vec3 MaterialSpecularColor = vec3(0.3,0.3,0.3);

	// Distance to the light
	float distance = length( LightPosition_worldspace - Position_worldspace );
	
	// Taking the normal from the normalmap texture
	// bring it to range between -1 and 1
	// and multiply by the tgnmatrix i created in the vertex shader
	vec3 n = normalize(tbnMatrix * (255.0/128.0 * (texture( normalmap_texture, UV )).xyz - 1));

	// Direction of the light (from the fragment to the light)
	vec3 l = normalize( LightDirection_cameraspace );
	// Cosine of the angle between the normal and the light direction, 
	// clamped above 0
	//  - light is at the vertical of the triangle -> 1
	//  - light is perpendicular to the triangle -> 0
	//  - light is behind the triangle -> 0
	float cosTheta = clamp( dot( n,l ), 0,1 );
	
	// Eye vector (towards the camera)
	vec3 E = normalize(EyeDirection_cameraspace);
	// Direction in which the triangle reflects the light
	vec3 R = reflect(-l,n);
	// Cosine of the angle between the Eye vector and the Reflect vector,
	// clamped to 0
	//  - Looking into the reflection -> 1
	//  - Looking elsewhere -> < 1
	float cosAlpha = clamp( dot( E,R ), 0,1 );
	
	// ...variable bias
	float bias = 0.005*tan(acos(cosTheta));
	bias = clamp(bias, 0,0.01);
	
	// Visibility
	float visibility = textureProj(shadowMap, ShadowCoord, bias);

	vec3 color = 
		// Ambient : simulates indirect lighting
		MaterialAmbientColor +
		// Diffuse : "color" of the object
		visibility * MaterialDiffuseColor * LightColor * LightPower * cosTheta +
		// Specular : reflective highlight, like a mirror
		visibility * MaterialSpecularColor * LightColor * LightPower * pow(cosAlpha,5);
	
	gl_FragColor = vec4(color, 1.0);
}