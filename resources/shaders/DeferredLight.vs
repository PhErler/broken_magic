#version 330                                                                        

layout (location = 0) in vec3 Position; 

uniform mat4 uWVP;

void main()
{          
    gl_Position = uWVP * vec4(Position, 1.0);
}
