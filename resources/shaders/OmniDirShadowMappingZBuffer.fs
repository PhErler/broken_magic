#version 330 core

uniform vec2 nearFar; // near and far plane for cm-cams
uniform vec4 lightPos; // world space light position

in vec4 wsPosFromGS;

void main(void) {

	// calculate distance
	float wsDist = distance(wsPosFromGS, lightPos);
	
	// map value to [0;1] by dividing by far plane distance
	float wsDistNormalized = (wsDist - nearFar.x) / (nearFar.y - nearFar.x);
	
	
	// write modified depth
	gl_FragDepth = wsDistNormalized;
	
	// when using depth-only FBO, do NOT write to color!!!
}