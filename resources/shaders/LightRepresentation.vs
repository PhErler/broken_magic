#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexMS;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormalMS;

// Output data ; will be interpolated for each fragment.
out vec3 normalVS;
out vec3 eyeVS;
out vec3 lightDirVS;

// Values that stay constant for the whole mesh.
uniform mat4 mvp;
uniform mat4 v;
uniform mat4 m;
uniform mat4 mv;
uniform vec3 posLightVS;

void main()
{

	// Output position of the vertex, in clip space : MVP * position
	gl_Position =  mvp * vec4(vertexMS,1);
	
	// Vector that goes from the vertex to the camera, in camera space.
	// In camera space, the camera is at the origin (0,0,0).
	vec3 vertexVS = (v * m * vec4(vertexMS, 1)).xyz;
	eyeVS = vec3(0,0,0) - vertexVS;

	// we want the light dir in camera space without translation
	lightDirVS = normalize(vertexVS - posLightVS);
	
	// Normal of the the vertex, in camera space
	// Only correct if ModelMatrix does not scale the model ! Use its inverse transpose if not.
	normalVS = (mv * vec4(vertexNormalMS, 0)).xyz;
}

