#version 330 core

struct BaseLight
{
	vec3 DiffuseColor;
	vec3 AmbientColor;
	vec3 SpecularColor;
};

struct DirectionalLight
{
    BaseLight Base;
    vec3 Direction;
};

// Uniform are constant for the whole mesh
uniform sampler2D uDiffuseTextureSampler;
uniform sampler2DShadow uShadowMap;

uniform DirectionalLight uDirectionalLight;

uniform vec3 uEyeWorldPos;
uniform float uShadowMappingSampleDistance;
uniform vec2 uTextureTiling;
uniform float uSpecularExponent;

// Interpolated values from the vertex shaders
in vec2 UV;
in vec3 Position_worldspace;
in vec3 Normal_cameraspace;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;
in vec4 ShadowCoord;

// Ouput data
layout(location = 0) out vec4 color;

vec2 poissonDisk[16] = vec2[]( 
   vec2( -0.94201624, -0.39906216 ), 
   vec2( 0.94558609, -0.76890725 ), 
   vec2( -0.094184101, -0.92938870 ), 
   vec2( 0.34495938, 0.29387760 ), 
   vec2( -0.91588581, 0.45771432 ), 
   vec2( -0.81544232, -0.87912464 ), 
   vec2( -0.38277543, 0.27676845 ), 
   vec2( 0.97484398, 0.75648379 ), 
   vec2( 0.44323325, -0.97511554 ), 
   vec2( 0.53742981, -0.47373420 ), 
   vec2( -0.26496911, -0.41893023 ), 
   vec2( 0.79197514, 0.19090188 ), 
   vec2( -0.24188840, 0.99706507 ), 
   vec2( -0.81409955, 0.91437590 ), 
   vec2( 0.19984126, 0.78641367 ), 
   vec2( 0.14383161, -0.14100790 ) 
);

// 0 to 1, multiply with color to get shadowed color
float getShadowFactor()
{
	int numSamples = 4;
	float shadowInfluence = 1 / float(numSamples);
	float shadowMult = 1;
	// Sample the shadow map numSamples times
	for (int i = 0; i < numSamples; ++i){
		// Always the same samples.
		// Gives a fixed pattern in the shadow, but no noise
		int index = i;
		
		vec3 shadowCoordinates = vec3(ShadowCoord.xy + poissonDisk[index] / uShadowMappingSampleDistance, ShadowCoord.z / ShadowCoord.w);
		
		// if fully in shadow only ambient color remains
		shadowMult -= shadowInfluence * (1.0 - texture(uShadowMap, shadowCoordinates));
	}
	return shadowMult;
}

// Blinn-Phong-Shading
vec4 CalcLightInternal(BaseLight light,
					   vec3 lightDirection,
					   vec3 worldPos,
					   vec3 normal)
{
	// ambient
    vec4 ambientColor = vec4(light.AmbientColor, 1.0);
	
	// diffuse
    float diffuseFactor = clamp( dot(normal, lightDirection), 0, 1 );
	vec4 diffuseColor = vec4(light.DiffuseColor * diffuseFactor, 1.0);

	// specular
    vec4 specularColor = vec4(0, 0, 0, 0);
	vec3 vertexToEye = normalize(uEyeWorldPos - worldPos);
	vec3 lightReflect = normalize(reflect(lightDirection, normal));
	float specularFactor = clamp( dot(vertexToEye, lightReflect), 0, 1 );
	specularFactor = pow(specularFactor, uSpecularExponent);
	specularColor = vec4(light.SpecularColor * specularFactor, 1.0);

	float shadowFactor = getShadowFactor();
	
    return (ambientColor + diffuseColor * shadowFactor + specularColor * shadowFactor);
}

void main()
{
    vec4 dirLight = CalcLightInternal(uDirectionalLight.Base,
		normalize(LightDirection_cameraspace), Position_worldspace, Normal_cameraspace);
	vec3 materialDiffuseColor = texture2D( uDiffuseTextureSampler, UV * uTextureTiling ).rgb;
	color = vec4(materialDiffuseColor, 1.0) * dirLight;
}