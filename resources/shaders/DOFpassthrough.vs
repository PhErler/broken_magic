#version 330 core

// Input vertex data, different for all executions of this shader.
in vec3 vertexPosition_modelspace;

// Output data ; will be interpolated for each fragment.
out vec2 FragTexCoord;

void main(void)
{
	gl_Position = vec4(vertexPosition_modelspace,1);
	FragTexCoord = (vertexPosition_modelspace.xy+vec2(1,1))/2.0;
}
