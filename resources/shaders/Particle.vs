#version 330 core

// input
layout(location = 0) in vec3 vertices;  // vertices
layout(location = 1) in vec4 position;	// position
layout(location = 2) in vec4 color;		// color

// output
out vec2 UV;
out vec4 particlecolor;

uniform vec3 CameraRight_worldspace; // orientation of particle depends on camera
uniform vec3 CameraUp_worldspace;
uniform mat4 VP;

void main()
{
	float particleSize = position.w;
	vec3 particleCenter_wordspace = position.xyz;
	
	vec3 vertexPosition_worldspace = 
		particleCenter_wordspace
		+ CameraRight_worldspace * vertices.x * particleSize
		+ CameraUp_worldspace * vertices.y * particleSize;

	// new position of vertex
	gl_Position = VP * vec4(vertexPosition_worldspace, 1.0f);

	// UV coordinates
	UV = vertices.xy + vec2(0.5, 0.5);
	particlecolor = color;
}

