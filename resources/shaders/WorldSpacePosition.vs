#version 330

layout (location = 0) in vec3 Position;                                          

uniform mat4 gMVP;
uniform mat4 gModelToWorld;

out vec3 WorldSpacePos;

void main()
{       
    gl_Position    = gMVP * vec4(Position, 1.0);
    WorldSpacePos  = (gModelToWorld * vec4(Position, 1.0)).xyz;
}