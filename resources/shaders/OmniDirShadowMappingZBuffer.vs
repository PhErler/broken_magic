#version 330 core

uniform mat4 modelMatrix; //model matrix (passed per object)

in vec3 vertexPos; // object space vertex positions

void main(void) {

	// transform vertex to world space
	gl_Position = modelMatrix * vec4(vertexPos, 1.0);
	
	// in the GS the value of gl_Position can
	// be accessed like this:
	// gl_in[�triangle_vertex_idx�].gl_Position;
}