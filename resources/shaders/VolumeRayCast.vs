#version 330

in vec3 Position;                                          

uniform mat4 worldToModel;

noperspective out vec2 textureCoordinatesSS;

void main()
{       
	gl_Position = vec4(Position, 1);
    textureCoordinatesSS = (gl_Position.xy + vec2(1.0, 1.0)) * 0.5;
}