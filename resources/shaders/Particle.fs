#version 330 core

// imput values from vertex shader
in vec2 UV;
in vec4 particlecolor;

// output
out vec4 color;

uniform sampler2D myTextureSampler;

void main(){
	// output color at UV position
	color = texture2D( myTextureSampler, UV ) * particlecolor;
}