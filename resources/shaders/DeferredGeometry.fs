#version 330

in vec2 TexCoord0;
in vec3 Normal0;
in vec3 WorldPos0;
in mat3 tbnMatrix;

layout (location = 0) out vec3 WorldPosOut;
layout (location = 1) out vec3 DiffuseOut;
layout (location = 2) out vec3 NormalOut;
layout (location = 3) out vec3 TexCoordOut;

uniform sampler2D gColorMap;
uniform sampler2D gNormalMap; 
uniform int normalmapping;

void main()
{
	WorldPosOut     = WorldPos0;
	DiffuseOut      = texture(gColorMap, TexCoord0).xyz;
	NormalOut       = normalize(Normal0);
	TexCoordOut     = vec3(TexCoord0, 0.0);
	
	// For normal mapping
	//if (normalmapping != 0) {
		// Taking the normal from the normalmap texture
		// bring it to range between -1 and 1
		// and multiply by the tbnmatrix i created in the vertex shader
		//NormalOut = normalize(tbnMatrix * (255.0/128.0 * (texture( gNormalMap, TexCoord0 )).xyz - 1));  
	//}
}
