#version 330

uniform sampler2D colorTex;
uniform sampler2D DOF_blurValueTex, DOF_gaussianTex;
uniform sampler2D BLOOM_gaussian;
uniform float exposure;
in vec2 FragTexCoord;

void main (void)
{
	// ------------------------- DOF            ---------------
	vec4 color          = texture2D(colorTex, FragTexCoord);
	vec4 dof_gaussian   = texture2D(DOF_gaussianTex, FragTexCoord);
	vec4 dof_blurred    = texture2D(DOF_blurValueTex, FragTexCoord);
	vec4 bloom_gaussian = texture2D(BLOOM_gaussian, FragTexCoord);

	//vec4 dof_FragColor = color + dof_blurred * (dof_gaussian - color);
	vec4 dof_FragColor = dof_blurred * (dof_gaussian - color);
	

	// ------------------------- BLENDING BLOOM ---------------
		
	vec4 bloom_value = bloom_gaussian; // additive blending from bloom
	
	// ------------------------- HDR            ---------------
	const float gamma = 2.2;
	vec3 hdrColor = color.rgb;
  
    // Reinhard tone mapping
    vec3 mapped = vec3(1.0) - exp(-hdrColor * exposure); // To be able to play around
    //vec3 mapped = hdrColor / (hdrColor + vec3(1.0));
    // Gamma correction 
    mapped = pow(mapped, vec3(1.0 / gamma));
  
    vec4 toneColor = vec4(mapped, 1.0);
	
	if (exposure == 0.0)
	{
		toneColor = color;
	}	
	
	// ------------------------- RESULT for gl_FragColor
	gl_FragColor = toneColor + dof_FragColor + bloom_value;
	
}
