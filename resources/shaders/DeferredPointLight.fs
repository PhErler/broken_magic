#version 330

struct BaseLight
{
    vec3 DiffuseColor;
    vec3 AmbientColor;
    vec3 SpecularColor;
};

struct Attenuation
{
    float Constant;
    float Linear;
    float Exp;
};

struct PointLight
{
    BaseLight Base;
    vec3 Position;
    Attenuation Atten;
};

/*struct SpotLight
{
    PointLight Base;
    vec3 Direction;
    float Cutoff;
};*/

uniform sampler2D uPositionMap;
uniform sampler2D uColorMap;
uniform sampler2D uNormalMap;

uniform samplerCubeShadow uCubeShadowMap;

uniform PointLight uPointLight;
//uniform SpotLight uSpotLight;

uniform vec3 uEyeWorldPos;
uniform float uSpecularExponent;
uniform vec2 uScreenSize;
uniform int gLightType;
uniform float uSizeOfCubeTex;

uniform vec2 uNearFar; // near and far plane for cm-cams

// array of offset direction for sampling
vec3 gridSamplingDisk[20] = vec3[]
(
   vec3(1, 1, 1), vec3(1, -1, 1), vec3(-1, -1, 1), vec3(-1, 1, 1), 
   vec3(1, 1, -1), vec3(1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
   vec3(1, 1, 0), vec3(1, -1, 0), vec3(-1, -1, 0), vec3(-1, 1, 0),
   vec3(1, 0, 1), vec3(-1, 0, 1), vec3(1, 0, -1), vec3(-1, 0, -1),
   vec3(0, 1, 1), vec3(0, -1, 1), vec3(0, -1, -1), vec3(0, 1, -1)
);

// Blinn-Phong-Shading
vec4 CalcLightInternal(BaseLight light,
					   vec3 lightDirection,
					   vec3 worldPos,
					   vec3 normal)
{
	// ambient
    vec4 ambientColor = vec4(light.AmbientColor, 1.0);
	
	// diffuse
    float diffuseFactor = clamp( dot(normal, lightDirection), 0, 1 );
	vec4 diffuseColor = vec4(light.DiffuseColor * diffuseFactor, 1.0);

	// specular
    vec4 specularColor = vec4(0, 0, 0, 0);
	vec3 vertexToEye = normalize(uEyeWorldPos - worldPos);
	vec3 lightReflect = normalize(reflect(lightDirection, normal));
	float specularFactor = clamp( dot(vertexToEye, lightReflect), 0, 1 );
	specularFactor = pow(specularFactor, uSpecularExponent);
	specularColor = vec4(light.SpecularColor * specularFactor, 1.0);

    return (ambientColor + diffuseColor + specularColor);
}

vec4 CalcPointLight(vec3 worldPos, vec3 normal, vec3 lightDirection)
{
    float Distance = length(lightDirection);
    lightDirection = normalize(lightDirection);

    vec4 color = CalcLightInternal(uPointLight.Base, lightDirection, worldPos, normal);

    float attenuation =  uPointLight.Atten.Constant +
                         uPointLight.Atten.Linear * Distance +
                         uPointLight.Atten.Exp * Distance * Distance;

    attenuation = 1 / (1 + attenuation);

    return color * attenuation;
}

vec2 CalcTexCoord()
{
    return gl_FragCoord.xy / uScreenSize;
}

out vec4 FragColor;

void main()
{	
    vec2 texCoord = CalcTexCoord();
	vec3 worldPos = texture(uPositionMap, texCoord).xyz;
	vec3 normal = texture(uNormalMap, texCoord).xyz;
	normal = normalize(normal);

	// calculate vector from surface point to light position
	vec3 lightDir = worldPos - uPointLight.Position;
	
	// discard if fragment is not facing to light
	if (dot(lightDir, normal) > 0)
	{
		discard;
	}
	
	vec3 diffuseColor = texture(uColorMap, texCoord).xyz;
	
	// WS dist-to-lightsource for current fragment
	float currFragmentDistToLight = length(lightDir);
	
	// map value to [0;1] by dividing by far plane distance
	float currFragmentDistToLightNormalized = (currFragmentDistToLight - uNearFar.x) / (uNearFar.y - uNearFar.x);
	
	float eps = 0.01; // add a small offset (adjust as needed)
	
	float shadowFactor = 0;

   	// radius of PCF depending on distance from the light source
   	float diskRadius = (1.0 - currFragmentDistToLightNormalized) * 0.01;
   
	int numSamples;
	// first 8 are bounding box, expect it to be filled equally
   	for (numSamples = 0; numSamples < 8; ++numSamples)
   	{
		// read shadow factor value from cubemap shadow map
		shadowFactor += texture(uCubeShadowMap, vec4(lightDir + gridSamplingDisk[numSamples] * diskRadius, 
			currFragmentDistToLightNormalized - eps));
	}
	if (shadowFactor == 0 || shadowFactor == 8)
	{
   		for (numSamples = 8; numSamples < 20; ++numSamples)
   		{
			// read shadow factor value from cubemap shadow map
			shadowFactor += texture(uCubeShadowMap, vec4(lightDir + gridSamplingDisk[numSamples] * diskRadius, 
				currFragmentDistToLightNormalized - eps));
		}
	}
	
	shadowFactor /= numSamples;
	
	FragColor = shadowFactor * vec4(diffuseColor, 1.0) * CalcPointLight(worldPos, normal, lightDir);
	
	//FragColor = vec4(smallestDistToLightNormalized,
	//	currFragmentDistToLightNormalized,0,1);
	//FragColor = vec4(smallestDistToLightNormalized,
	//	smallestDistToLightNormalized,smallestDistToLightNormalized,1);
	//FragColor = vec4(currFragmentDistToLightNormalized, 
	//	currFragmentDistToLightNormalized,currFragmentDistToLightNormalized,1);
	//FragColor = vec4(shadowFactor,shadowFactor,shadowFactor,1);
}
