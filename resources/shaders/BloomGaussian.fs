#version 330 core

uniform sampler2D Tex0;
uniform int Width;
uniform int Height;
uniform bool horizontal;

layout(location = 0) out vec4 FragmentColor;

uniform float offset[3] = float[]( 0.0, 1.3846153846, 3.2307692308 );
uniform float weight[3] = float[]( 0.2270270270, 0.3162162162, 0.0702702703 );

void main(void)
{
	FragmentColor = texture( Tex0, vec2(gl_FragCoord) / vec2(Width, Height) ) * weight[0];
	if (horizontal)
	{
		for (int i=1; i<3; i++) {
			FragmentColor += texture2D( Tex0, ( vec2(gl_FragCoord)+vec2(0.0, offset[i]) )/vec2(Width, Height) ) * weight[i];
			FragmentColor += texture2D( Tex0, ( vec2(gl_FragCoord)-vec2(0.0, offset[i]) )/vec2(Width, Height) ) * weight[i];
		}
	}
	else 
	{
		for (int i=1; i<3; i++) {
			FragmentColor += texture2D( Tex0, ( vec2(gl_FragCoord)+vec2(offset[i], 0.0) )/vec2(Width, Height) ) * weight[i];
			FragmentColor += texture2D( Tex0, ( vec2(gl_FragCoord)-vec2(offset[i], 0.0) )/vec2(Width, Height) ) * weight[i];
		}
	}
}
