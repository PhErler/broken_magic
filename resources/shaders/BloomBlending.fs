#version 330

uniform sampler2D Tex0, Tex1;
uniform float exposure;
in vec2 FragTexCoord;

void main (void)
{
	const float gamma = 2.2;
	vec4 color    = texture2D(Tex0, FragTexCoord);
	vec4 gaussian = texture2D(Tex1, FragTexCoord);
	
	vec3 addition = color.rgb + gaussian.rgb; // additive blending
	gl_FragColor = vec4(addition, 1.0f);
	
	
	// if working with HDR
	// tone mapping
    //vec3 result = vec3(1.0) - exp(-addition * exposure);
	
    // also gamma correct while we're at it       
    //result = pow(result, vec3(1.0 / gamma));
	
    //gl_FragColor = vec4(result, 1.0f);
}
