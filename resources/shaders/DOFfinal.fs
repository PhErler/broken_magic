#version 330

uniform sampler2D colorTex, blurValueTex, gaussianTex;
//uniform sampler2D Tex0, Tex1;
in vec2 FragTexCoord;

void main (void)
{
	vec4 color    = texture2D(colorTex, FragTexCoord);
	vec4 gaussian = texture2D(gaussianTex, FragTexCoord);
	vec4 blurred  = texture2D(blurValueTex, FragTexCoord);

	gl_FragColor = color + blurred * (gaussian - color);
	
}
