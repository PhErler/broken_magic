#version 330 core

layout(triangles) in;

//3 vertices per tri, output 6 tris (1 for each cm-face)
layout(triangle_strip, max_vertices=18) out;

// contains P*V[i], transforms from WS to cubemap-face i
uniform mat4 cubemapMatrix[6];

out vec4 wsPosFromGS;

void main(void) {

	//iterate over the 6 cubemap faces
	for(gl_Layer=0; gl_Layer<6; ++gl_Layer) 
	{
		for(int triangleVertex = 0; triangleVertex < 3; ++triangleVertex) 
		{
			wsPosFromGS = gl_in[triangleVertex].gl_Position;
			gl_Position = cubemapMatrix[gl_Layer] * wsPosFromGS;
			EmitVertex();
		}
		EndPrimitive();
	}
}