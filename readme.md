# Broken Magic


A graphics demo by Robin Melan and Philipp Erler
http://www.pplusplus.lima-city.de/brokenmagic.html

## Credits


### Music

Thanks to Adrian Gajda!


### Tutorials

Thanks to OGLdev for the awesome deferred shading tutorials!  
[Tutorial 35](http://ogldev.atspace.co.uk/www/tutorial35/tutorial35.html)  
[Tutorial 36](http://ogldev.atspace.co.uk/www/tutorial36/tutorial36.html)  
[Tutorial 37](http://ogldev.atspace.co.uk/www/tutorial37/tutorial37.html)  


### 3D Models

Non-commercial use or Personal use only or Commercial use License of [tf3dm](http://tf3dm.com/)   
[Tower Castle by alexfreestockvideo](http://tf3dm.com/3d-model/tower-castle-50097.html)  
[Dragon by kaos3d](http://tf3dm.com/3d-model/dragon-27353.html)  
[Sword by snakers](http://tf3dm.com/3d-model/fantasy-sword--35396.html)  
[Medieval Sword by michelh](http://tf3dm.com/3d-model/medieval-sword-super-textures-4k--54373.html)  
[Medieval Door by hassanalmalki](http://tf3dm.com/3d-model/medieval-door-16986.html)  
[Knight by mishanaycool](http://tf3dm.com/3d-model/knight-84265.html)  
[Old Book by animatedheaven](http://tf3dm.com/3d-model/old-open-book-71010.html)  
[Book of Monsters by moddlestudios](http://tf3dm.com/3d-model/book-of-monsters-52893.html)  
[Book 3d by 3dregenerator](http://tf3dm.com/3d-model/puo-72217.html)  
[Glass by thevideo115](http://tf3dm.com/3d-model/glass-71732.html)  
[Column by seth1998z](http://tf3dm.com/3d-model/column-53061.html)  
[Round table by fedis244](http://tf3dm.com/3d-model/3d-model-other-60936.html)  
[Barrels by lecra](http://tf3dm.com/3d-model/wooden-barrels-18992.html)  
[Eagle Flying by umar6419](http://tf3dm.com/3d-model/eagle-2-59246.html)  
[Eagle Sitting by umar6419](http://tf3dm.com/3d-model/eagle-1-16586.html)  
[Chest by 3dregenerator](http://tf3dm.com/3d-model/chest-with-gold-38221.html)  
[Castle by herminio](http://tf3dm.com/3d-model/castle-26507.html)  
[Crystal by allrise_mfkr](http://tf3dm.com/3d-model/3d-crystal-29456.html)  
[Human skull by luciferbelzebuth](http://tf3dm.com/3d-model/bad-skull-26784.html)  

Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0)  
[Castle Tower by Jason Shoumar](https://clara.io/view/e1756ee6-6f2e-48c2-86b0-bdb680a5684e#)  
[Skull by shawn neuroth](http://www.123dapp.com/3dcr-Creature/skull/2044522)  

Free use  
[Sidetable by Arvic](http://archive3d.net/?a=download&id=e0f627d0)  
[Bookcase by Tesch Enri](http://archive3d.net/?a=download&id=1ebe1bd2)  
[Chair by Viera](http://archive3d.net/?a=download&id=b5f2b3f3)  
[Window by Viktor Kovbunov](http://archive3d.net/?a=download&id=756f5fea)  


### Textures

Paving, Stone by totalTextures (TU Wien License)  

Creative Commons Attribution 3.0 Unported (CC BY 3.0)   
[Roof by Saroman](http://opengameart.org/node/10480)  
[Medieval textures by jojo-ojoj](http://www.deviantart.com/art/Bronze-seamless-textures-424959539)  
[Wizard Portrait 3 by kirill777](http://opengameart.org/content/necromancer) modified  

Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0)    
[Marble by goodtextures](http://img02.deviantart.net/de1e/i/2011/340/9/a/seamless_marble_texture_01_by_goodtextures-d4ic061.jpg)  
[Wizard Portrait 1 by killyoverdrive](http://opengameart.org/content/dark-alchemyst) modified  
[Wizard Portrait 2 by Igor-Esaulov](http://igor-esaulov.deviantart.com/art/Female-wizard-of-Ovl-Necromantic-Guild-295281371) modified  

Free non-commercial, personal use
[Book lord of the rings by karnak](http://www.desktopwallpapers4.me/movies/middle-earth-map-the-lord-of-the-rings-18255/)  
[Snow globe by Jonas De Ro](http://www.textures.com/download/snow0078/42412)  
[Crystals by Textures.com](http://www.textures.com/download/windowsother0023/52180?q=glass&filter=seamless)  


### Libraries

This distributions contain source code from several sources. Each of these sources has its own copyright and licenses.

The GLFW library is distributed under the terms of the zlib/libpng license, as specified in the "glfw/COPYING.txt" file.

The GLM library is distributed under the terms of the MIT License, as specified in the "glm/copying.txt" file.

With a few exceptions exceptions, all other materials in this distribution are copywritten and distributed under the terms of the MIT License, as specified in the "./MIT License.txt" file. The following files are not distributed under this license:

* glimg/source/stb_image.c
* glimg/source/stb_image.h

These files have been released in the public domain and therefore are not copywritten.

